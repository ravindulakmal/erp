package com.et.smartrrol.inventory.services;


import com.et.smartrrol.commondata.models.ApiResponse;
import com.et.smartrrol.commondata.models.Company;
import com.et.smartrrol.commondata.models.Status;
import com.et.smartrrol.commondata.models.v0.Supplier;
import com.et.smartrrol.commondata.models.v0.SupplierCredited;
import com.et.smartrrol.commondata.repo.CompanyRepo;
import com.et.smartrrol.commondata.repo.v0.SupplierCreditedRepo;
import com.et.smartrrol.commondata.repo.v0.SupplierRepo;
import com.et.smartrrol.inventory.payload.supplier.SupplierCreditedDTO;
import com.et.smartrrol.inventory.payload.supplier.SupplierDTO;
import com.et.smartrrol.inventory.payload.supplier.SupplierSearchDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Service
public class SupplierServices {

    @Autowired
    private SupplierRepo supplierRepo;

    @Autowired
    private CompanyRepo companyRepo;

    @Autowired
    private SupplierCreditedRepo supplierCreditedRepo;

    public ApiResponse createSupplier(SupplierDTO supplierDTO){
        ApiResponse apiResponse = new ApiResponse();
        try {
            Supplier supplier = new Supplier();
            BeanUtils.copyProperties(supplierDTO, supplier);
            supplier.setStatus(Status.STATUS_ACTIVE.getVal());
            supplier.setCompId(supplierDTO.getCompId());
            supplier.setCode(this.generateSupplierCode(companyRepo.findById(supplierDTO.getCompId()).get()));
            supplier.setIsCredited(supplierDTO.getIsCredited());
            Supplier save = supplierRepo.save(supplier);
            if (save != null) {

                if (supplierDTO.getIsCredited()==1){
                    SupplierCredited supplierCredited= new SupplierCredited();
//                    if ()
                    supplierCredited.setStatus(Status.STATUS_ACTIVE.getVal());
                    supplierCredited.setSupplier(save);
                    supplierCredited.setPaymentTerms(supplierDTO.getSupplierCreditedDTO().getPaymentTerms());
                    supplierCredited.setCreditLimit(supplierDTO.getSupplierCreditedDTO().getCreditLimit());
                    SupplierCredited save1 = supplierCreditedRepo.save(supplierCredited);
                    if (save1!=null) {
                        apiResponse.setResponseDesc("SUPPLIER CREATED WITH CREDIT");
                        apiResponse.setResponseCode(200);
                        apiResponse.setData(save);
                    }
                }else{
                    apiResponse.setResponseDesc("SUPPLIER CREATED WITHOUT CREDIT");
                    apiResponse.setResponseCode(200);
                    apiResponse.setData(save);
                }
            }
        }catch (Exception e){
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(0);
        }
        return apiResponse;
    }


    public ApiResponse updateSuppler(SupplierDTO supplierDTO){
        ApiResponse apiResponse = new ApiResponse();
        try {
            Supplier supplier = supplierRepo.findById(supplierDTO.getId()).get();
            String code =  supplier.getCode();
            BeanUtils.copyProperties(supplierDTO, supplier);
            supplier.setCode(code);
            supplier.setStatus(Status.STATUS_ACTIVE.getVal());
            supplier.setCompId(supplierDTO.getCompId());
            supplier.setCode(supplierDTO.getCode());
            supplier.setIsCredited(supplierDTO.getIsCredited());
            Supplier save = supplierRepo.save(supplier);
            if (supplierDTO.getIsCredited()==1){
                SupplierCredited supplierCredited = null;
                if (supplierDTO.getSupplierCreditedDTO().getId()==null || supplierDTO.getSupplierCreditedDTO().getId()==0) {
                    supplierCredited = new SupplierCredited();
                }else{
                    supplierCredited = supplierCreditedRepo.findById(supplierDTO.getSupplierCreditedDTO().getId()).get();
                }

                supplierCredited.setSupplier(save);
                supplierCredited.setStatus(Status.STATUS_ACTIVE.getVal());
                supplierCredited.setPaymentTerms(supplierDTO.getSupplierCreditedDTO().getPaymentTerms());
                supplierCredited.setCreditLimit(supplierDTO.getSupplierCreditedDTO().getCreditLimit());
                SupplierCredited save1 = supplierCreditedRepo.save(supplierCredited);
                if (save1!=null) {
                    apiResponse.setResponseDesc("SUPPLIER UPDATED WITH CREDITED");
                    apiResponse.setResponseCode(200);
                    apiResponse.setData(save);
                }
            }else{
                apiResponse.setResponseDesc("SUPPLIER CREATED WITHOUT CREDIT");
                apiResponse.setResponseCode(200);
                apiResponse.setData(save);
            }
        }catch (Exception e){
//            e.printStackTrace();
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(0);
        }
        return apiResponse;
    }

    public ApiResponse findAllSupplier(SupplierSearchDTO supplierDTO){
        ApiResponse apiResponse = new ApiResponse();

        List<SupplierDTO> supplierDTOList = new ArrayList<>();
        try {
            PageRequest page_req = PageRequest.of(supplierDTO.getPage(), supplierDTO.getCount());
            Page<Supplier> all = supplierRepo.findAll(this.getBrandBySearch(supplierDTO), page_req);


            apiResponse.setResponseDesc("SUPPLIER FIND ALL AND SEARCH");
            apiResponse.setResponseCode(200);
            apiResponse.setData(all);

        }catch (Exception e){

            e.printStackTrace();
//            apiResponse.setResponseDesc(e.getMessage());
//            apiResponse.setResponseCode(500);
//            apiResponse.setData(0);
        }
        return apiResponse;
    }

    private Specification<Supplier> getBrandBySearch(SupplierSearchDTO supplierSearchDTO){

        Specification<Supplier> ordersSpecification = new Specification<Supplier>() {
            @Override
            public Predicate toPredicate(Root<Supplier> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                predicates.add(criteriaBuilder.equal(root.get("compId"),supplierSearchDTO.getCompId()));
                predicates.add(criteriaBuilder.equal(root.get("status"),Status.STATUS_ACTIVE.getVal()));
                predicates.add(criteriaBuilder.equal(root.get("branchId"),supplierSearchDTO.getBranchId()));
                Predicate like = criteriaBuilder.like(root.get("fullName"), "%" + supplierSearchDTO.getName() + "%");
                Predicate orderCode = criteriaBuilder.like(root.get("code"), "%" + supplierSearchDTO.getName() + "%");
                Predicate description = criteriaBuilder.like(root.get("email"), "%" + supplierSearchDTO.getName() + "%");
                Predicate mobileNo = criteriaBuilder.like(root.get("mobileNo"), "%" + supplierSearchDTO.getName() + "%");
                Predicate emergencyMobileNo = criteriaBuilder.like(root.get("emergencyMobileNo"), "%" + supplierSearchDTO.getName() + "%");
                predicates.add(criteriaBuilder.or(like,orderCode,description,mobileNo,emergencyMobileNo));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };

        return ordersSpecification;

    }

    public ApiResponse findBynameWithoutPagination(SupplierSearchDTO supplierDTO){
        ApiResponse apiResponse = new ApiResponse();

        List<SupplierDTO> supplierDTOList = new ArrayList<>();
        try {


            supplierRepo.findBynameWithoutPagination(Status.STATUS_ACTIVE.getVal(), supplierDTO.getCompId(),supplierDTO.getName()).forEach(customer -> {
                SupplierDTO supplierDTO1 = new SupplierDTO();
                BeanUtils.copyProperties(customer,supplierDTO1);
                supplierDTO1.setStatus(customer.getStatus());
                supplierDTO1.setCompId(customer.getCompId());
                supplierDTO1.setCode(customer.getCode());
                if(customer.getIsCredited()==1){
                    SupplierCreditedDTO supplierCredited= new SupplierCreditedDTO();

                    BeanUtils.copyProperties(supplierCreditedRepo.findBySupplierIdAndStatus(customer.getId(),Status.STATUS_ACTIVE.getVal()),supplierCredited);
                    supplierDTO1.setSupplierCreditedDTO(supplierCredited);
                }
                supplierDTOList.add(supplierDTO1);
            });

            apiResponse.setResponseDesc("SUPPLIER FIND ALL AND SEARCH");
            apiResponse.setResponseCode(200);
            apiResponse.setData(supplierDTOList);

        }catch (Exception e){

            e.printStackTrace();
//            apiResponse.setResponseDesc(e.getMessage());
//            apiResponse.setResponseCode(500);
//            apiResponse.setData(0);
        }
        return apiResponse;
    }

    public ApiResponse deleteSupplier(Integer id){

        ApiResponse apiResponse = new ApiResponse();
        Supplier supplier = supplierRepo.findById(id).get();
        supplier.setStatus(Status.STATUS_DISABLED.getVal());
        Supplier save = supplierRepo.save(supplier);
        if (save!=null){
            apiResponse.setResponseDesc("SUPPLIER DISABLED");
            apiResponse.setResponseCode(200);
            apiResponse.setData(save);
        }
        return apiResponse;
    }

    private String generateSupplierCode(Company company){
        Integer maxId = supplierRepo.getMaxId();
        int i = maxId + 1;
        String code = "SUPP"+company.getId()+"00"+i;
        return code;
    }
}
