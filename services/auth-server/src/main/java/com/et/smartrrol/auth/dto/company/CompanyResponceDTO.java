package com.et.smartrrol.auth.dto.company;

import com.et.smartrrol.auth.dto.branch.BranchDTO;


import com.et.smartrrol.commondata.models.BaseEntity;
import lombok.Data;

import java.util.List;

@Data
public class CompanyResponceDTO extends BaseEntity {
    private Integer id;
    private Integer subscriptionId;
    private String name;
    private String companyCode;
    private String subsName;
    private String brNo;
    private String officialMobile,businessName,businessType;
    private List<BranchDTO> branchDTOList;
}
