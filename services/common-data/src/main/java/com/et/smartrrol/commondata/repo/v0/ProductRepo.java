package com.et.smartrrol.commondata.repo.v0;


import com.et.smartrrol.commondata.models.v0.Product;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepo extends JpaRepository<Product, Integer>, JpaSpecificationExecutor<Product> {

    Product findByProductCode(String prodCode);

    @Query("Select u from Product u where  (:status is null or u.status = :status) and (:companyID is null or u.compId = :companyID) and" +
            " (:email is null or u.productCode like %:email% or u.productName like %:email% " +
            " or u.quality like %:email%)")
    List<Product> findByname(Integer status, Integer companyID, String email, Pageable pageable);

    @Query("Select u from Product u where  (:status is null or u.status = :status) and (:companyID is null or u.compId = :companyID) and" +
            " (:email is null or u.productCode like %:email% or u.productName like %:email% " +
            " or u.quality like %:email%)")
    List<Product> findBynameWithoutPagination(Integer status, Integer companyID, String email);

    @Query("SELECT coalesce(max(inv.id), 0) FROM Product inv")
    Integer getMaxId();


    boolean existsByBatchNumber(String batchNumber);
}
