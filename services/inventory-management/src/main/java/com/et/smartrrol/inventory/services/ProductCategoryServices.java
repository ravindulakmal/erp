package com.et.smartrrol.inventory.services;


import com.et.smartrrol.commondata.models.ApiResponse;
import com.et.smartrrol.commondata.models.Company;
import com.et.smartrrol.commondata.models.Status;
import com.et.smartrrol.commondata.models.v0.ProductCategory;
import com.et.smartrrol.commondata.repo.CompanyRepo;
import com.et.smartrrol.commondata.repo.v0.ProductCategoryRepo;
import com.et.smartrrol.inventory.payload.category.CategorySearchDTO;
import com.et.smartrrol.inventory.payload.product.ProductCategoryDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProductCategoryServices {
    @Autowired
    private ProductCategoryRepo productCategoryRepo;

    @Autowired
    private CompanyRepo companyRepo;

    public ApiResponse createProductCategory(ProductCategoryDTO productDTO){
        ApiResponse apiResponse = new ApiResponse();

        try{
            ProductCategory productCategory = new ProductCategory();
            BeanUtils.copyProperties(productDTO,productCategory);
            productCategory.setStatus(Status.STATUS_ACTIVE.getVal());
            productCategory.setCompId(productDTO.getCompId());
            productCategory.setCode(this.codeGenerator(companyRepo.findById(productDTO.getCompId()).get()));
            ProductCategory save = productCategoryRepo.save(productCategory);
            if (save!=null){
                apiResponse.setResponseDesc("PRODUCT CATEGORY CREATED");
                apiResponse.setResponseCode(200);
                apiResponse.setData(save);
            }
        }catch (Exception e){
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }
        return apiResponse;
    }

    public ApiResponse updateProductCategory(ProductCategoryDTO productDTO){
        ApiResponse apiResponse = new ApiResponse();

        try{
            ProductCategory productCategory = productCategoryRepo.findById(productDTO.getId()).get();
            BeanUtils.copyProperties(productDTO,productCategory);
            productCategory.setStatus(productDTO.getStatus());
            productCategory.setCompId(productDTO.getCompId());
            productCategory.setCode(productDTO.getCode());
            ProductCategory save = productCategoryRepo.save(productCategory);
            if (save!=null){
                apiResponse.setResponseDesc("PRODUCT CATEGORY UPDATED");
                apiResponse.setResponseCode(200);
                apiResponse.setData(save);
            }
        }catch (Exception e){
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }
        return apiResponse;
    }

    public ApiResponse findAllProductCategory(CategorySearchDTO categorySearchDTO){
        ApiResponse apiResponse = new ApiResponse();
        try {

            PageRequest page_req = PageRequest.of(categorySearchDTO.getPage(), categorySearchDTO.getCount());

            Page<ProductCategory> all = productCategoryRepo.findAll(this.getBrandBySearch(categorySearchDTO), page_req);

            apiResponse.setResponseDesc("FIND ALL PRODUCT CATEGORY");
            apiResponse.setResponseCode(200);
            apiResponse.setData(all);

        }catch (Exception e){
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }return apiResponse;
    }


    private Specification<ProductCategory> getBrandBySearch(CategorySearchDTO categorySearchDTO){

        Specification<ProductCategory> ordersSpecification = new Specification<ProductCategory>() {
            @Override
            public Predicate toPredicate(Root<ProductCategory> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                predicates.add(criteriaBuilder.equal(root.get("compId"),categorySearchDTO.getCompId()));
                predicates.add(criteriaBuilder.equal(root.get("status"),Status.STATUS_ACTIVE.getVal()));
                predicates.add(criteriaBuilder.equal(root.get("branchId"),categorySearchDTO.getBranchId()));
                Predicate like = criteriaBuilder.like(root.get("name"), "%" + categorySearchDTO.getName() + "%");
                Predicate orderCode = criteriaBuilder.like(root.get("code"), "%" + categorySearchDTO.getName() + "%");
                Predicate description = criteriaBuilder.like(root.get("description"), "%" + categorySearchDTO.getName() + "%");
                predicates.add(criteriaBuilder.or(like,orderCode,description));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };

        return ordersSpecification;

    }

    public ApiResponse findBynameWithoutPagination(CategorySearchDTO categorySearchDTO){
        ApiResponse apiResponse = new ApiResponse();
        try {
            List<ProductCategoryDTO> productCategoryDTOS = new ArrayList<>();


//            PageRequest page_req = PageRequest.of(categorySearchDTO.getPage(), categorySearchDTO.getCount());
            productCategoryRepo.findBynameWithoutPagination(Status.STATUS_ACTIVE.getVal(), categorySearchDTO.getCompId(), categorySearchDTO.getName()).forEach(branch -> {
                ProductCategoryDTO productCategoryDTO = new ProductCategoryDTO();

                BeanUtils.copyProperties(branch,productCategoryDTO);
                productCategoryDTO.setCompId(branch.getCompId());
                productCategoryDTOS.add(productCategoryDTO);
            });

            apiResponse.setResponseDesc("FIND ALL PRODUCT CATEGORY");
            apiResponse.setResponseCode(200);
            apiResponse.setData(productCategoryDTOS);

        }catch (Exception e){
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }return apiResponse;
    }

    public ApiResponse deleteroductCategory(Integer id){
        ApiResponse apiResponse = new ApiResponse();
        ProductCategory productCategory = productCategoryRepo.findById(id).get();
        productCategory.setStatus(Status.STATUS_DISABLED.getVal());
        productCategoryRepo.save(productCategory);

        apiResponse.setResponseDesc("FIND AND SEARCH PRODUCT CATEGORY");
        apiResponse.setResponseCode(200);
        apiResponse.setData(id);
        return apiResponse;
    }

    private String codeGenerator(Company company){
        Integer maxId = productCategoryRepo.getMaxId();
        int i = maxId + 1;
        String code = "PRODCAT"+company.getId()+"00"+i;
        return code;
    }

}
