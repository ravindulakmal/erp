package com.et.smartrrol.commondata.repo.v0;


import com.et.smartrrol.commondata.models.v0.CustomerCredited;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerCreditedRepo extends JpaRepository<CustomerCredited, Integer> , JpaSpecificationExecutor<CustomerCredited> {

    CustomerCredited findByCustomerId(Integer integer);
}
