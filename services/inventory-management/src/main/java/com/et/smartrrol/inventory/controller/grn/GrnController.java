package com.et.smartrrol.inventory.controller.grn;



import com.et.smartrrol.commondata.models.ApiResponse;
import com.et.smartrrol.inventory.payload.grn.GrnDto;
import com.et.smartrrol.inventory.payload.grn.SearchgrnDTO;
import com.et.smartrrol.inventory.services.GrnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("/grn")
@RestController
@CrossOrigin("*")

public class GrnController {

    @Autowired
    private GrnService grnService;

    @GetMapping
    public String testService(HttpServletRequest request) {
        System.out.println("I am " + request.getRequestURL().toString());
        return request.getRequestURL().toString();
    }



    @PostMapping("/create")
    public ApiResponse createGrn(@RequestBody GrnDto grnDto, @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId) {
//        , @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId

        grnDto.setUserName(userName);
        grnDto.setBranchId(branchId);
        grnDto.setCompId(compId);

        return grnService.createGrn(grnDto);

    }

    @PutMapping("/update")
    public ApiResponse updateGrn(@RequestBody GrnDto grnDto, @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId) {
//        , @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId

        grnDto.setUserName(userName);
        grnDto.setBranchId(branchId);
        grnDto.setCompId(compId);

        return grnService.updateGrn(grnDto);

    }

    @PostMapping("/findAll")
    public ApiResponse findAllBynameWIthoutPagination(@RequestBody SearchgrnDTO grnDto, @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId) {
        grnDto.setUserName(userName);
        grnDto.setBranchID(branchId);
        grnDto.setCompId(compId);
        return grnService.findAllBynameWIthoutPagination(grnDto);
    }

    @PostMapping("/findAll/{page}/{count}")
    public ApiResponse findAllGrns(@RequestBody SearchgrnDTO grnDto,
                                   @RequestHeader("userName") String userName,
                                   @RequestHeader("branchId") Integer branchId,
                                   @RequestHeader("compId") Integer compId,
                                   @PathVariable("page") Integer page,
                                   @PathVariable("count") Integer count) {
        grnDto.setUserName(userName);
        grnDto.setBranchID(branchId);
        grnDto.setCompId(compId);
        grnDto.setPage(page);
        grnDto.setCount(count);
        return grnService.findAll(grnDto);
    }

//    @DeleteMapping("/deleteGrn/{grnId}")
//    public ApiResponse deleteGrn(@PathVariable Integer grnId) {
//        ApiResponse apiResponse = new ApiResponse();
//        grnVersionTwoService.deleteGrn(grnId);
////        apiResponse.setData(allGrn);
//        apiResponse.setResponseCode(HttpStatus.OK.value());
//        return apiResponse;
//    }


//    @DeleteMapping("/deleteGrn/{grnId}/{itemId}")
//    public ApiResponse deleteGrnItemFromGrn(@PathVariable Integer grnId, @PathVariable Integer itemId) {
//        ApiResponse apiResponse = new ApiResponse();
//        grnVersionTwoService.deleteGrnItemFromGrn(grnId, itemId);
////        apiResponse.setData(allGrn);
//        apiResponse.setResponseCode(HttpStatus.OK.value());
//        return apiResponse;
//    }

}

