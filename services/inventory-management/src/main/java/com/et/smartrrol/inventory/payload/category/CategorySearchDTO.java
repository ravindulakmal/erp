package com.et.smartrrol.inventory.payload.category;

import lombok.Data;

@Data
public class CategorySearchDTO {
    private Integer compId;
    private Integer branchId;
    private String userName;
    private String name;
    private Integer page;
    private Integer count;
}
