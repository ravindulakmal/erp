package com.et.smartrrol.inventory;

import com.et.smartrrol.commondata.repo.CompanyRepo;
import com.et.smartrrol.commondata.repo.v0.GrnRepo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableJpaRepositories("com.et.smartrrol.commondata.repo")
@EntityScan("com.et.smartrrol.commondata.models")
@SpringBootApplication
@EnableEurekaClient
@EnableSwagger2
public class InventoryManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(InventoryManagementApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}


//	@Bean
//	public GrnRepo grnRepo(GrnRepo grnRepo) {
//		return grnRepo;
//	}

//	@Bean
//	public CompanyRepo companyRepo(CompanyRepo companyRepo) {
//		return companyRepo;
//	}

}
