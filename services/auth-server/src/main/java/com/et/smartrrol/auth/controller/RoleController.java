package com.et.smartrrol.auth.controller;

import com.et.smartrrol.auth.dto.RoleDTO;
import com.et.smartrrol.auth.service.RoleService;
import com.et.smartrrol.commondata.models.ApiResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/role")
@CrossOrigin("*")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @PostMapping("/create")
    public ApiResponse createRole(@RequestBody RoleDTO grnVersionTwoDTO) {
        ApiResponse apiResponse = new ApiResponse();

        try {
            Integer grnVersionTwo = roleService.creteRoel(grnVersionTwoDTO);
            apiResponse.setResponseCode(HttpStatus.OK.value());
            apiResponse.setData(grnVersionTwo);
            apiResponse.setResponseDesc("CREATE ROLE");
        } catch (Exception e) {
            e.printStackTrace();
            apiResponse.setResponseCode(HttpStatus.BAD_REQUEST.value());
            apiResponse.setData(null);
        }

        return apiResponse;
    }

    @GetMapping("/findAll")
    public ApiResponse findAll() {
        ApiResponse apiResponse = new ApiResponse();
        try {
            List<RoleDTO> allGrn = roleService.findALl();
            System.out.println(allGrn.size());
            apiResponse.setData(allGrn);
            apiResponse.setResponseDesc("FIND ALL ROLE");
        } catch (Exception e) {
            e.printStackTrace();
        }
        apiResponse.setResponseCode(HttpStatus.OK.value());
        return apiResponse;
    }

    @DeleteMapping("/delete/{id}")
    public ApiResponse deleteGrn(@PathVariable Integer id) {
        return roleService.deleteRole(id);
    }
}
