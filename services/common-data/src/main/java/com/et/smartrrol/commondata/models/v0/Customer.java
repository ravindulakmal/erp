package com.et.smartrrol.commondata.models.v0;


import com.et.smartrrol.commondata.models.BaseEntity;

import com.et.smartrrol.commondata.models.order.Orders;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@ToString
@Getter
@Setter
public class Customer extends BaseEntity implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;


    private String firstName,
            lastName,
            title,
            mobile,
            addLine1,
            addLine2,
            city,
            email,code;

    @OneToOne(mappedBy = "customer",fetch=FetchType.LAZY)
    private CustomerCredited customerCredited;
    @JsonIgnore
    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
    private List<Orders> orders;

}
