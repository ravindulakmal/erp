package com.et.smartrrol.inventory.payload.stock;


import com.et.smartrrol.commondata.models.BaseEntity;
import lombok.Data;

@Data
public class BranchDTO extends BaseEntity {
    private String name;
    private String branchCode;
    private String contact;
    private String address;
    private Integer numberOfEmployee;
    private Integer companyId;
    private Integer status;
    private Integer id;
    private Integer userId;

}
