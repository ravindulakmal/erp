package com.et.smartrrol.commondata.repo;


import com.et.smartrrol.commondata.models.inventory.ItemCategory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepo extends JpaRepository<ItemCategory,Integer>, JpaSpecificationExecutor<ItemCategory> {
    List<ItemCategory> findAllByName(String name);

    List<ItemCategory> findAllByParentItemCategoryIdNull();

    List<ItemCategory> findAllByParentItemCategoryIdAndStatus(Integer id, Integer status);

    List<ItemCategory> findAllByParentItemCategoryId(int id);



    @Query("Select u from ItemCategory u where  (:status is null or u.status = :status) and (:companyID is null or u.compId = :companyID) and  (:email is null or  u.name like %:email% or" +
            "  u.description like %:email%  )")
    List<ItemCategory> findByname(Integer status, Integer companyID, String email, Pageable pageable);

    @Query("Select u from ItemCategory u where  (:status is null or u.status = :status) and (:companyID is null or u.compId = :companyID) and  (:email is null or  u.name like %:email% or" +
            "  u.description like %:email%  )")
    List<ItemCategory> findBynameWithoutPagination(Integer status, Integer companyID, String email);

    @Query("SELECT coalesce(max(inv.id), 0) FROM ItemCategory inv")
    Integer getMaxId();
}
