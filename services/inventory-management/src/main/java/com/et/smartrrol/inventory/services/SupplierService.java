package com.et.smartrrol.inventory.services;



import com.et.smartrrol.commondata.models.v0.Supplier;
import com.et.smartrrol.commondata.repo.v0.SupplierRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SupplierService {
    @Autowired
    private  SupplierRepo supplierRepo;

    public SupplierService(SupplierRepo supplierRepo) {
        this.supplierRepo = supplierRepo;
    }

    public void createSupplier(Supplier supplier
    ){
        supplierRepo.save(supplier);
    }

    public List<Supplier> findAll(){
        List<Supplier> all = supplierRepo.findAll();
        return all;
    }

    public void deleteSupplier(Integer id){
        supplierRepo.deleteById(id);
    }

    public Supplier findByid(Integer id){
        return supplierRepo.findById(id).isPresent()?supplierRepo.findById(id).get():null;
    }
}
