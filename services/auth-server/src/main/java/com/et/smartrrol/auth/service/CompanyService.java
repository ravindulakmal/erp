package com.et.smartrrol.auth.service;

import com.et.smartrrol.auth.dto.branch.BranchDTO;
import com.et.smartrrol.auth.dto.company.AddCompanyToUserDTO;
import com.et.smartrrol.auth.dto.company.CompanyDTO;
import com.et.smartrrol.auth.dto.company.CompanyResponceDTO;


import com.et.smartrrol.auth.dto.company.ImageDTO;
import com.et.smartrrol.commondata.models.*;
import com.et.smartrrol.commondata.repo.CompanyRepo;
import com.et.smartrrol.commondata.repo.SubscriptionRepo;
import com.et.smartrrol.commondata.repo.UserRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {
    @Autowired
    private CompanyRepo companyRepo;

    @Autowired
    private SubscriptionRepo subscriptionRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private UserService userService;

    public ApiResponse companyCreateWithRequest(CompanyDTO compa){
        ApiResponse apiResponse = new ApiResponse();
        try {
            Company company = new Company();
            BeanUtils.copyProperties(compa, company);
            company.setStatus(Status.STATUS_ACTIVE.getVal());
            company.setCompanyCode(this.generateCompanyCode(compa));
            company.setSubscription(subscriptionRepo.findById(compa.getSubscriptionId()).get());
            Company save = companyRepo.save(company);
            List<User> userList = this.setUsersToCompany(save, compa.getUserId());
            save.setUserList(userList);
            Company save1 = companyRepo.save(save);
            if (save1 != null) {
                apiResponse.setResponseDesc("COMPANY  CREATED");
                apiResponse.setResponseCode(200);
                apiResponse.setData(save1);
                return apiResponse;
            }
        }catch (Exception e){
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(null);

        }
        return apiResponse;
    }

    public ApiResponse companyUpdateWithRequest(CompanyDTO compa){
        ApiResponse apiResponse = new ApiResponse();
        try {
            Company company = companyRepo.findById(compa.getId()).get();
            BeanUtils.copyProperties(compa, company);
            company.setStatus(Status.STATUS_ACTIVE.getVal());
            company.setCompanyCode(this.generateCompanyCode(compa));
            company.setSubscription(subscriptionRepo.findById(compa.getSubscriptionId()).get());
            Company save = companyRepo.save(company);

            if (save != null) {
                apiResponse.setResponseDesc("COMPANY  UPDATED");
                apiResponse.setResponseCode(200);
                apiResponse.setData(save);
                return apiResponse;
            }
        }catch (Exception e){
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(null);

        }
        return apiResponse;
    }

    private List<User> setUsersToCompany(Company save,Integer userId) {

        User user = userRepo.findById(userId).get();
        List<Company> companies = new ArrayList<>();
        companies.add(save);
        user.getCompanies().add(save);
        User save1 = userRepo.save(user);
        List<User> userList = new ArrayList<>();
        return userList;
    }

    public ApiResponse addUserByCompany(AddCompanyToUserDTO addCompanyToUserDTO){

        ApiResponse apiResponse = new ApiResponse();
        try {

            Company company = companyRepo.findById(addCompanyToUserDTO.getCompanyId()).get();
            List<Company> companies = new ArrayList<>();
            companies.add(company);
            userRepo.findAllById(addCompanyToUserDTO.getUserIds()).forEach(user -> {
                user.setCompanies(companies);
                userRepo.save(user);
            });
            apiResponse.setResponseDesc("COMPANY MEMBERS ADDED");
            apiResponse.setResponseCode(200);
            apiResponse.setData(null);
            return apiResponse;
        }catch (Exception e){
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(null);
        }
        return apiResponse;
    }


    public Integer companyCreate(CompanyDTO compa){

        Company company = new Company();
        BeanUtils.copyProperties(compa, company);
        company.setStatus(Status.STATUS_ACTIVE.getVal());
        company.setCompanyCode(this.generateCompanyCode(compa));
        company.setSubscription(subscriptionRepo.findById(compa.getSubscriptionId()).get());
        Company save = companyRepo.save(company);

        return save.getId();
    }

    public Integer companyImageUpload(ImageDTO imageDTO, Integer compId){
        Integer id = 0;
        try {
            Company company = companyRepo.findById(compId).get();

            company.setImageName(imageDTO.getImageName());
            company.setImageType(imageDTO.getImageType());

            Company save = companyRepo.save(company);
            id=save.getId();
        }catch (Exception e){
            e.printStackTrace();
        }
        return id;
    }

    public ApiResponse findAll(String username, Integer comId) {
        ApiResponse apiResponse = new ApiResponse();

        List<User> userList =new ArrayList<>();

        try{
            List<CompanyResponceDTO> subscriptionDTOS = new ArrayList<>();

            User byUserName = userRepo.findByUserNameAndStatus(username, Status.STATUS_ACTIVE.getVal());


            List<Company> companies = byUserName.getCompanies();
            companies.forEach(company -> {
//
                CompanyResponceDTO companyResponceDTO = new CompanyResponceDTO();
                BeanUtils.copyProperties(company, companyResponceDTO);
                List<BranchDTO> branchDTOList = new ArrayList<>();
                for (Branch branch:
                        company.getBranches()) {
                    BranchDTO branchDTO = new BranchDTO();
                    BeanUtils.copyProperties(branch,branchDTO);
                    branchDTOList.add(branchDTO);
                }


                companyResponceDTO.setBranchDTOList(branchDTOList);
                subscriptionDTOS.add(companyResponceDTO);
            });


            apiResponse.setResponseDesc("COMPANY  FIND ALL");
            apiResponse.setResponseCode(200);
            apiResponse.setData(subscriptionDTOS);
            return apiResponse;
        }catch (Exception e){
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(null);

        }
        return apiResponse;

    }

    private String generateCompanyCode(CompanyDTO companyDTO){
        Integer maxId = companyRepo.getMaxId();
        int i = maxId + 1;
        String code = "CO"+"00"+i;
        return code;
    }

    public ApiResponse filterByCompanyIdAndBranchId(Integer companyId, Integer branchId){
        ApiResponse apiResponse = new ApiResponse();

        try{
            Optional<Company> byId = companyRepo.findById(companyId);
            CompanyResponceDTO companyResponceDTO = new CompanyResponceDTO();
            BeanUtils.copyProperties(byId.get(), companyResponceDTO);
            List<BranchDTO> branchDTOList = new ArrayList<>();
            for (Branch branches:byId.get().getBranches()){
                if (branches.getId()==branchId){
                    BranchDTO branchDTO = new BranchDTO();
                    BeanUtils.copyProperties(branches,branchDTO);
                    branchDTOList.add(branchDTO);
                }
            }
            companyResponceDTO.setBranchDTOList(branchDTOList);
            apiResponse.setResponseDesc("COMPANY  FIND BY ID AND BRANCH");
            apiResponse.setResponseCode(200);
            apiResponse.setData(companyResponceDTO);
            return apiResponse;
        }catch (Exception e){
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(null);

        }
        return apiResponse;
    }
}
