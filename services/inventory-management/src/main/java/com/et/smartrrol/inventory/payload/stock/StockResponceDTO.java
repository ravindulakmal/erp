package com.et.smartrrol.inventory.payload.stock;


import com.et.smartrrol.commondata.models.BaseEntity;
import com.et.smartrrol.inventory.payload.product.ProductResponceDTO;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
public class StockResponceDTO extends BaseEntity {
    private Integer id;

    private BigDecimal plusQty;
    private BigDecimal minusQty;
    private BigDecimal batchAvailableQty;
    private BigDecimal retailPrice;
    private BigDecimal unitPrice;
    private BigDecimal sellingPrice;
    private String reason;
    private String batchNumber;
    private LocalDate localDate;
    private Integer isTransferdEnable;
    private Integer transferdOrNot;
    private Integer grnByproductId;
    private ProductResponceDTO productResponceDTO;
    private List<StockAdjusementDTO> stockAdjusementDTOS;
}
