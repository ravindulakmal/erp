package com.et.smartrrol.paymentservice.service;


import com.et.smartrrol.commondata.models.ApiResponse;
import com.et.smartrrol.commondata.models.Company;
import com.et.smartrrol.commondata.models.Invoice.Invoice;
import com.et.smartrrol.commondata.models.Status;
import com.et.smartrrol.commondata.models.payment.Payment;
import com.et.smartrrol.commondata.models.v0.Customer;
import com.et.smartrrol.commondata.models.v0.CustomerCredited;
import com.et.smartrrol.commondata.repo.CompanyRepo;
import com.et.smartrrol.commondata.repo.invoice.InvoiceRepo;
import com.et.smartrrol.commondata.repo.order.OrderRepo;
import com.et.smartrrol.commondata.repo.payment.PaymentRepo;
import com.et.smartrrol.commondata.repo.payment.PaymentTypeRepo;
import com.et.smartrrol.commondata.repo.v0.CustomerRepo;
import com.et.smartrrol.paymentservice.payload.payment.PaymentDTO;
import com.et.smartrrol.paymentservice.payload.payment.PaymentSearchDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class PaymentService {

    @Autowired
    private PaymentRepo paymentRepo;

    @Autowired
    private InvoiceRepo invoiceRepo;

    @Autowired
    private PaymentTypeRepo paymentTypeRepo;

    @Autowired
    private CustomerRepo customerRepo;

    @Autowired
    private OrderRepo orderRepo;

    @Autowired
    private CompanyRepo companyRepo;


    public ApiResponse paymentCreate(PaymentDTO paymentDTO) {
        ApiResponse apiResponse = new ApiResponse();

        try {
            Invoice invoice = invoiceRepo.findById(paymentDTO.getInvoiceId()).get();
            if (paymentDTO.getPaidAmount().compareTo(BigDecimal.ZERO) == 1) {
                if (paymentDTO.getPaidAmount().compareTo(invoice.getBalanceDue()) == 1) {
                    invoice.setBalanceDue(invoice.getBalanceDue().subtract(paymentDTO.getPaidAmount()));
                    invoice.setPaidAmount(invoice.getPaidAmount().add(paymentDTO.getPaidAmount()));
                    invoiceRepo.save(invoice);
                } else if (paymentDTO.getPaidAmount().compareTo(invoice.getBalanceDue()) == 0) {
                    invoice.setBalanceDue(invoice.getBalanceDue().subtract(paymentDTO.getPaidAmount()));
                    invoice.setPaidAmount(invoice.getPaidAmount().add(paymentDTO.getPaidAmount()));
                    invoice.setIsSettled(Status.SETTLED.getVal());
                    invoiceRepo.save(invoice);
                }
            }
            Payment payment = new Payment();
            payment.setStatus(Status.STATUS_ACTIVE.getVal());
            payment.setCompId(paymentDTO.getCompId());
            payment.setBranchId(paymentDTO.getBranchId());
            payment.setInvoice(invoice);
            payment.setPaidAmount(paymentDTO.getPaidAmount());
            payment.setUserName(paymentDTO.getUserName());
            payment.setPaymentCode(this.generatePaymentCode(companyRepo.findById(paymentDTO.getCompId()).get()));
            payment.setPaymentType(paymentTypeRepo.findById(paymentDTO.getPaymentTypeID()).get());
            payment = paymentRepo.save(payment);
            if (payment.getId() != null) {
                apiResponse.setResponseDesc("PAYMENT ADDED");
                apiResponse.setResponseCode(200);
                apiResponse.setData(payment);
            }


        } catch (Exception e) {
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }
        return apiResponse;
    }


    public ApiResponse paymentDelete(Integer paymentID) {
        ApiResponse apiResponse = new ApiResponse();

        try {
            Payment payment = paymentRepo.findById(paymentID).get();

            Invoice invoice = payment.getInvoice();
            invoice.setPayableAmount(invoice.getPayableAmount().subtract(payment.getPaidAmount()));
            invoice.setBalanceDue(invoice.getBalanceDue().add(payment.getPaidAmount()));
            invoice.setStatus(Status.NOT_SETTLED.getVal());
            invoiceRepo.save(invoice);
            payment.setStatus(Status.STATUS_SUSPENDED.getVal());
            Payment save = paymentRepo.save(payment);
            apiResponse.setResponseDesc("REMOVED PAYMENT");
            apiResponse.setResponseCode(200);
            apiResponse.setData(save);
        } catch (Exception e) {
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }
        return apiResponse;
    }

    public ApiResponse availableCreditedByCustomerID(Integer customerID) {
        ApiResponse apiResponse = new ApiResponse();

        try {
            Customer customer = customerRepo.findById(customerID).get();
            if (customer.getCustomerCredited() != null) {
                List<Invoice> allByOrders_customer_idAndStatus = invoiceRepo.findAllByOrders_Customer_IdAndStatusAndIsSettledAndOrders_IsCredited(customerID,
                        Status.STATUS_ACTIVE.getVal(),
                        Status.NOT_SETTLED.getVal(),
                        Status.STATUS_ACTIVE.getVal());
                BigDecimal bigDecimal = BigDecimal.ZERO;
                for (Invoice invoice : allByOrders_customer_idAndStatus) {
                    bigDecimal.add(invoice.getBalanceDue());
                }

                CustomerCredited customerCredited = customer.getCustomerCredited();
                BigDecimal subtract = customerCredited.getCreditLimit().subtract(bigDecimal);

                apiResponse.setResponseDesc("DUE CREDIT LIMIT OF" + customer.getFirstName() + " " + customer.getLastName());
                apiResponse.setResponseCode(200);
                apiResponse.setData(subtract);
            }

        } catch (Exception e) {
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }
        return apiResponse;
    }


    private String generatePaymentCode(Company company){
        Integer maxId = paymentRepo.getMaxId();
        int i = maxId + 1;
        String code = "PAY"+company.getId()+"00"+i;
        return code;
    }


    public ApiResponse findAllPayments(PaymentSearchDTO paymentSearchDTO){
        ApiResponse apiResponse = new ApiResponse();
        try {
            PageRequest page_req = PageRequest.of(paymentSearchDTO.getPage(), paymentSearchDTO.getCount());

            Page<Payment> all = paymentRepo.findAll(this.getPaymentBySearch(paymentSearchDTO), page_req);
//            List<Payment> customer = paymentRepo.findByname(Status.STATUS_ACTIVE.getVal(),paymentSearchDTO.getBranchId(),
//                    paymentSearchDTO.getCompId(),paymentSearchDTO.getPayment(),page_req);
            apiResponse.setResponseDesc("PAYMENT FIND ALL");
            apiResponse.setResponseCode(200);
            apiResponse.setData(all);
        } catch (Exception e) {
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }
        return apiResponse;
    }


    private Specification<Payment> getPaymentBySearch(PaymentSearchDTO paymentSearchDTO){

        Specification<Payment> ordersSpecification = new Specification<Payment>() {
            @Override
            public Predicate toPredicate(Root<Payment> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                predicates.add(criteriaBuilder.equal(root.get("compId"),paymentSearchDTO.getCompId()));
                predicates.add(criteriaBuilder.equal(root.get("status"),Status.STATUS_ACTIVE.getVal()));
                predicates.add(criteriaBuilder.equal(root.get("branchId"),paymentSearchDTO.getBranchId()));
                Predicate like = criteriaBuilder.like(root.get("invoice").get("orders").get("orderCode"), "%" + paymentSearchDTO.getPayment() + "%");
                Predicate orderCode = criteriaBuilder.like(root.get("paymentCode"), "%" + paymentSearchDTO.getPayment() + "%");
                predicates.add(criteriaBuilder.or(like,orderCode));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };

        return ordersSpecification;

    }

    public ApiResponse findAllPaymentsWithoutPagination(PaymentSearchDTO paymentSearchDTO){
        ApiResponse apiResponse = new ApiResponse();
        try {

            List<Payment> customer = paymentRepo.findBynameWithoutPagination(Status.STATUS_ACTIVE.getVal(),paymentSearchDTO.getBranchId(),
                    paymentSearchDTO.getCompId(),paymentSearchDTO.getPayment());
            apiResponse.setResponseDesc("PAYMENT FIND ALL");
            apiResponse.setResponseCode(200);
            apiResponse.setData(customer);
        } catch (Exception e) {
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }
        return apiResponse;
    }

}
