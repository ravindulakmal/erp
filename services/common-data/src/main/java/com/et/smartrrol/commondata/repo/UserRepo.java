package com.et.smartrrol.commondata.repo;

import com.et.smartrrol.commondata.models.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<User, Integer>, JpaSpecificationExecutor<User> {

        User findByUserNameAndStatus(String username, Integer isActive);

    @Override
    List<User> findAllById(Iterable<Integer> integers);

    Optional<User> findByUserName(String username);

    boolean existsByUserNameAndStatus(String username, Integer isActive);
//    User findByEmailAndStatus(String username, Integer isActive);

//    User findByEmailAndStatus(String username, Integer isActive);

    User findByIdAndUserNameAndOtpAndStatus(Integer mail, String email,String otp,Integer status);

    boolean existsByOtp(String integer);

    boolean existsByIdAndUserName(Integer mail, String email);

    boolean existsByUserName(String emIL);




    List<User> findAllByStatus(Integer status);
//    and (:companyID is null or u.companies. = :companyID)
//    ,List<Integer> companyID
    @Query("Select u from User u where  (:status is null or u.status = :status)  and  (:email is null or  u.userName like %:email% or" +
            "  u.firstname like %:email% or " +
            " u.lastname like %:email% or" +
            "  u.personalMobile like %:email%)")
    List<User> findByname(Integer status,String email);



    @Query("SELECT coalesce(max(inv.id), 0) FROM User inv")
    Integer getMaxId();

}
