package com.et.smartrrol.inventory.services;



import com.et.smartrrol.commondata.models.ApiResponse;
import com.et.smartrrol.commondata.models.Company;
import com.et.smartrrol.commondata.models.Status;
import com.et.smartrrol.commondata.models.inventory.ItemCategory;
import com.et.smartrrol.commondata.repo.CategoryRepo;
import com.et.smartrrol.commondata.repo.CompanyRepo;
import com.et.smartrrol.inventory.payload.category.CategoryCascaderResponseDTO;
import com.et.smartrrol.inventory.payload.category.CategoryRequestDTO;
import com.et.smartrrol.inventory.payload.category.CategoryResponseDTO;
import com.et.smartrrol.inventory.payload.category.CategorySearchDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CategoryService {
    @Autowired
    private CategoryRepo categoryRepo;

    @Autowired
    private CompanyRepo companyRepo;

    @Transactional
    public ApiResponse saveCategory(CategoryRequestDTO categoryRequestDTO) {

        ApiResponse apiResponse = new ApiResponse();
        try {
            ItemCategory itemCategory = new ItemCategory();
            if (categoryRequestDTO.getCategoryId() != null) {
                itemCategory.setItemCategoryId(categoryRequestDTO.getCategoryId());
            }
            itemCategory.setName(categoryRequestDTO.getName());
            itemCategory.setDescription(categoryRequestDTO.getDescription());
            itemCategory.setStatus(Status.STATUS_ACTIVE.getVal());
            itemCategory.setCompId(categoryRequestDTO.getCompId());
            Company company = companyRepo.findById(categoryRequestDTO.getCompId()).get();
            itemCategory.setCode(this.generateCategoryCode(company));
            if (categoryRequestDTO.getParentItemCategoryId() != null) {

//            Optional<ItemCategory> categoryById = findCategoryById(categoryRequestDTO.getParentItemCategoryId());

//            if (categoryById.isPresent()) {
                itemCategory.setParentItemCategoryId(categoryRequestDTO.getParentItemCategoryId());
//            }
            }

            // ADD USER DETAILS
//            itemCategory.setUserName(categoryRequestDTO.getUserName());
//            itemCategory.setBranchId(categoryRequestDTO.getBranchId());

            ItemCategory save = categoryRepo.save(itemCategory);
            Integer itemCategoryId = save.getItemCategoryId();
            apiResponse.setResponseDesc("CATEGORY CREATED");
            apiResponse.setResponseCode(200);
            apiResponse.setData(itemCategoryId);
        }catch (Exception e){
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }
        return apiResponse;
    }

    @Transactional
    public Optional<ItemCategory> findCategoryById(Integer id) {
        return categoryRepo.findById(id);
    }

    @Transactional
    public ApiResponse updateCategory(CategoryRequestDTO categoryRequestDTO) {

        ApiResponse apiResponse = new ApiResponse();
        Optional<ItemCategory> categoryById = findCategoryById(categoryRequestDTO.getCategoryId());

        if (categoryById.isPresent()) {
            ItemCategory itemCategory = categoryById.get();
            if (categoryRequestDTO.getName() != null) {
                itemCategory.setName(categoryRequestDTO.getName());
            }

            if (categoryRequestDTO.getDescription() != null) {
                itemCategory.setDescription(categoryRequestDTO.getDescription());

            }
            itemCategory.setStatus(categoryRequestDTO.getStatus());
            itemCategory.setCompId(categoryRequestDTO.getCompId());
            itemCategory.setCode(categoryRequestDTO.getCode());
            ItemCategory update = categoryRepo.save(itemCategory);
            if (update!=null){
                apiResponse.setResponseDesc("CATEGORY UPDATED");
                apiResponse.setResponseCode(200);
                apiResponse.setData(update);
            }
        } else {
            apiResponse.setResponseDesc("CATEGORY IS NOT AVAILABLE");
            apiResponse.setResponseCode(400);
            apiResponse.setData(null);
        }
        return apiResponse;
    }

    @Transactional
    public Integer changeCategoryStatus(Integer id, String status) {
        Optional<ItemCategory> categoryById = findCategoryById(id);

        if (categoryById.isPresent()) {
            ItemCategory itemCategory = categoryById.get();

            List<ItemCategory> allByParentItemCategoryId = categoryRepo.findAllByParentItemCategoryIdAndStatus(id, 1);

            if (allByParentItemCategoryId.size() > 0) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Have Child Categories!!");
            } else {

                if (itemCategory.getParentItemCategoryId() != null && Integer.parseInt(status) == 0) {

                    Optional<ItemCategory> byId = categoryRepo.findById(itemCategory.getParentItemCategoryId());
                    if (byId.isPresent()) {
                        ItemCategory parentItemCategory = byId.get();
                        if (parentItemCategory.getStatus() == 0) {
                            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Parent Category is Disable!!");
                        }
                    }
                }

                if (Integer.parseInt(status) == 0) {
                    itemCategory.setStatus(1);
                } else {
                    itemCategory.setStatus(0);
                }

            }

            ItemCategory updateStatus = categoryRepo.save(itemCategory);
            return updateStatus.getItemCategoryId();
        } else {
            return null;
        }
    }

    @Transactional
    public ApiResponse getAllCategories(CategorySearchDTO categorySearchDTO) {

        ApiResponse apiResponse = new ApiResponse();

        PageRequest page_req = PageRequest.of(categorySearchDTO.getPage(), categorySearchDTO.getCount());

        Page<ItemCategory> all = categoryRepo.findAll(this.getCategoryBySearch(categorySearchDTO), page_req);


        apiResponse.setResponseDesc("GET CATEGORY ALL");
        apiResponse.setResponseCode(200);
        apiResponse.setData(all);
        return apiResponse;
    }

    private Specification<ItemCategory> getCategoryBySearch(CategorySearchDTO searchBrandDTO){

        Specification<ItemCategory> ordersSpecification = new Specification<ItemCategory>() {
            @Override
            public Predicate toPredicate(Root<ItemCategory> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                predicates.add(criteriaBuilder.equal(root.get("compId"),searchBrandDTO.getCompId()));
                predicates.add(criteriaBuilder.equal(root.get("status"),Status.STATUS_ACTIVE.getVal()));
                predicates.add(criteriaBuilder.equal(root.get("branchId"),searchBrandDTO.getBranchId()));
                Predicate like = criteriaBuilder.like(root.get("description"), "%" + searchBrandDTO.getName() + "%");
                Predicate orderCode = criteriaBuilder.like(root.get("name"), "%" + searchBrandDTO.getName() + "%");
                predicates.add(criteriaBuilder.or(like,orderCode));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };

        return ordersSpecification;

    }


    @Transactional
    public ApiResponse findBynameWithoutPagination(CategorySearchDTO categorySearchDTO) {

        ApiResponse apiResponse = new ApiResponse();
        List<ItemCategory> all;
//        PageRequest page_req = PageRequest.of(categorySearchDTO.getPage(), categorySearchDTO.getCount());


        all = categoryRepo.findBynameWithoutPagination(Status.STATUS_ACTIVE.getVal(),categorySearchDTO.getCompId(),categorySearchDTO.getName());


        List<CategoryResponseDTO> categoryResponseDTOS = new ArrayList<>();

        for (ItemCategory itemCategory : all) {
            CategoryResponseDTO categoryResponseDTO = new CategoryResponseDTO();
            categoryResponseDTO.setCategoryId(itemCategory.getItemCategoryId());
            categoryResponseDTO.setName(itemCategory.getName());
            categoryResponseDTO.setDescription(itemCategory.getDescription());
            categoryResponseDTO.setStatus(itemCategory.getStatus());
            categoryResponseDTO.setCode(itemCategory.getCode());

            if (itemCategory.getParentItemCategoryId() != 0) {
                ItemCategory byId = categoryRepo.findById(itemCategory.getParentItemCategoryId()).get();
                if (byId != null) {
                    categoryResponseDTO.setParentItemCategoryId(byId.getItemCategoryId());
                    categoryResponseDTO.setParentItemCategoryName(byId.getName());
                }
            }
            categoryResponseDTOS.add(categoryResponseDTO);
        }
        apiResponse.setResponseDesc("GET CATEGORY ALL");
        apiResponse.setResponseCode(200);
        apiResponse.setData(categoryResponseDTOS);
        return apiResponse;
    }


    public ApiResponse deleteroductCategory(Integer id){
        ApiResponse apiResponse = new ApiResponse();
        ItemCategory itemCategory = categoryRepo.findById(id).get();
        itemCategory.setStatus(Status.STATUS_DISABLED.getVal());
        categoryRepo.save(itemCategory);

        apiResponse.setResponseDesc("FIND AND SEARCH ITEM CATEGORY");
        apiResponse.setResponseCode(200);
        apiResponse.setData(id);
        return apiResponse;
    }

    private List<CategoryCascaderResponseDTO> iterateCascaderDetails(List<ItemCategory> all, List<CategoryCascaderResponseDTO> categoryCascaderResponseDTOS) {
        for (ItemCategory itemCategory : all) {
            if (itemCategory.getStatus() == 1) {
                CategoryCascaderResponseDTO categoryCascaderResponseDTO = new CategoryCascaderResponseDTO();
                categoryCascaderResponseDTO.setValue(itemCategory.getItemCategoryId() + "");
                categoryCascaderResponseDTO.setLabel(itemCategory.getName());


                List<CategoryCascaderResponseDTO> categoryChildrenCascaderResponseDTOS = recursiveChildrenCategory(itemCategory.getItemCategoryId());
                if (categoryChildrenCascaderResponseDTOS.size() > 0) {
                    categoryCascaderResponseDTO.setChildren(categoryChildrenCascaderResponseDTOS);
                }

                categoryCascaderResponseDTOS.add(categoryCascaderResponseDTO);
            }
        }

        return categoryCascaderResponseDTOS;
    }

    @Transactional
    public List<CategoryCascaderResponseDTO> getAllCategoriesForCascader() {
        List<ItemCategory> all = categoryRepo.findAllByParentItemCategoryIdNull();

        List<CategoryCascaderResponseDTO> categoryCascaderResponseDTO = new ArrayList<>();

        return iterateCascaderDetails(all, categoryCascaderResponseDTO);
    }

    private List<CategoryCascaderResponseDTO> recursiveChildrenCategory(int itemCategoryId) {

        List<ItemCategory> all = categoryRepo.findAllByParentItemCategoryId(itemCategoryId);

        List<CategoryCascaderResponseDTO> categoryCascaderResponseDTO = new ArrayList<>();

        return iterateCascaderDetails(all, categoryCascaderResponseDTO);

    }

    private String generateCategoryCode(Company company){
        Integer maxId = categoryRepo.getMaxId();
        int i = maxId + 1;
        String code = "CAT"+company.getId()+"00"+i;
        return code;
    }

//    private List<CategoryCascaderResponseDTO> recursiveParentCategory(ItemCategory itemCategory) {
//        List<CategoryCascaderResponseDTO> CategoryCascaderResponseDTO = new ArrayList<>();
//
//        CategoryCascaderResponseDTO categoryCascaderResponseDTO = new CategoryCascaderResponseDTO();
//        categoryCascaderResponseDTO.setValue(itemCategory.getItemCategoryId() + "");
//        categoryCascaderResponseDTO.setLabel(itemCategory.getName());
//
//        if (itemCategory.getParentItemCategoryId() != null) {
//            List<CategoryCascaderResponseDTO> categoryCascaderResponseDTOS = recursiveParentCategory(itemCategory.getParentItemCategoryId());
//            categoryCascaderResponseDTO.setChildren(categoryCascaderResponseDTOS);
//        }
//
//        CategoryCascaderResponseDTO.add(categoryCascaderResponseDTO);
//
//
//        return CategoryCascaderResponseDTO;
//
//    }
}
