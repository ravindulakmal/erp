package com.et.smartrrol.commondata.repo;

import com.et.smartrrol.commondata.models.Subscription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubscriptionRepo extends JpaRepository<Subscription,Integer>, JpaSpecificationExecutor<Subscription> {


    List<Subscription> findAllByStatus(Integer status);
}
