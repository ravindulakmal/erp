package com.et.smartrrol.commondata.models.inventory;



import com.et.smartrrol.commondata.models.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;


@Getter
@Setter
@ToString
@Entity
@Table(name = "item_category")
public class ItemCategory extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer itemCategoryId;
    private String name;
    private String description;
    private String code;
    private Integer status;
//    @JsonIgnore
//    @OneToMany(mappedBy = "itemCategory", cascade = CascadeType.ALL)
//    private List<MetaCategory> metaCategories;
    private Integer parentItemCategoryId;


//adum
//    gents ladies

}
