package com.et.smartrrol.inventory.services;


import com.et.smartrrol.commondata.models.ApiResponse;
import com.et.smartrrol.commondata.models.Branch;
import com.et.smartrrol.commondata.models.Company;
import com.et.smartrrol.commondata.models.Status;
import com.et.smartrrol.commondata.models.stock.Stock;
import com.et.smartrrol.commondata.models.stock.StockAdjusment;
import com.et.smartrrol.commondata.models.stock.StockTransfer;
import com.et.smartrrol.commondata.repo.BranchRepo;
import com.et.smartrrol.commondata.repo.CompanyRepo;
import com.et.smartrrol.commondata.repo.stock.StockAdjusmentRepo;
import com.et.smartrrol.commondata.repo.stock.StockRepo;
import com.et.smartrrol.commondata.repo.stock.StockTransferRepo;
import com.et.smartrrol.commondata.repo.v0.ProductRepo;
import com.et.smartrrol.inventory.payload.category.BrandDto;
import com.et.smartrrol.inventory.payload.product.ProductCategoryDTO;
import com.et.smartrrol.inventory.payload.product.ProductResponceDTO;
import com.et.smartrrol.inventory.payload.stock.*;
import com.et.smartrrol.inventory.payload.supplier.SupplierDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class StockService {

    @Autowired
    private ProductRepo productRepo;

    @Autowired
    private StockRepo stockRepo;

    @Autowired
    private StockAdjusmentRepo stockAdjusmentRepo;

    @Autowired
    private CompanyRepo companyRepo;

    @Autowired
    private StockTransferRepo stockTransferRepo;

    @Autowired
    private BranchRepo branchRepo;


    public ApiResponse findAllByProductWithoutPagination(StockSearchDTO stockSearchDTO) {

        ApiResponse apiResponse = new ApiResponse();
        List<StockResponceDTO> stockSearchDTOS = new ArrayList<>();

        try {
            PageRequest page_req = PageRequest.of(stockSearchDTO.getPage(), stockSearchDTO.getCount());
            productRepo.findBynameWithoutPagination(Status.STATUS_ACTIVE.getVal(), stockSearchDTO.getCompId(), stockSearchDTO.getName()).stream().forEach(product -> {

                stockRepo.findAllByStatusAndCompIdAndProduct_Id(Status.STATUS_ACTIVE.getVal(), stockSearchDTO.getCompId(), product.getId()).stream().forEach(stock -> {
                    StockResponceDTO stockResponceDTO = new StockResponceDTO();

                    BeanUtils.copyProperties(stock, stockResponceDTO);
                    ProductResponceDTO productDTO = new ProductResponceDTO();
                    BeanUtils.copyProperties(product, productDTO);
//                 BRAND TO DTO
                    BrandDto brandDto = new BrandDto();
                    BeanUtils.copyProperties(product.getBrand(), brandDto);
                    productDTO.setBrandDto(brandDto);
//                 CATEGORY TO DTO
                    ProductCategoryDTO categoryResponseDTO = new ProductCategoryDTO();

                    BeanUtils.copyProperties(product.getProductCategory(), categoryResponseDTO);
                    productDTO.setProductCategoryDTO(categoryResponseDTO);
//                 SUPPLIER TO DTO
                    SupplierDTO supplierDTO = new SupplierDTO();
                    BeanUtils.copyProperties(product.getSupplier(), supplierDTO);
                    productDTO.setSupplierDTO(supplierDTO);
                    stockResponceDTO.setProductResponceDTO(productDTO);
                    stockResponceDTO.setBatchAvailableQty(stock.getPlusQty().subtract(stock.getMinusQty()));
                    List<StockAdjusment> stockAdjusments = stock.getStockAdjusments();
                    List<StockAdjusementDTO> stockAdjusementDTOS = new ArrayList<>();
                    for (StockAdjusment adjusment : stockAdjusments) {
                        StockAdjusementDTO stockAdjusementDTO = new StockAdjusementDTO();
                        BeanUtils.copyProperties(adjusment, stockAdjusementDTO);
                        stockAdjusementDTOS.add(stockAdjusementDTO);
                    }
                    stockResponceDTO.setStockAdjusementDTOS(stockAdjusementDTOS);
                    stockSearchDTOS.add(stockResponceDTO);
                });
//                System.out.println();
//                if (stockSearchDTOS.size() != 0) {
//                    StockResponceDTO stockResponceDTO = stockSearchDTOS.get(0);
//                    stockResponceDTO.getPlusQty().add(product.getInitialQty());
//                }


            });
            apiResponse.setResponseDesc("STOCK BY PRODUCT FIND AND SEARCH");
            apiResponse.setResponseCode(200);
            apiResponse.setData(stockSearchDTOS);
        } catch (Exception e) {
            e.printStackTrace();
//            apiResponse.setResponseDesc(e.getMessage());
//            apiResponse.setResponseCode(500);
//            apiResponse.setData(e);
        }
        return apiResponse;
    }

    public ApiResponse findAllStock(StockSearchDTO stockSearchDTO) {

        ApiResponse apiResponse = new ApiResponse();
        List<StockResponceDTO> stockSearchDTOS = new ArrayList<>();

        try {
            PageRequest page_req = PageRequest.of(stockSearchDTO.getPage(), stockSearchDTO.getCount());

            Page<Stock> all = stockRepo.findAll(this.getStockBySearch(stockSearchDTO), page_req);

            apiResponse.setResponseDesc("STOCK BY PRODUCT FIND AND SEARCH");
            apiResponse.setResponseCode(200);
            apiResponse.setData(all);
        } catch (Exception e) {
//            e.printStackTrace();
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }
        return apiResponse;
    }

    private Specification<Stock> getStockBySearch(StockSearchDTO stockSearchDTO){

        Specification<Stock> ordersSpecification = new Specification<Stock>() {
            @Override
            public Predicate toPredicate(Root<Stock> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                predicates.add(criteriaBuilder.equal(root.get("compId"),stockSearchDTO.getCompId()));
                predicates.add(criteriaBuilder.equal(root.get("status"),Status.STATUS_ACTIVE.getVal()));
                predicates.add(criteriaBuilder.equal(root.get("branchId"),stockSearchDTO.getBranchId()));
                Predicate like = criteriaBuilder.like(root.get("product").get("productName"),"%" + stockSearchDTO.getName() + "%" );
                Predicate orderCode = criteriaBuilder.like(root.get("product").get("productCode"), "%" + stockSearchDTO.getName() + "%");
                predicates.add(criteriaBuilder.or(like,orderCode));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };

        return ordersSpecification;

    }

    public ApiResponse AvailableQTyByProduct(StockSearchDTO stockSearchDTO) {

        ApiResponse apiResponse = new ApiResponse();
        List<StockResponceDTO> stockSearchDTOS = new ArrayList<>();

        try {

            PageRequest page_req = PageRequest.of(stockSearchDTO.getPage(), stockSearchDTO.getCount());

            productRepo.findByname(Status.STATUS_ACTIVE.getVal(), stockSearchDTO.getCompId(), stockSearchDTO.getName(),page_req).stream().forEach(product -> {

                List<Stock> allByStatusAndAndCompIdAndProduct_id = stockRepo.findAllByStatusAndCompIdAndProduct_Id(Status.STATUS_ACTIVE.getVal(), stockSearchDTO.getCompId(), product.getId());
                BigDecimal balance = BigDecimal.ZERO;
                for (Stock stock :
                        allByStatusAndAndCompIdAndProduct_id) {
                    balance.add(stock.getPlusQty()).subtract(stock.getMinusQty());
                }

                AvailableQtyDTO availableQtyDTO = new AvailableQtyDTO();
                ProductResponceDTO productDTO = new ProductResponceDTO();
                BeanUtils.copyProperties(product, productDTO);
//                 BRAND TO DTO
                BrandDto brandDto = new BrandDto();
                BeanUtils.copyProperties(product.getBrand(), brandDto);
                productDTO.setBrandDto(brandDto);
//                 CATEGORY TO DTO
                ProductCategoryDTO categoryResponseDTO = new ProductCategoryDTO();

                BeanUtils.copyProperties(product.getProductCategory(), categoryResponseDTO);
                productDTO.setProductCategoryDTO(categoryResponseDTO);
//                 SUPPLIER TO DTO
                SupplierDTO supplierDTO = new SupplierDTO();
                BeanUtils.copyProperties(product.getSupplier(), supplierDTO);
                productDTO.setSupplierDTO(supplierDTO);

                availableQtyDTO.setProductResponceDTO(productDTO);
                availableQtyDTO.setBalance(balance);
                apiResponse.setResponseDesc("AVAILABLE STOCK QTY BY PRODUCT  FIND AND SEARCH");
                apiResponse.setResponseCode(200);
                apiResponse.setData(balance);
            });

        } catch (Exception e) {
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }
        return apiResponse;
    }

    public ApiResponse stockAdjustMeant(StockAdjusementDTO stockAdjusementDTO) {
        ApiResponse apiResponse = new ApiResponse();

        try {
            StockAdjusment stockAdjusment = new StockAdjusment();
            BeanUtils.copyProperties(stockAdjusementDTO, stockAdjusment);
            Stock stock = stockRepo.findById(stockAdjusementDTO.getStockId()).get();
            stock.setPlusQty(stockAdjusementDTO.getNewQty());
            Stock save = stockRepo.save(stock);
            stockAdjusment.setStock(save);
            stockAdjusment.setDate(LocalDate.now());
            stockAdjusment.setStatus(Status.STATUS_ACTIVE.getVal());
            StockAdjusment save1 = stockAdjusmentRepo.save(stockAdjusment);

            if (save1 != null) {
                apiResponse.setResponseDesc("STOCK ADJUSTMENTS ");
                apiResponse.setResponseCode(200);
                apiResponse.setData(save1);
            }

        } catch (Exception e) {
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }

        return apiResponse;
    }

    public ApiResponse stockTransfer(StockTransferDTO stockTransferDTO) {
        ApiResponse apiResponse = new ApiResponse();

        try {
            Stock stock = stockRepo.findById(stockTransferDTO.getTransferOldStockId()).get();
            stock.setMinusQty(stock.getMinusQty().add(stockTransferDTO.getTransferQty()));
            stockRepo.save(stock);

            Stock st = new Stock();
            st.setTransferdOrNot(Status.TRANSFERED.getVal());
            st.setPlusQty(stockTransferDTO.getTransferQty());
            st.setSellingPrice(stock.getSellingPrice());
            st.setUnitPrice(stock.getUnitPrice());
            st.setRetailPrice(st.getRetailPrice());
            st.setGrnByproductId(stock.getGrnByproductId());
            st.setReason("TRANSFERD STOCK");
            st.setUserName(stockTransferDTO.getUserName());
            st.setBranchId(stockTransferDTO.getNewBranch());
            st.setCompId(stockTransferDTO.getNewCompany());
            st.setStatus(Status.STATUS_ACTIVE.getVal());
            st.setBatchNumber(this.generateBatchNumber(companyRepo.findById(stockTransferDTO.getNewCompany()).get()));
            Stock save = stockRepo.save(st);
            if (save != null) {
                StockTransfer stockTransfer = new StockTransfer();
                stockTransfer.setStock(stock);
                stockTransfer.setNewBatchStock(save.getId());
                stockTransfer.setNewBranch(stockTransferDTO.getNewBranch());
                stockTransfer.setNewCompany(stockTransferDTO.getNewCompany());
                stockTransfer.setOldCompany(stockTransferDTO.getOldCompany());

                stockTransfer.setOldBranch(stock.getBranchId());
                stockTransfer.setTransferQty(stockTransferDTO.getTransferQty());
                stockTransfer.setBranchId(stockTransferDTO.getBranchId());
                stockTransfer.setCompId(stockTransferDTO.getCompId());
                stockTransfer.setStatus(Status.STATUS_ACTIVE.getVal());
                StockTransfer save1 = stockTransferRepo.save(stockTransfer);
                if (save1 != null) {
                    apiResponse.setResponseDesc("STOCK TRANSFER TO " + stockTransfer.getNewBranch());
                    apiResponse.setResponseCode(200);
                    apiResponse.setData(save1);
                }
//                stockTransfer.set(Status.STATUS_ACTIVE.getVal());

            }

        } catch (Exception e) {
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }
        return apiResponse;
    }

    private String generateBatchNumber(Company company) {
        Integer maxId = stockRepo.getMaxId();
        int i = maxId + 1;
        String code = "BATCH" + company.getId() + "00" + i;
        return code;
    }


    public ApiResponse findAllAvailableBatchStockByProduct(StockSearchDTO stockSearchDTO){
        ApiResponse apiResponse = new ApiResponse();
        List<StockResponceDTO> stockSearchDTOS = new ArrayList<>();

        try {

            PageRequest page_req = PageRequest.of(stockSearchDTO.getPage(), stockSearchDTO.getCount());

            productRepo.findByname(Status.STATUS_ACTIVE.getVal(), stockSearchDTO.getCompId(), stockSearchDTO.getName(),page_req).stream().forEach(product -> {

                stockRepo.findAllByStatusAndCompIdAndProduct_Id(Status.STATUS_ACTIVE.getVal(), stockSearchDTO.getCompId(), product.getId(),page_req).stream().forEach(stock -> {
                    StockResponceDTO stockResponceDTO = new StockResponceDTO();

                    BeanUtils.copyProperties(stock, stockResponceDTO);
                    ProductResponceDTO productDTO = new ProductResponceDTO();
                    BeanUtils.copyProperties(product, productDTO);
//                 BRAND TO DTO
                    BrandDto brandDto = new BrandDto();
                    BeanUtils.copyProperties(product.getBrand(), brandDto);
                    productDTO.setBrandDto(brandDto);
//                 CATEGORY TO DTO
                    ProductCategoryDTO categoryResponseDTO = new ProductCategoryDTO();

                    BeanUtils.copyProperties(product.getProductCategory(), categoryResponseDTO);
                    productDTO.setProductCategoryDTO(categoryResponseDTO);
//                 SUPPLIER TO DTO
                    SupplierDTO supplierDTO = new SupplierDTO();
                    BeanUtils.copyProperties(product.getSupplier(), supplierDTO);
                    productDTO.setSupplierDTO(supplierDTO);
                    stockResponceDTO.setProductResponceDTO(productDTO);
                    stockResponceDTO.setBatchAvailableQty(stock.getPlusQty().subtract(stock.getMinusQty()));
                    List<StockAdjusment> stockAdjusments = stock.getStockAdjusments();
                    List<StockAdjusementDTO> stockAdjusementDTOS = new ArrayList<>();
                    for (StockAdjusment adjusment : stockAdjusments) {
                        StockAdjusementDTO stockAdjusementDTO = new StockAdjusementDTO();
                        BeanUtils.copyProperties(adjusment, stockAdjusementDTO);
                        stockAdjusementDTOS.add(stockAdjusementDTO);
                    }

                    if (stockResponceDTO.getBatchAvailableQty().compareTo(BigDecimal.ZERO)==1) {

                        stockResponceDTO.setStockAdjusementDTOS(stockAdjusementDTOS);
                    }
                    stockSearchDTOS.add(stockResponceDTO);
                });
//                System.out.println();
//                if (stockSearchDTOS.size() != 0) {
//                    StockResponceDTO stockResponceDTO = stockSearchDTOS.get(0);
//                    stockResponceDTO.getPlusQty().add(product.getInitialQty());
//                }


            });
            apiResponse.setResponseDesc("STOCK BY PRODUCT FIND AND SEARCH");
            apiResponse.setResponseCode(200);
            apiResponse.setData(stockSearchDTOS);
        } catch (Exception e) {
            e.printStackTrace();
//            apiResponse.setResponseDesc(e.getMessage());
//            apiResponse.setResponseCode(500);
//            apiResponse.setData(e);
        }
        return apiResponse;
    }

    private StockResponceDTO generateEntityToDTO(Stock stock){
        StockResponceDTO stockResponceDTO = new StockResponceDTO();

        BeanUtils.copyProperties(stock, stockResponceDTO);
        ProductResponceDTO productDTO = new ProductResponceDTO();
        BeanUtils.copyProperties(stock.getProduct(), productDTO);
//                 BRAND TO DTO
        BrandDto brandDto = new BrandDto();
        BeanUtils.copyProperties(stock.getProduct().getBrand(), brandDto);
        productDTO.setBrandDto(brandDto);
//                 CATEGORY TO DTO
        ProductCategoryDTO categoryResponseDTO = new ProductCategoryDTO();

        BeanUtils.copyProperties(stock.getProduct().getProductCategory(), categoryResponseDTO);
        productDTO.setProductCategoryDTO(categoryResponseDTO);
//                 SUPPLIER TO DTO
        SupplierDTO supplierDTO = new SupplierDTO();
        BeanUtils.copyProperties(stock.getProduct().getSupplier(), supplierDTO);
        productDTO.setSupplierDTO(supplierDTO);
        stockResponceDTO.setProductResponceDTO(productDTO);
        if (stock.getMinusQty()!=null && stock.getPlusQty()!=null) {
            stockResponceDTO.setBatchAvailableQty(stock.getPlusQty().subtract(stock.getMinusQty()));
        }
//        else if (stock.getPlusQty()==null){
////            stockResponceDTO.setBatchAvailableQty(stock.getMinusQty());
//        }
        List<StockAdjusment> stockAdjusments = stock.getStockAdjusments();
        List<StockAdjusementDTO> stockAdjusementDTOS = new ArrayList<>();
        for (StockAdjusment adjusment : stockAdjusments) {
            StockAdjusementDTO stockAdjusementDTO = new StockAdjusementDTO();
            BeanUtils.copyProperties(adjusment, stockAdjusementDTO);
            stockAdjusementDTOS.add(stockAdjusementDTO);
        }
        stockResponceDTO.setStockAdjusementDTOS(stockAdjusementDTOS);
        return stockResponceDTO;
    }

    private CompanyResponceDTO companytoDto(Company company){
        CompanyResponceDTO subscriptionDTO = new CompanyResponceDTO();
        BeanUtils.copyProperties(company, subscriptionDTO);
        List<BranchDTO> branchDTOList = new ArrayList<>();
        for (Branch branch:
                company.getBranches()) {
            BranchDTO branchDTO = new BranchDTO();
            BeanUtils.copyProperties(branch,branchDTO);
            branchDTOList.add(branchDTO);
        }
        return subscriptionDTO;
    }

    private BranchDTO branchtoDto(Branch branch){
        BranchDTO branchDTO1 = new BranchDTO();

        BeanUtils.copyProperties(branch,branchDTO1);
        branchDTO1.setCompanyId(branch.getCompany().getId());
        return branchDTO1;
    }

    public ApiResponse findAllStockTransfer(StockSearchDTO stockSearchDTO) {
        ApiResponse apiResponse = new ApiResponse();

        List<StockTransferResponceTODTO> stockTransferResponceTODTOS =new ArrayList<>();


        PageRequest page_req = PageRequest.of(stockSearchDTO.getPage(), stockSearchDTO.getCount());
        try {
            List<StockTransfer> allByStatusAndOldCompanyAndOldBranch = stockTransferRepo.findAllByStatusAndOldCompanyAndOldBranch(Status.STATUS_ACTIVE.getVal(),
                    stockSearchDTO.getCompId(),
                    stockSearchDTO.getBranchId(),page_req);
            for (StockTransfer stockTransfer : allByStatusAndOldCompanyAndOldBranch
            ) {
                StockTransferResponceTODTO stockTransferResponceTODTO = new StockTransferResponceTODTO();
                stockTransferResponceTODTO.setTransferQty(stockTransfer.getTransferQty());
                stockTransferResponceTODTO.setId(stockTransfer.getId());
                stockTransferResponceTODTO.setNewStock(this.generateEntityToDTO(stockRepo.findById(stockTransfer.getNewBatchStock()).get()));
                stockTransferResponceTODTO.setOldStock(this.generateEntityToDTO(stockTransfer.getStock()));
                stockTransferResponceTODTO.setNewCompany(this.companytoDto(companyRepo.findById(stockTransfer.getNewCompany()).get()));
                stockTransferResponceTODTO.setOldCompany(this.companytoDto(companyRepo.findById(stockTransfer.getOldCompany()).get()));
                stockTransferResponceTODTO.setNewBranch(this.branchtoDto(branchRepo.findById(stockTransfer.getNewBranch()).get()));
                stockTransferResponceTODTO.setOldBranch(this.branchtoDto(branchRepo.findById(stockTransfer.getOldBranch()).get()));
                stockTransferResponceTODTOS.add(stockTransferResponceTODTO);
            }
            apiResponse.setResponseDesc("FIND ALL STOCK TRANSFER");
            apiResponse.setResponseCode(200);
            apiResponse.setData(stockTransferResponceTODTOS);
        }catch (Exception e){
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }return apiResponse;

    }

    public ApiResponse findAllStokAdjusement(StockSearchDTO stockSearchDTO){
        ApiResponse apiResponse = new ApiResponse();

        try{
            List<StockAdjusementResponceDTO> stockAdjusementResponceDTOS = new ArrayList<>();
//            PageRequest page_req = PageRequest.of(stockSearchDTO.getPage(), stockSearchDTO.getCount());
            PageRequest page_req = PageRequest.of(0, 10);
            for (StockAdjusment stockAdjusment : stockAdjusmentRepo.findAllByStatusAndCompIdAndBranchId(Status.STATUS_ACTIVE.getVal(),
                    stockSearchDTO.getCompId(),
                    stockSearchDTO.getBranchId(),page_req)) {
                StockAdjusementResponceDTO stockAdjusementResponceDTO = new StockAdjusementResponceDTO();
                BeanUtils.copyProperties(stockAdjusment,stockAdjusementResponceDTO);
                stockAdjusementResponceDTO.setStockResponceDTO(this.generateEntityToDTO(stockAdjusment.getStock()));
                stockAdjusementResponceDTOS.add(stockAdjusementResponceDTO);
            }

            apiResponse.setResponseDesc("FIND ALL STOCK TRANSFER");
            apiResponse.setResponseCode(200);
            apiResponse.setData(stockAdjusementResponceDTOS);
        }catch (Exception e){
            e.printStackTrace();
//            apiResponse.setResponseDesc(e.getMessage());
//            apiResponse.setResponseCode(500);
//            apiResponse.setData(e);
        }return apiResponse;

    }

}
