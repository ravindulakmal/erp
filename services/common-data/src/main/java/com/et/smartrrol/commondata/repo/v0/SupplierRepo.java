package com.et.smartrrol.commondata.repo.v0;

import com.et.smartrrol.commondata.models.v0.Supplier;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SupplierRepo extends JpaRepository<Supplier,Integer>, JpaSpecificationExecutor<Supplier> {
    @Query("Select u from Supplier u where  (:status is null or u.status = :status) and (:companyID is null or u.compId = :companyID) and  (:email is null or  u.email like %:email% or" +
            "  u.fullName like %:email% or " +
            " u.emergencyMobileNo like %:email% or" +
            "  u.mobileNo like %:email% or" +
            " u.city like %:email% )")
    List<Supplier> findByname(Integer status, Integer companyID, String email, Pageable pageable);

    @Query("Select u from Supplier u where  (:status is null or u.status = :status) and (:companyID is null or u.compId = :companyID) and  (:email is null or  u.email like %:email% or" +
            "  u.fullName like %:email% or " +
            " u.emergencyMobileNo like %:email% or" +
            "  u.mobileNo like %:email% or" +
            " u.city like %:email% )")
    List<Supplier> findBynameWithoutPagination(Integer status, Integer companyID, String email);

    @Query("SELECT coalesce(max(inv.id), 0) FROM Supplier inv")
    Integer getMaxId();
}
