package com.et.smartrrol.inventory.payload.grn;

import lombok.Data;

@Data
public class SearchgrnDTO {
    private Integer compId;
    private String name;
    private String userName;
    private Integer branchID;
    private Integer page;
    private Integer count;
}
