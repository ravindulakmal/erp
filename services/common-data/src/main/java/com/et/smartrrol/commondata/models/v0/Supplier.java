package com.et.smartrrol.commondata.models.v0;


import com.et.smartrrol.commondata.models.BaseEntity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Getter
@Setter
public class Supplier extends BaseEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String fullName,code;
    private String email;
    //    @OneToOne(cascade = CascadeType.ALL)
//    @JoinColumn(unique = true)
//    private ContactDetails contactDetails;
    private String addLine1;
    private String addLine2;
    private String city;
    private String mobileNo;
    private String emergencyMobileNo;
    private Integer isCredited;
    @JsonIgnore
    @OneToMany(mappedBy = "supplier", cascade = CascadeType.ALL)
    private List<Product> products;
    @OneToOne(mappedBy = "supplier",fetch=FetchType.LAZY)
    private SupplierCredited supplierCredited;

    @JsonIgnore
    @OneToMany(mappedBy = "supplier", cascade = CascadeType.ALL)
    private List<Grn> grns;
}
