package com.et.smartrrol.inventory.payload.supplier;


import com.et.smartrrol.commondata.models.BaseEntity;
import lombok.Data;

@Data
public class SupplierDTO extends BaseEntity {
    private Integer id;
    //    private Integer status;
//    private BigDecimal creditLimit;
//    private BigDecimal paymentTerms;
//    private Integer compId;
    private Integer isCredited;
    private String fullName,code;
    private String email;
    private SupplierCreditedDTO supplierCreditedDTO;
    //    @OneToOne(cascade = CascadeType.ALL)
//    @JoinColumn(unique = true)
//    private ContactDetails contactDetails;
    private String addLine1;
    private String addLine2;
    private String city;
    private String mobileNo;
    private String emergencyMobileNo;

}
