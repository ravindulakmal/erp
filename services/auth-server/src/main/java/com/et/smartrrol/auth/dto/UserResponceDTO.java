package com.et.smartrrol.auth.dto;

import com.et.smartrrol.auth.dto.company.CompanyResponceDTO;


import com.et.smartrrol.commondata.models.BaseEntity;
import lombok.Data;

import java.util.List;

@Data
public class UserResponceDTO extends BaseEntity {
    private Integer id;
    private Integer status;

    private Integer userIdWhoseCreate;
    private Integer defaultCompanyId;
    private Integer defaultBranchId;
    private String code;
    private String password;
    private String email;
    private String title,firstname, lastname,
            personalMobile,
            sex;
    private List<RoleDTO> roleDTOS;
    private List<CompanyResponceDTO> companyDTOS;
}
