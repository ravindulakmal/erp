package com.et.smartrrol.commondata.repo;

import com.et.smartrrol.commondata.models.Function;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface FunctionRepo extends JpaRepository<Function,Integer>, JpaSpecificationExecutor<Function> {
}
