package com.et.smartrrol.inventory.payload.stock;


import com.et.smartrrol.commondata.models.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class StockAdjusementDTO extends BaseEntity {
    private Integer id;
    private LocalDate date;
    private Integer stockAdjusmentTypeIsMinusOrPlus;
    private BigDecimal newQty;
    private BigDecimal qtyBalance;
    private Integer stockId;
}
