package com.et.smartrrol.inventory.services;

import com.et.smartrrol.commondata.models.ApiResponse;
import com.et.smartrrol.commondata.models.Company;
import com.et.smartrrol.commondata.models.Status;
import com.et.smartrrol.commondata.models.v0.Brand;
import com.et.smartrrol.commondata.repo.CompanyRepo;
import com.et.smartrrol.commondata.repo.v0.BrandRepo;
import com.et.smartrrol.inventory.payload.category.BrandDto;
import com.et.smartrrol.inventory.payload.category.SearchBrandDTO;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Service
public class BrandService {

    @Autowired
    private BrandRepo brandRepo;

    @Autowired
    private CompanyRepo companyRepo;

    public ApiResponse saveBrand(BrandDto brandDto){
        ApiResponse apiResponse = new ApiResponse();
        try {
            Brand brand = new Brand();
            BeanUtils.copyProperties(brandDto, brand);
            brand.setStatus(Status.STATUS_ACTIVE.getVal());
            brand.setCompId(brandDto.getCompId());
            brand.setUserName(brandDto.getUserName());
            brand.setCode(this.brandCodeGenerator(companyRepo.findById(brandDto.getCompId()).get()));
            Brand save = brandRepo.save(brand);
            if (save != null) {
                apiResponse.setResponseDesc("BRAND CREATED");
                apiResponse.setResponseCode(200);
                apiResponse.setData(save);
            }
        }catch (Exception e){
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }
        return apiResponse;
    }


    public ApiResponse update(BrandDto brandDto){
        ApiResponse apiResponse = new ApiResponse();
        try {
            Brand brand = brandRepo.findById(brandDto.getId()).get();
            BeanUtils.copyProperties(brandDto, brand);
            brand.setStatus(Status.STATUS_ACTIVE.getVal());
            brand.setCompId(brandDto.getCompId());
            brand.setUserName(brandDto.getUserName());
            brand.setCode(this.brandCodeGenerator(companyRepo.findById(brandDto.getCompId()).get()));
            Brand save = brandRepo.save(brand);
            if (save != null) {
                apiResponse.setResponseDesc("BRAND UPDATED");
                apiResponse.setResponseCode(200);
                apiResponse.setData(save);
            }
        }catch (Exception e){
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }
        return apiResponse;
    }

    public ApiResponse findAllBrand(SearchBrandDTO searchBrandDTO){
        List<BrandDto>  brandDtos = new ArrayList<>();
        ApiResponse apiResponse = new ApiResponse();

        try {
            PageRequest page_req = PageRequest.of(searchBrandDTO.getPage(), searchBrandDTO.getCount());
//            PageRequest page_req = PageRequest.of(0, 10);

            Page<Brand> all = brandRepo.findAll(this.getBrandBySearch(searchBrandDTO), page_req);

            apiResponse.setResponseDesc("FIND AND SEARCH BRAND");
            apiResponse.setResponseCode(200);
            apiResponse.setData(all);
        }catch (Exception e){
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }

        return apiResponse;
    }

    private Specification<Brand> getBrandBySearch(SearchBrandDTO searchBrandDTO){

        Specification<Brand> ordersSpecification = new Specification<Brand>() {
            @Override
            public Predicate toPredicate(Root<Brand> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                predicates.add(criteriaBuilder.equal(root.get("compId"),searchBrandDTO.getCompId()));
                predicates.add(criteriaBuilder.equal(root.get("status"),Status.STATUS_ACTIVE.getVal()));
                predicates.add(criteriaBuilder.equal(root.get("branchId"),searchBrandDTO.getBranchId()));
                Predicate like = criteriaBuilder.like(root.get("name"), "%" + searchBrandDTO.getName() + "%");
                Predicate orderCode = criteriaBuilder.like(root.get("code"), "%" + searchBrandDTO.getName() + "%");
                predicates.add(criteriaBuilder.or(like,orderCode));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };

        return ordersSpecification;

    }

    public ApiResponse findBynameWithoutPagination(SearchBrandDTO searchBrandDTO){
        List<BrandDto>  brandDtos = new ArrayList<>();
        ApiResponse apiResponse = new ApiResponse();

        try {
//            PageRequest page_req = PageRequest.of(searchBrandDTO.getPage(), searchBrandDTO.getCount());



            brandRepo.findBynameWithoutPagination(Status.STATUS_ACTIVE.getVal(),searchBrandDTO.getCompId(),searchBrandDTO.getName()).forEach(brand -> {
                BrandDto brandDto = new BrandDto();
                BeanUtils.copyProperties(brand,brandDto);
                brandDtos.add(brandDto);

            });

            apiResponse.setResponseDesc("FIND AND SEARCH BRAND");
            apiResponse.setResponseCode(200);
            apiResponse.setData(brandDtos);
        }catch (Exception e){
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }

        return apiResponse;
    }

    public ApiResponse deleteBran(Integer id){
        ApiResponse apiResponse = new ApiResponse();
        Brand brand = brandRepo.findById(id).get();
        brand.setStatus(Status.STATUS_DISABLED.getVal());
        brandRepo.save(brand);

        apiResponse.setResponseDesc("FIND AND SEARCH BRAND");
        apiResponse.setResponseCode(200);
        apiResponse.setData(id);
        return apiResponse;
    }

    private String brandCodeGenerator(Company company){
        Integer maxId = brandRepo.getMaxId();
        int i = maxId + 1;
        String code = "BRD"+company.getId()+"00"+i;
        return code;
    }
}
