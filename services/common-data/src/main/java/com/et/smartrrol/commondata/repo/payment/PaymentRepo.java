package com.et.smartrrol.commondata.repo.payment;


import com.et.smartrrol.commondata.models.payment.Payment;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface PaymentRepo extends JpaRepository<Payment, Integer>, JpaSpecificationExecutor<Payment> {

    List<Payment> findAllByStatusAndInvoice_Orders_Customer_Id(Integer status,Integer customerId);

    @Query(value = "Select u from Payment u where  (:status is null or u.status = :status) and (:branchId is null or u.branchId = :branchId) " +
            " and (:companyID is null or u.compId = :companyID) and  (:email is null or  u.paymentCode like %:email% or" +
            "  u.invoice.orders.orderCode like %:email%)")
    List<Payment> findByname(Integer status, Integer branchId, Integer companyID, String email, Pageable pageable);

    @Query(value = "Select u from Payment u where  (:status is null or u.status = :status) and (:branchId is null or u.branchId = :branchId) " +
            " and (:companyID is null or u.compId = :companyID) and  (:email is null or  u.paymentCode like %:email% or" +
            "  u.invoice.orders.orderCode like %:email%)")
    List<Payment> findBynameWithoutPagination(Integer status, Integer branchId, Integer companyID, String email );

    @Query("SELECT coalesce(max(inv.id), 0) FROM Payment inv")
    Integer getMaxId();
}
