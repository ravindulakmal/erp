package com.et.smartrrol.commondata.models.v0;


import com.et.smartrrol.commondata.models.BaseEntity;

import com.et.smartrrol.commondata.models.stock.Stock;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Getter
@Setter
public class Product extends BaseEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String productCode;
    private String productName;
    private Integer reOrderLevel;
    private String quality;
    private String updatedBy;
    private String batchNumber;
    private BigDecimal initialQty;
    @ManyToOne
    @JoinColumn
    private Supplier supplier;
    @ManyToOne
    @JoinColumn
    private ProductCategory productCategory;
    @ManyToOne
    @JoinColumn
    private Brand brand;
    //    @ManyToOne
//    @JoinColumn
//    private Limits limits;
    @JsonIgnore
    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private List<GrnByProducts> grnByProducts;
    @JsonIgnore
    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private List<Stock> stocks;;
}
