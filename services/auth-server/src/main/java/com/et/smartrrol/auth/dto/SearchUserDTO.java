package com.et.smartrrol.auth.dto;

import lombok.Data;

@Data
public class SearchUserDTO {
    private String name;
    private Integer companyID;
}
