package com.et.smartrrol.auth.dto.branch;

import lombok.Data;


@Data
public class SearchBranchDTO {
    private String name;
    private Integer companyId;
    private Integer page;
    private Integer count;
}
