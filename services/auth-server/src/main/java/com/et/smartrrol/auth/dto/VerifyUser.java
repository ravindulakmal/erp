package com.et.smartrrol.auth.dto;

import lombok.Data;

@Data
public class VerifyUser {

    private String otp;
    private String password;
    private String email;
    private Integer roleId;
    private Integer id;
}
