package com.et.smartrrol.commondata.repo.v0;


import com.et.smartrrol.commondata.models.v0.ProductCategory;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductCategoryRepo extends JpaRepository<ProductCategory,Integer>, JpaSpecificationExecutor<ProductCategory> {
    @Query("Select u from ProductCategory u where  (:status is null or u.status = :status) and (:companyID is null or u.compId = :companyID) and " +
            " (:email is null or  u.name like %:email% )")
    List<ProductCategory> findByname(Integer status, Integer companyID, String email, Pageable pageable);

    @Query("Select u from ProductCategory u where  (:status is null or u.status = :status) and (:companyID is null or u.compId = :companyID) and " +
            " (:email is null or  u.name like %:email% )")
    List<ProductCategory> findBynameWithoutPagination(Integer status, Integer companyID, String email);

    @Query("SELECT coalesce(max(inv.id), 0) FROM ProductCategory inv")
    Integer getMaxId();
}
