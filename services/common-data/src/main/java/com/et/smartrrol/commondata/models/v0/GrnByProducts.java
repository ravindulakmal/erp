package com.et.smartrrol.commondata.models.v0;


import com.et.smartrrol.commondata.models.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@ToString

public class GrnByProducts extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer Id;
    private BigDecimal amount;
    private BigDecimal qtytolimit;
    private Integer qty;
    private BigDecimal unitPrice;
    private BigDecimal sellingPrice;
    private BigDecimal retailPrice;
    private String batchNumber;
    private LocalDate expiryDate;
    @ManyToOne
    @JoinColumn
    private Product product;

    @ManyToOne
    @JoinColumn
    private Grn grn;

}
