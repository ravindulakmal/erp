package com.et.smartrrol.auth.dto;

import lombok.Data;

import java.util.List;

@Data
public class UserDTO {
    private Integer id;
    private Integer userIdWhoseCreate;

    private String password,code;
    private String email;
    private String title,firstname, lastname,
            personalMobile, sex;
    private List<Integer> roleIds;
    private List<Integer> companyIds;
}
