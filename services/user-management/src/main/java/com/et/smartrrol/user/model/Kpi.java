package com.et.smartrrol.user.model;

import java.io.Serializable;

public class Kpi implements Serializable {
    String empName;
    Integer kpi;
    String month;

    public Kpi(String empName, Integer kpi, String month) {
        this.empName = empName;
        this.kpi = kpi;
        this.month = month;
    }

    public Kpi() {
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public Integer getKpi() {
        return kpi;
    }

    public void setKpi(Integer kpi) {
        this.kpi = kpi;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }
}
