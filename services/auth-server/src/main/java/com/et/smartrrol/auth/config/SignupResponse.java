package com.et.smartrrol.auth.config;

import com.et.smartrrol.auth.dto.UserResponceDTO;

import java.util.List;

public class SignupResponse {

    private String token;
    private String type = "Bearer";
    private String refreshToken;
    private Integer id;
    private String username;
    private String email;
    private List<String> roles;

    private UserResponceDTO userDTO;
    private String DESCRIPTION;
    private Integer CODE;
    public SignupResponse(String accessToken, String refreshToken, Integer id, String username, String email, List roles,String DESCRIPTION,Integer CODE,UserResponceDTO userDTO) {
        this.token = accessToken;
        this.refreshToken = refreshToken;
        this.id = id;
        this.username = username;
        this.email = email;
        this.roles = roles;

        this.DESCRIPTION = DESCRIPTION;
        this.CODE = CODE;
        this.userDTO = userDTO;
    }

    public String getAccessToken() {
        return token;
    }

    public void setAccessToken(String accessToken) {
        this.token = accessToken;
    }

    public String getTokenType() {
        return type;
    }

    public void setTokenType(String tokenType) {
        this.type = tokenType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<String> getRoles() {
        return roles;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }


    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public Integer getCODE() {
        return CODE;
    }

    public void setCODE(Integer CODE) {
        this.CODE = CODE;
    }

    public UserResponceDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserResponceDTO userDTO) {
        this.userDTO = userDTO;
    }
}
