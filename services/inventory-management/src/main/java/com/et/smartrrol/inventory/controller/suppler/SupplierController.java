package com.et.smartrrol.inventory.controller.suppler;


import com.et.smartrrol.commondata.models.ApiResponse;
import com.et.smartrrol.inventory.payload.supplier.SupplierDTO;
import com.et.smartrrol.inventory.payload.supplier.SupplierSearchDTO;
import com.et.smartrrol.inventory.services.SupplierServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/supplier")

public class SupplierController {

    @Autowired
    private SupplierServices supplierServices;

    @GetMapping
    public String testService(HttpServletRequest request) {
        System.out.println("I am " + request.getRequestURL().toString());
        return request.getRequestURL().toString();
    }
    @PostMapping("/create")
//    @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId
    public ApiResponse create(@RequestBody SupplierDTO supplierDTO, @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId) {
        supplierDTO.setUserName(userName);
        supplierDTO.setBranchId(branchId);
        supplierDTO.setCompId(compId);
        return supplierServices.createSupplier(supplierDTO);
    }

    @PutMapping("/update")
    public ApiResponse update(@RequestBody SupplierDTO supplierDTO, @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId) {
        supplierDTO.setUserName(userName);
        supplierDTO.setBranchId(branchId);
        supplierDTO.setCompId(compId);
        return supplierServices.updateSuppler(supplierDTO);
    }

    @PostMapping("/findALlAndSearch")
    public ApiResponse findBynameWithoutPagination(@RequestBody SupplierSearchDTO supplierSearchDTO,
                                                   @RequestHeader("userName") String userName,
                                                   @RequestHeader("branchId") Integer branchId,
                                                   @RequestHeader("compId") Integer compId) {
        supplierSearchDTO.setUserName(userName);
        supplierSearchDTO.setBranchId(branchId);
        supplierSearchDTO.setCompId(compId);
        return supplierServices.findBynameWithoutPagination(supplierSearchDTO);
    }

    @PostMapping("/findALlAndSearch/{page}/{count}")
    public ApiResponse findALlAndSearch(@RequestBody SupplierSearchDTO supplierSearchDTO,
                                        @RequestHeader("userName") String userName,
                                        @RequestHeader("branchId") Integer branchId,
                                        @RequestHeader("compId") Integer compId,
                                        @PathVariable("page") Integer page,
                                        @PathVariable("count") Integer count) {
        supplierSearchDTO.setUserName(userName);
        supplierSearchDTO.setBranchId(branchId);
        supplierSearchDTO.setCompId(compId);
        supplierSearchDTO.setPage(page);
        supplierSearchDTO.setCount(count);
        return supplierServices.findAllSupplier(supplierSearchDTO);
    }


    @DeleteMapping("/delete/{id}")
    public ApiResponse deleteSupplier(@PathVariable Integer id) {
        return supplierServices.deleteSupplier(id);
    }
}
