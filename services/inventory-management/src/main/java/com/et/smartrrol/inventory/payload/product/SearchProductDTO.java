package com.et.smartrrol.inventory.payload.product;

import lombok.Data;

@Data
public class SearchProductDTO {
    private Integer compId;
    private String name;
    private String userName;
    private Integer branchID;
    private Integer page;
    private Integer count;
}
