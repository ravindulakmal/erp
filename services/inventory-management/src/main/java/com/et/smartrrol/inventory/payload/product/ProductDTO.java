package com.et.smartrrol.inventory.payload.product;

import com.et.smartrrol.commondata.models.BaseEntity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductDTO  extends BaseEntity {
    private Integer id;
    private String productCode;
    private String productName;
    private Integer reOrderLevel;
    private String quality;
    private Integer supplierID;
    //    private Integer limitId;
    private Integer productCategoryId;
    private Integer brandId;
    private BigDecimal initialQty;
//    private String userName;
//    private Integer branchId;
//    private Integer status;
//    private Integer compId;
}
