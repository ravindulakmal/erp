package com.et.smartrrol.sales.service;


import com.et.smartrrol.commondata.models.ApiResponse;
import com.et.smartrrol.commondata.models.Company;
import com.et.smartrrol.commondata.models.Invoice.Invoice;
import com.et.smartrrol.commondata.models.Status;
import com.et.smartrrol.commondata.models.order.OrderDetail;
import com.et.smartrrol.commondata.models.order.Orders;
import com.et.smartrrol.commondata.models.stock.Stock;
import com.et.smartrrol.commondata.repo.CompanyRepo;
import com.et.smartrrol.commondata.repo.invoice.InvoiceRepo;
import com.et.smartrrol.commondata.repo.order.OrderDetailRepo;
import com.et.smartrrol.commondata.repo.order.OrderRepo;
import com.et.smartrrol.commondata.repo.stock.StockRepo;
import com.et.smartrrol.commondata.repo.v0.CustomerRepo;
import com.et.smartrrol.sales.payloard.OrderDTO;
import com.et.smartrrol.sales.payloard.OrderDetailDTO;
import com.et.smartrrol.sales.payloard.OrderSearchDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderService {

    @Autowired
    private OrderRepo orderRepo;

    @Autowired
    private OrderDetailRepo orderDetailRepo;

    @Autowired
    private CustomerRepo customerRepo;

    @Autowired
    private StockRepo stockRepo;

    @Autowired
    private CompanyRepo companyRepo;

    @Autowired
    private InvoiceRepo invoiceRepo;


    @Transactional
    public ApiResponse orderCreate(OrderDTO orderDTO){
        ApiResponse apiResponse = new ApiResponse();

        try {
            if (!orderDTO.getOrderDetailDTOList().isEmpty()) {
                Orders orders = new Orders();
                BeanUtils.copyProperties(orderDTO, orders);
                orders.setCustomer(customerRepo.findById(orderDTO.getCustomerId()).get());
                orders.setStatus(Status.STATUS_ACTIVE.getVal());
                orders.setOrderCode(this.generateOrderCode(companyRepo.findById(orderDTO.getCompId()).get()));
                Orders save = orderRepo.save(orders);
                if (save!=null){

                    List<OrderDetail> orderDetailList = new ArrayList<>();
                    for (OrderDetailDTO orderDetailDTO : orderDTO.getOrderDetailDTOList()) {
                        OrderDetail orderDetail = new OrderDetail();
                        BeanUtils.copyProperties(orderDetailDTO,orderDetail);
                        orderDetail.setOrders(save);
                        orderDetail.setStock(stockRepo.findById(orderDetailDTO.getStockId()).get());
                        orderDetail.setStatus(Status.STATUS_ACTIVE.getVal());
                        OrderDetail save1 = orderDetailRepo.save(orderDetail);
                        if (save1!=null){

                            Stock stock = stockRepo.findById(orderDetailDTO.getStockId()).get();
                            stock.setMinusQty(stock.getMinusQty().add(orderDetailDTO.getQty()));
                            stockRepo.save(stock);
                            orderDetailList.add(save1);

                        }
                    }

                    Invoice invoice = new Invoice();
                    String s = this.generateInvoiceCode(companyRepo.findById(orderDTO.getCompId()).get());
                    invoice.setCode(s);
                    invoice.setOrders(save);
                    invoice.setBalanceDue(save.getNetAmount());
                    invoice.setPaidAmount(BigDecimal.ZERO);
                    invoice.setStatus(Status.STATUS_ACTIVE.getVal());
                    invoice.setIsSettled(Status.NOT_SETTLED.getVal());
                    invoice.setPayableAmount(save.getNetAmount());
                    if (orderDTO.getIsCredited()==1){
                        invoice.setPayableCreditLimit(orderDTO.getCreditLimit());
                        invoice.setBalanceDueCreditLimit(orderDTO.getCreditLimit());
                    }
                    Invoice saveInvoice = invoiceRepo.save(invoice);

                    apiResponse.setResponseDesc("ORDER ADDED");
                    apiResponse.setResponseCode(200);
                    apiResponse.setData(save);
                }
            }
        }catch (Exception e){
//            e.printStackTrace();
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }
        return apiResponse;
    }

    @Transactional
    public ApiResponse orderUpdate(OrderDTO orderDTO){
        ApiResponse apiResponse = new ApiResponse();

        try {
            if (!orderDTO.getOrderDetailDTOList().isEmpty()) {
                Orders orders = orderRepo.findById(orderDTO.getId()).get();
                BeanUtils.copyProperties(orderDTO, orders);
                orders.setCustomer(customerRepo.findById(orderDTO.getCustomerId()).get());
                orders.setStatus(Status.STATUS_ACTIVE.getVal());
                orders.setOrderCode(this.generateOrderCode(companyRepo.findById(orderDTO.getCompId()).get()));
                Orders save = orderRepo.save(orders);
                if (save!=null){

                    List<OrderDetail> orderDetailList = new ArrayList<>();
                    for (OrderDetailDTO orderDetailDTO : orderDTO.getOrderDetailDTOList()) {
                        if (orderDetailDTO.getId()==0 || orderDetailDTO.getId()== null) {
                            OrderDetail orderDetail = new OrderDetail();
                            BeanUtils.copyProperties(orderDetailDTO, orderDetail);
                            orderDetail.setOrders(save);
                            orderDetail.setStock(stockRepo.findById(orderDetailDTO.getStockId()).get());
                            orderDetail.setStatus(Status.STATUS_ACTIVE.getVal());
                            OrderDetail save1 = orderDetailRepo.save(orderDetail);
                            if (save1 != null) {

                                Stock stock = stockRepo.findById(orderDetailDTO.getStockId()).get();
                                stock.setMinusQty(stock.getMinusQty().add(orderDetailDTO.getQty()));
                                stockRepo.save(stock);
                                orderDetailList.add(save1);

                            }
                        }else{

                            OrderDetail orderDetail = orderDetailRepo.findById(orderDetailDTO.getId()).get();
                            BigDecimal getoldQty = orderDetail.getQty();
                            orderDetail.setOrders(save);
                            orderDetail.setStock(stockRepo.findById(orderDetailDTO.getStockId()).get());
                            orderDetail.setStatus(Status.STATUS_ACTIVE.getVal());
                            OrderDetail save1 = orderDetailRepo.save(orderDetail);
                            if (save1 != null) {

                                if (getoldQty.compareTo(orderDetailDTO.getQty())==1) {
                                    BigDecimal subtract = getoldQty.subtract(orderDetail.getQty());
                                    Stock stock = stockRepo.findById(orderDetailDTO.getStockId()).get();
                                    stock.setMinusQty(stock.getMinusQty().subtract(subtract));
                                    stockRepo.save(stock);
                                    orderDetailList.add(save1);
                                }else if (getoldQty.compareTo(orderDetailDTO.getQty())==-1){
                                    BigDecimal subtract = orderDetailDTO.getQty().subtract(getoldQty);
                                    Stock stock = stockRepo.findById(orderDetailDTO.getStockId()).get();
                                    stock.setMinusQty(stock.getMinusQty().add(subtract));
                                    stockRepo.save(stock);
                                    orderDetailList.add(save1);
                                }

                            }
                        }
                    }

                    apiResponse.setResponseDesc("ORDER UPDATED");
                    apiResponse.setResponseCode(200);
                    apiResponse.setData(save);

                }
            }
        }catch (Exception e){
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }
        return apiResponse;
    }


    public ApiResponse findAllOrder(OrderSearchDTO orderSearchDTO){
        ApiResponse apiResponse = new ApiResponse();
//        Pageable pageable = PageRequest.of(orderSearchDTO.getCurrent()-1, orderSearchDTO.getRowCount(), Sort.Direction.DESC);

        PageRequest page_req = PageRequest.of(orderSearchDTO.getPage(), orderSearchDTO.getCount());
        try{

            Page<Orders> all = orderRepo.findAll(this.getOrdersBySearch(orderSearchDTO), page_req);


            apiResponse.setResponseDesc("ORDER FIND BY NAME OR CODE");
            apiResponse.setResponseCode(200);
            apiResponse.setData(all);
        }catch (Exception e){
//            e.printStackTrace();
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }
        return apiResponse;
    }



    private Specification<Orders> getOrdersBySearch(OrderSearchDTO orderSearchDTO){

        Specification<Orders> ordersSpecification = new Specification<Orders>() {
            @Override
            public Predicate toPredicate(Root<Orders> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                predicates.add(criteriaBuilder.equal(root.get("compId"),orderSearchDTO.getCompId()));
                predicates.add(criteriaBuilder.equal(root.get("status"),Status.STATUS_ACTIVE.getVal()));
                predicates.add(criteriaBuilder.equal(root.get("branchId"),orderSearchDTO.getBranchId()));
                Predicate like = criteriaBuilder.like(root.get("customer").get("firstName"), "%" + orderSearchDTO.getCustomerName() + "%");
                Predicate orderCode = criteriaBuilder.like(root.get("orderCode"), "%" + orderSearchDTO.getCustomerName() + "%");
                Predicate like1 = criteriaBuilder.like(root.get("customer").get("lastName"), "%" + orderSearchDTO.getCustomerName() + "%");
                predicates.add(criteriaBuilder.or(like,orderCode,like1));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };

        return ordersSpecification;

    }


    public ApiResponse findBynameWithoutPagination(OrderSearchDTO orderSearchDTO){
        ApiResponse apiResponse = new ApiResponse();

        try{
            List<Orders> byname = orderRepo.findBynameWithoutPagination(Status.STATUS_ACTIVE.getVal(),
                    orderSearchDTO.getBranchId(), orderSearchDTO.getCompId(),
                    orderSearchDTO.getCustomerName());
            apiResponse.setResponseDesc("ORDER FIND BY NAME OR CODE");
            apiResponse.setResponseCode(200);
            apiResponse.setData(byname);
        }catch (Exception e){
//            e.printStackTrace();
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }
        return apiResponse;
    }


    public ApiResponse orderCancel(Integer orderId){
        ApiResponse apiResponse = new ApiResponse();
        try{

            Orders orders = orderRepo.findById(orderId).get();
            orders.setStatus(Status.STATUS_DISABLED.getVal());
            List<OrderDetail> orderDetails = orders.getOrderDetails();
            for (OrderDetail orderDetail : orderDetails){
                Stock stock = orderDetail.getStock();
                stock.getPlusQty().add(orderDetail.getQty());
                stockRepo.save(stock);
            }
            Orders save = orderRepo.save(orders);


            apiResponse.setResponseDesc("ORDER IS DISABLED");
            apiResponse.setResponseCode(200);
            apiResponse.setData(save);
        }catch (Exception e){

            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }
        return apiResponse;
    }



    public String generateOrderCode(Company company){
        Integer maxId = orderRepo.getMaxId();
        int i = maxId + 1;
        String code = "ORD" + company.getId() + "00" + i;
        return code;
    }

    public String generateInvoiceCode(Company company){
        Integer maxId = invoiceRepo.getMaxId();
        int i = maxId + 1;
        String code = "ORD" + company.getId() + "00" + i;
        return code;
    }

}
