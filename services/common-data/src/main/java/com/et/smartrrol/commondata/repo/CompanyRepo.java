package com.et.smartrrol.commondata.repo;

import com.et.smartrrol.commondata.models.Company;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface CompanyRepo extends JpaRepository<Company,Integer>, JpaSpecificationExecutor<Company> {



    List<Company> findAllByStatus(Integer sta);


//    List<Company> findAllByUserListAndStatus(List<User> userList, Integer status);



    @Query("SELECT coalesce(max(inv.id), 0) FROM Company inv")
    Integer getMaxId();
}
