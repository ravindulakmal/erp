package com.et.smartrrol.commondata.models;

public enum Status {

    STATUS_PENDING(1, "Pending"),
    STATUS_ACTIVE(2, "Active"),
    STATUS_DISABLED(9, "Disabled"),
    OTP_LENGTH(9, "otpLenth"),
    FOR_CHANGE_PASSWORD(10, "changePassword"),
    WAITING_FOR_CHANGE_PASSWORD(11, "waitingForChangePassword"),
    NOT_TRANSFERED(12, "not transferd"),
    TRANSFERED(13, "transferd"),
    SETTLED(14, "settled"),
    NOT_SETTLED(15, "notSettled"),



    STATUS_SUSPENDED(6, "Suspended"),

    STATUS_CRD_APPROVED(3, "Approved"),
    STATUS_CRD_PIN_GENERATED(8, "Pin Generated"),
    STATUS_CRD_PREACTIVE(9, "Pre Active"),

    NA(0, "NA"),

    CARD_BATCH_APPROVED(3, "Approved"),
    CARD_BATCH_PIN_GEN(8, "Pin Generated"),
    CARD_BATCH_PIN_PRINT(9, "Pin Printed"),
    ;
    public static final String EMAIL_MESSAGE_BODY_OTP_VERIFICATION = "Please use following OTP for your %s, This OTP Expires in %s. \n\nOTP: %s";

    int val;
    String defaultDesc;

    public int getVal() {
        return val;
    }

    void setVal(int val) {
        this.val = val;
    }

    public String getDefaultDesc() {
        return defaultDesc;
    }

    void setDefaultDesc(String defaultDesc) {
        this.defaultDesc = defaultDesc;
    }

    private Status(int val, String defaultDesc)
    {
        this.val = val;
        this.defaultDesc = defaultDesc;
    }

}
