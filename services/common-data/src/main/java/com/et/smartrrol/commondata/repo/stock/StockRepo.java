package com.et.smartrrol.commondata.repo.stock;


import com.et.smartrrol.commondata.models.stock.Stock;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StockRepo extends JpaRepository<Stock,Integer>, JpaSpecificationExecutor<Stock> {

    Stock findByGrnByproductId(Integer integer);

    @Query("SELECT coalesce(max(inv.id), 0) FROM Stock inv")
    Integer getMaxId();

    List<Stock> findAllByStatusAndCompIdAndProduct_Id(Integer status, Integer compId, Integer productID);
    List<Stock> findAllByStatusAndCompIdAndProduct_Id(Integer status, Integer compId, Integer productID, Pageable pageable);
}
