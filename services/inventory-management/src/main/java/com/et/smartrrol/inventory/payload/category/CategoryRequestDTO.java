package com.et.smartrrol.inventory.payload.category;

import com.et.smartrrol.commondata.models.BaseEntity;

import lombok.Data;

@Data
public class CategoryRequestDTO extends BaseEntity {
    private Integer categoryId;
    private String name,code;
    private String description;
    private Integer parentItemCategoryId;
    private Integer status;
}
