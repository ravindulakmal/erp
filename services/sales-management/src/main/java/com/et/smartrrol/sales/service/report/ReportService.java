package com.et.smartrrol.sales.service.report;


import com.et.smartrrol.commondata.models.Branch;
import com.et.smartrrol.commondata.models.Company;
import com.et.smartrrol.commondata.models.order.OrderDetail;
import com.et.smartrrol.commondata.models.order.Orders;
import com.et.smartrrol.commondata.repo.BranchRepo;
import com.et.smartrrol.commondata.repo.CompanyRepo;
import com.et.smartrrol.commondata.repo.order.OrderRepo;
import com.et.smartrrol.commondata.repo.v0.CustomerRepo;
import com.et.smartrrol.sales.payloard.OrderItemOBJ;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ReportService {

    @Autowired
    private OrderRepo orderRepo;

    @Autowired
    private CustomerRepo customerRepo;

    @Autowired
    private CompanyRepo companyRepo;

    @Autowired
    private BranchRepo branchRepo;


    public JasperPrint orderReport(Integer orderId) {
        JasperPrint jasperPrint = null;
        try {
            Optional<Orders> byId = orderRepo.findById(orderId);

            JasperReport jasperReport = JasperCompileManager.compileReport("C:/eyerax/eyerax-inventory-backend/src/main/resources/reports/eyerax-inventory-bill.jrxml");
//            JasperReport = JasperCompileManager.compileReport("C:/Users/Ruvil/JaspersoftWorkspace/MyReports/eyerax-inventory-bill.jrxml");
            if (byId.isPresent()) {
                Map<String, Object> parameters = new HashMap<>();
                Company company = companyRepo.findById(byId.get().getCompId()).get();

                parameters.put("companyName", company.getCompanyCode() + " / " + company.getBusinessName());
//                parameters.put("companyAddress",company.get() + " / "+ company.getName());
                Branch branch = branchRepo.findById(byId.get().getBranchId()).get();
                parameters.put("branchName", branch.getBranchCode() + " / " + branch.getName());
                parameters.put("contactInfo", company.getOfficialMobile());
                parameters.put("orderCode", byId.get().getOrderCode());
                parameters.put("discount", byId.get().getDiscount()+"");
                parameters.put("totAmount", byId.get().getGrossAmount()+"");
                parameters.put("netAmount", byId.get().getNetAmount()+"");
                parameters.put("tax", byId.get().getTax()+"");

                List<OrderItemOBJ> orderItemOBJS = new LinkedList<>();
                for (OrderDetail detail :
                        byId.get().getOrderDetails()) {
                    orderItemOBJS.add(new OrderItemOBJ(detail.getStock().getProduct().getProductName() + " / " + detail.getStock().getProduct().getProductCode(),
                            detail.getQty() + "", detail.getTot() + ""));
                }
                JRBeanCollectionDataSource jrDataSource = new JRBeanCollectionDataSource(orderItemOBJS);
                parameters.put("tableDataset", jrDataSource);
                System.out.println(parameters);

                jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrDataSource);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jasperPrint;
    }
}

