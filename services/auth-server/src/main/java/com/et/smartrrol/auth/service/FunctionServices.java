package com.et.smartrrol.auth.service;


import com.et.smartrrol.auth.dto.FunctionDTO;
import com.et.smartrrol.commondata.models.ApiResponse;
import com.et.smartrrol.commondata.models.Function;
import com.et.smartrrol.commondata.models.Status;
import com.et.smartrrol.commondata.repo.FunctionRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FunctionServices {
    @Autowired
    private FunctionRepo functionRepo;

    public ApiResponse createFunction(FunctionDTO functionDTO){
        ApiResponse apiResponse = new ApiResponse();
        try {
            Function function = new Function();
            BeanUtils.copyProperties(functionDTO, function);
            function.setStatus(Status.STATUS_ACTIVE.getVal());
            Function save = functionRepo.save(function);
            if (save!=null){
                apiResponse.setResponseDesc("FUNCTION  CREATED");
                apiResponse.setResponseCode(200);
                apiResponse.setData(save);
            }
        }catch (Exception e){
            apiResponse.setResponseDesc("FUNCTION CREATE ERROR");
            apiResponse.setResponseCode(500);
            apiResponse.setData(e.getMessage());

        }
        return apiResponse;
    }

    public ApiResponse updateFunction(FunctionDTO functionDTO){
        ApiResponse apiResponse = new ApiResponse();
        try {
            Function function = functionRepo.findById(functionDTO.getId()).get();
            BeanUtils.copyProperties(functionDTO, function);
            function.setStatus(Status.STATUS_ACTIVE.getVal());
            Function save = functionRepo.save(function);
            if (save!=null){
                apiResponse.setResponseDesc("FUNCTION UPDATED");
                apiResponse.setResponseCode(200);
                apiResponse.setData(save);
            }
        }catch (Exception e){
            apiResponse.setResponseDesc("FUNCTION UPDATED ERROR");
            apiResponse.setResponseCode(500);
            apiResponse.setData(e.getMessage());

        }
        return apiResponse;
    }

    public ApiResponse findAllFunctions(){
        ApiResponse apiResponse = new ApiResponse();
        try {

            List<FunctionDTO> functionDTOList =new ArrayList<>();
            functionRepo.findAll().forEach(function -> {
                FunctionDTO functionDTO = new FunctionDTO();
                BeanUtils.copyProperties(function,functionDTO);

                functionDTOList.add(functionDTO);
            });
            if (!functionDTOList.isEmpty()) {
                apiResponse.setResponseDesc("FUNCTION FIND ALL");
                apiResponse.setResponseCode(200);
                apiResponse.setData(functionDTOList);
            }
        }catch (Exception e){
            apiResponse.setResponseDesc("FUNCTION FIND ALL ERROR");
            apiResponse.setResponseCode(500);
            apiResponse.setData(e.getMessage());
        }
        return apiResponse;
    }

    public ApiResponse deleteFunction(Integer id) {
        ApiResponse apiResponse = new ApiResponse();
        Function role = functionRepo.findById(id).get();
        role.setStatus(Status.STATUS_DISABLED.getVal());
        Function save = functionRepo.save(role);
        if (save!=null){
            apiResponse.setResponseDesc("FUNCTION DISABLED");
            apiResponse.setResponseCode(200);
            apiResponse.setData(save);
        }else{
            apiResponse.setResponseDesc("FUNCTION DISABLED ERROR");
            apiResponse.setResponseCode(500);
            apiResponse.setData(0);
        }
        return apiResponse;


    }
}
