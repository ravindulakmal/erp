package com.et.smartrrol.auth.service;


import com.et.smartrrol.auth.dto.*;
import com.et.smartrrol.auth.dto.branch.BranchDTO;
import com.et.smartrrol.auth.dto.company.CompanyDTO;
import com.et.smartrrol.auth.dto.company.CompanyResponceDTO;

import com.et.smartrrol.commondata.models.*;
import com.et.smartrrol.commondata.repo.CompanyRepo;
import com.et.smartrrol.commondata.repo.RoleRepo;
import com.et.smartrrol.commondata.repo.UserRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.et.smartrrol.commondata.models.Status.*;


@Service
public class UserService {


    @Autowired
    private UserRepo userRepo;

    @Autowired
    private RoleRepo roleRepo;

    @Autowired
    private CompanyRepo companyRepo;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private BranchService branchService;

    BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    @Transactional
    public ApiResponse createUser(UserDTO userDTO) throws Exception {
        ApiResponse apiResponse = new ApiResponse();
        User user;

        if (!userRepo.existsByUserName(userDTO.getEmail())) {
            try {
                user = new User();

                BeanUtils.copyProperties(userDTO, user);
                user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
                user.setStatus(STATUS_ACTIVE.getVal());
//                user.setRole(roleRepo.findById(userDTO.getRoleId()).get());
//                user.setCompany(companyRepo.findById(userDTO.getCompanyID()).get());
                user.setUserName(userDTO.getEmail());
                user.setCode(this.generateUSerCode());
                User save = userRepo.save(user);
                User user1 = this.setUserToMultipleRole(userDTO.getRoleIds(), save);
                User user2 = this.setUserstoCompanies(userDTO.getCompanyIds(), save);
                User saves = userRepo.save(user2);
                if (saves != null) {
                    apiResponse.setResponseDesc("USER  CREATED");
                    apiResponse.setResponseCode(200);
                    apiResponse.setData(save);
                    return apiResponse;
                }
            } catch (Exception e) {
                apiResponse.setResponseDesc("USER CREATE ERROR");
                apiResponse.setResponseCode(e.hashCode());
                apiResponse.setData(e.getMessage());
                return apiResponse;
            }
        } else {
//            throw new Exception("User All ready create");
            apiResponse.setResponseDesc("USER ALL READY EXIST");
            apiResponse.setResponseCode(500);
            apiResponse.setData(0);

        }
        return apiResponse;
    }

    private User setUserstoCompanies(List<Integer> integerList, User user){
        List<User> userList = new ArrayList<>();
        List<Company> companies = new ArrayList<>();
        userList.add(user);
        companyRepo.findAllById(integerList).forEach(role -> {
            role.setUserList(userList);
            companies.add(role);
        });
        user.setCompanies(companies);
        return user;
    }


    private User setUserToMultipleRole(List<Integer> integerList , User user){
        List<User> userList = new ArrayList<>();
        List<Role> roles = new ArrayList<>();
        userList.add(user);
        roleRepo.findAllById(integerList).forEach(role -> {
            role.setUserList(userList);
            roles.add(role);
        });
        user.setRole(roles);
        return user;
    }

    public ApiResponse Registration(RegistraionDTO userDTO)  {
        ApiResponse apiResponse = new ApiResponse();
        User user;

        if (!userRepo.existsByUserName(userDTO.getEmail())) {
            try {
                user = new User();
                BeanUtils.copyProperties(userDTO, user);
                user.setUserName(userDTO.getEmail());
                user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
                user.setStatus(Status.STATUS_ACTIVE.getVal());

                CompanyDTO companyDTO = new CompanyDTO();
                companyDTO.setBrNo(userDTO.getBrNo());
                companyDTO.setBusinessType(userDTO.getBusinessType());
                companyDTO.setOfficialMobile(userDTO.getOfficialMobile());
                companyDTO.setBusinessName(userDTO.getBusinessName());
                companyDTO.setSubscriptionId(userDTO.getSubscriptionId());
                Integer integer = companyService.companyCreate(companyDTO);
                Company company = companyRepo.findById(integer).get();
//                user.setCompany(company);

                user.setCode(this.generateUSerCode());

                String s = this.generateOTPForPublicUser();
                user.setOtp(s);
                List<Integer> roles= new ArrayList<>();
                roles.add(9);

                User use = userRepo.save(user);
                User user1 = this.setUserToMultipleRole(roles, use);
                BranchDTO branchDTO = new BranchDTO();
                branchDTO.setCompanyId(company.getId());
                branchDTO.setAddress("Head Office Address");
                branchDTO.setName("Head Office");
                branchDTO.setContact(company.getOfficialMobile());
                branchDTO.setNumberOfEmployee(0);

                ApiResponse branch = branchService.createBranch(branchDTO);

                List<Integer> companyList = new ArrayList<>();
                companyList.add(company.getId());
                User user2 = this.setUserstoCompanies(companyList, user1);
                User saves = userRepo.save(user2);
                if (saves != null) {

                    Mail mail = new Mail();
                    mail.setMailFrom("ravindulakmal624@gmail.com");
                    mail.setMailTo(use.getUserName());
                    mail.setMailSubject("INVENTORY OF EYERAX MANAGEMENT  " + use.getOtp());
                    mail.setMailContent(EMAIL_MESSAGE_BODY_OTP_VERIFICATION);
                    emailService.sendEmail(mail);

                }
                apiResponse.setResponseDesc("USER  REGISTRATION");
                apiResponse.setResponseCode(200);
                apiResponse.setData(use);
                return apiResponse;
            } catch (Exception e) {
                apiResponse.setResponseDesc("USER REGISTRATION ERROR");
                apiResponse.setResponseCode(e.hashCode());
                apiResponse.setData(e);
                return apiResponse;
            }
        } else {
            apiResponse.setResponseDesc("USER ALL READY EXIST");
            apiResponse.setResponseCode(500);
            apiResponse.setData(0);
            return apiResponse;
        }


    }

    public ApiResponse updateUser(UserDTO userDTO) {
        ApiResponse apiResponse = new ApiResponse();
        Integer integer = null;
        try {
            User user;
            user = userRepo.findById(userDTO.getId()).get();
            user.setUserName(userDTO.getEmail());
            user.setFirstname(userDTO.getFirstname());
            user.setLastname(userDTO.getLastname());
            user.setSex(userDTO.getSex());
            user.setTitle(userDTO.getTitle());
            user.setPersonalMobile(userDTO.getPersonalMobile());
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            user.setStatus(user.getStatus());
//            user.setRole(roleRepo.findById(userDTO.getRoleId()).get());
            user.setCode(userDTO.getCode());
            User save = userRepo.save(user);
            if (save != null) {
                apiResponse.setResponseDesc("USER  UPDATED");
                apiResponse.setResponseCode(200);
                apiResponse.setData(save);
                return apiResponse;
            }
        } catch (Exception e) {
            apiResponse.setResponseDesc("USER UPDATED ERROR");
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);

        }
        return apiResponse;
    }

    public ApiResponse changePasswordRequest(UserDTO userDTO) throws Exception {

        ApiResponse apiResponse = new ApiResponse();
        User user = userRepo.findById(userDTO.getId()).get();
        user.setStatus(FOR_CHANGE_PASSWORD.getVal());
        String s = this.generateOTPForPublicUser();
        user.setOtp(s);
        User id = userRepo.save(user);
        if (id != null) {
            try {
                Mail mail = new Mail();
                mail.setMailFrom("ravindulakmal624@gmail.com");
                mail.setMailTo(id.getUserName());
                mail.setMailSubject("INVENTORY OF EYERAX MANAGEMENT " + id.getOtp());
                mail.setMailContent(EMAIL_MESSAGE_BODY_OTP_VERIFICATION);
                emailService.sendEmail(mail);

                apiResponse.setResponseDesc("MAIL SEND ERROR");
                apiResponse.setResponseCode(200);
                apiResponse.setData(id);
                return apiResponse;
            } catch (Exception e) {
                apiResponse.setResponseDesc("MAIL SEND ERROR");
                apiResponse.setResponseCode(500);
                apiResponse.setData(e);
                return apiResponse;
            }

        } else {
            apiResponse.setResponseDesc("CHANGE PASSWORD TO SEND OTP ERROR");
            apiResponse.setResponseCode(500);
            apiResponse.setData(0);
            return apiResponse;
        }
    }

    public ApiResponse verifyChangePassword(String otp, Integer id) {

        ApiResponse apiResponse = new ApiResponse();
        User user = userRepo.findById(id).get();
        if (user.getOtp().equalsIgnoreCase(otp) && user.getStatus() == FOR_CHANGE_PASSWORD.getVal()) {
            try {
                user.setStatus(WAITING_FOR_CHANGE_PASSWORD.getVal());
                User save = userRepo.save(user);
                if (save != null) {
                    apiResponse.setResponseDesc("USER UPDATED");
                    apiResponse.setResponseCode(200);
                    apiResponse.setData(id);
                    return apiResponse;
                }
            } catch (Exception e) {
                apiResponse.setResponseDesc("USER NOT UPDATED");
                apiResponse.setResponseCode(500);
                apiResponse.setData(e);
                return apiResponse;
            }
        } else {
            apiResponse.setResponseDesc("OTP NOT MATCHING");
            apiResponse.setResponseCode(500);
            apiResponse.setData(0);
        }
        return apiResponse;
    }

    public ApiResponse passwordChange(UserDTO userDTO) {

        ApiResponse apiResponse = new ApiResponse();
        User user = userRepo.findById(userDTO.getId()).get();
        if (user.getStatus() == WAITING_FOR_CHANGE_PASSWORD.getVal()) {
            user.setStatus(STATUS_ACTIVE.getVal());
            user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
            userRepo.save(user);
            apiResponse.setResponseDesc("USER PASSWORD CHANGE");
            apiResponse.setResponseCode(200);
            apiResponse.setData(user);
            return apiResponse;
        } else {
            apiResponse.setResponseDesc("STATUS NOT MAPPED AND VALID");
            apiResponse.setResponseCode(500);
            apiResponse.setData(0);
            return apiResponse;
        }
    }


//    public ApiResponse userLogin(String email, String password) throws Exception {
//        ApiResponse apiResponse = new ApiResponse();
//        User byUserNameAndStatus = userRepo.findByUserNameAndStatus(email, Status.STATUS_ACTIVE.getVal());
//        UserDTO userDTO = null;
//
//        boolean matches = bCryptPasswordEncoder.matches(password, byUserNameAndStatus.getPassword());
//        if (!matches) {
////            throw new Exception("User is not Matching");
//            apiResponse.setResponseDesc("USER NOT MATCHING");
//            apiResponse.setResponseCode(500);
//            apiResponse.setData(0);
//            return apiResponse;
//        } else {
//            userDTO = new UserDTO();
//            BeanUtils.copyProperties(byUserNameAndStatus, userDTO);
//            userDTO.setStatus(byUserNameAndStatus.getStatus());
//            userDTO.setCompanyID(byUserNameAndStatus.getCompany().getId());
//
//
//            apiResponse.setResponseDesc("USER LOGIN DONE");
//            apiResponse.setResponseCode(200);
//            apiResponse.setData(userDTO);
//            return apiResponse;
//        }
//
//
//    }

    public Integer resetPassword(Integer id) {
        User user = userRepo.findById(id).get();
        user.setPassword(bCryptPasswordEncoder.encode("123"));
        return userRepo.save(user).getId();
    }


    public UserResponceDTO findById(Integer id){
        User user = userRepo.findById(id).get();
        List<RoleDTO> roleDTOS = new ArrayList<>();
        List<Integer> roleListIntegers = new ArrayList<>();
        List<CompanyResponceDTO> companyDTOS = new ArrayList<>();
        UserResponceDTO userDTO = new UserResponceDTO();
        BeanUtils.copyProperties(user,userDTO);

        userDTO.setEmail(user.getUserName());
        userDTO.setLastname(user.getLastname());
        userDTO.setFirstname(user.getFirstname());
        user.getRole().forEach(role -> {
            RoleDTO roleDTO = new RoleDTO();
            BeanUtils.copyProperties(role,roleDTO);
            roleListIntegers.add(role.getId());
            roleDTOS.add(roleDTO);
        });
        user.getCompanies().forEach(company -> {
            CompanyResponceDTO companyDTO = new CompanyResponceDTO();
            BeanUtils.copyProperties(company,companyDTO);
            companyDTO.setSubscriptionId(company.getSubscription().getId());
            List<BranchDTO> branchDTOList = new ArrayList<>();
            for (Branch branch:
                    company.getBranches()) {
                BranchDTO branchDTO = new BranchDTO();
                BeanUtils.copyProperties(branch,branchDTO);
                branchDTOList.add(branchDTO);
            }
            companyDTO.setBranchDTOList(branchDTOList);
            companyDTOS.add(companyDTO);
        });

//        userDTO.setRoleIds(roleListIntegers);
        userDTO.setRoleDTOS(roleDTOS);
        userDTO.setCompanyDTOS(companyDTOS);
//                userDTO.se
        if (!userDTO.getCompanyDTOS().isEmpty()) {
            userDTO.setDefaultCompanyId(userDTO.getCompanyDTOS().get(0).getId());

            if (!userDTO.getCompanyDTOS().get(0).getBranchDTOList().isEmpty()) {
                userDTO.setDefaultBranchId(userDTO.getCompanyDTOS().get(0).getBranchDTOList().get(0).getId());
            }
        }
        userDTO.setStatus(user.getStatus());
        return userDTO;
    }


    public ApiResponse findAllUsers(SearchUserDTO searchUserDTO) {
        ApiResponse apiResponse = new ApiResponse();
        List<UserResponceDTO> userDTOS = new ArrayList<>();
        List<Integer> integerList = new ArrayList<>();
        integerList.add(searchUserDTO.getCompanyID());
        try {
            userRepo.findByname(Status.STATUS_ACTIVE.getVal(),  searchUserDTO.getName()).forEach(user -> {

                if (!user.getCompanies().isEmpty()) {
//
                    List<RoleDTO> roleDTOS = new ArrayList<>();
                    List<CompanyResponceDTO> companyDTOS = new ArrayList<>();
                    List<Integer> roleListIntegers = new ArrayList<>();
                    UserResponceDTO userDTO = new UserResponceDTO();
                    BeanUtils.copyProperties(user, userDTO);

                    userDTO.setEmail(user.getUserName());
                    userDTO.setLastname(user.getLastname());
                    userDTO.setFirstname(user.getFirstname());

                    user.getRole().forEach(role -> {
                        RoleDTO roleDTO = new RoleDTO();
                        BeanUtils.copyProperties(role, roleDTO);
                        roleListIntegers.add(role.getId());
                        roleDTOS.add(roleDTO);
                    });
                    user.getCompanies().forEach(company -> {
                        CompanyResponceDTO companyDTO = new CompanyResponceDTO();
                        BeanUtils.copyProperties(company, companyDTO);
                        List<BranchDTO> branchDTOList = new ArrayList<>();
                        for (Branch branch:
                                company.getBranches()) {
                            BranchDTO branchDTO = new BranchDTO();
                            BeanUtils.copyProperties(branch,branchDTO);
                            branchDTOList.add(branchDTO);
                        }
                        companyDTO.setSubscriptionId(company.getSubscription().getId());
                        companyDTO.setBranchDTOList(branchDTOList);
                        companyDTOS.add(companyDTO);
                    });

//        userDTO.setRoleIds(roleListIntegers);
                    userDTO.setRoleDTOS(roleDTOS);
                    userDTO.setCompanyDTOS(companyDTOS);
                    if (!userDTO.getCompanyDTOS().isEmpty()) {
                        userDTO.setDefaultCompanyId(userDTO.getCompanyDTOS().get(0).getId());

                        if (!userDTO.getCompanyDTOS().get(0).getBranchDTOList().isEmpty()) {
                            userDTO.setDefaultBranchId(userDTO.getCompanyDTOS().get(0).getBranchDTOList().get(0).getId());
                        }
                    }
//                userDTO.se
                    userDTO.setStatus(user.getStatus());
                    for (CompanyResponceDTO companyDTO : userDTO.getCompanyDTOS()){
                        if (companyDTO.getId() == searchUserDTO.getCompanyID() ){
                            userDTOS.add(userDTO);
                        }
                    }

                }
            });

            apiResponse.setResponseDesc("FIND ALL USERS BY COMPANY ID");
            apiResponse.setResponseCode(200);
            apiResponse.setData(userDTOS);
        } catch (Exception e) {
            apiResponse.setResponseDesc("FIND ALL USERS BY COMPANY ID");
            apiResponse.setResponseCode(e.hashCode());
            apiResponse.setData(e.getMessage());

        }
        return apiResponse;
    }

    public void deleteUser(Integer id) {
        User user = userRepo.findById(id).get();
        user.setStatus(Status.STATUS_DISABLED.getVal());
        userRepo.save(user);
    }


    private String generateOTPForPublicUser() {
        String otp;

        do {
            otp = generateOtp(Status.OTP_LENGTH.getVal());
        } while (userRepo.existsByOtp(otp));

        return otp;
    }

    public String generateOtp(int length) {
        String numbers = "0123456789";
        String otpCode = "";

        Random random = new Random();

        for (int i = 0; i < length; i++) {
            otpCode = otpCode.concat(String.valueOf(random.nextInt(numbers.length())));
        }

        return otpCode;
    }

    public Integer verifyOtp(String email, String otp, Integer id) throws Exception {
        Integer ids = null;
        if (!userRepo.existsByIdAndUserName(id, email)) {
            throw new Exception("User not matching");
        } else {
            if (!userRepo.existsByOtp(otp)) {
                throw new Exception("Otp not Allready");
            } else {
                User byIdAndEmailAndOtpAndStatus = userRepo.findByIdAndUserNameAndOtpAndStatus(id, email, otp, Status.STATUS_PENDING.getVal());
                byIdAndEmailAndOtpAndStatus.setStatus(Status.STATUS_ACTIVE.getVal());
                ids = userRepo.save(byIdAndEmailAndOtpAndStatus).getId();
            }

        }
        return ids;
    }

    private String generateUSerCode(){
        Integer maxId = userRepo.getMaxId();
        int i = maxId + 1;
        String code = "USR"+"00"+i;
        return code;
    }

}
