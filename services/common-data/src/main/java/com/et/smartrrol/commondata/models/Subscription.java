package com.et.smartrrol.commondata.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Entity
@Getter
@Setter
public class Subscription extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String subscription;
    private LocalDate startDate;
    private LocalDate expiredDate;
    private BigDecimal price;
    private String decription;

    @JsonIgnore
    @OneToMany(mappedBy = "subscription",cascade = CascadeType.ALL)
    private List<Company> companies;
}
