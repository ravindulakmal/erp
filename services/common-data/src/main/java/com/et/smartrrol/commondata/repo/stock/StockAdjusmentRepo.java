package com.et.smartrrol.commondata.repo.stock;

import com.et.smartrrol.commondata.models.stock.StockAdjusment;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StockAdjusmentRepo extends JpaRepository<StockAdjusment,Integer>, JpaSpecificationExecutor<StockAdjusment> {
    List<StockAdjusment> findAllByStatusAndCompIdAndBranchId(Integer status, Integer comPid, Integer branchId, Pageable pageable);

}
