package com.et.smartrrol.sales.payloard;


import com.et.smartrrol.commondata.models.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class OrderDTO extends BaseEntity {
    private Integer id;
    private BigDecimal grossAmount;
    private BigDecimal netAmount;
    private BigDecimal tax;
    private BigDecimal transport;
    private BigDecimal discount;
    private BigDecimal creditLimit;
    private Integer isCredited;
    private Integer isSettled;

    private Integer customerId;
    private List<OrderDetailDTO> orderDetailDTOList;
}
