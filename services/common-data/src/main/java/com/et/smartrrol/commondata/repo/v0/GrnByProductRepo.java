package com.et.smartrrol.commondata.repo.v0;

import com.et.smartrrol.commondata.models.v0.GrnByProducts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface GrnByProductRepo extends JpaRepository<GrnByProducts,Integer>, JpaSpecificationExecutor<GrnByProducts> {


    List<GrnByProducts> findAllByStatusAndGrn_Id(Integer status, Integer grnId);

    @Query("SELECT coalesce(max(inv.id), 0) FROM GrnByProducts inv")
    Integer getMaxId();
}
