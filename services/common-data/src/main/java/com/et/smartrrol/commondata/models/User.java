package com.et.smartrrol.commondata.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;


@Entity
@Getter
@Setter
public class User  extends AuditModel implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Integer status;
    private String otp;
    private String password;
    private String userName;
    private String title,code;
    private String firstname;
    private String lastname;
    private String personalMobile;
    private String sex;
    private Integer defaultCompanyId;
    private Integer defaultBranchId;

    private LocalDate birthDate;
    @ManyToMany(mappedBy = "userList", fetch = FetchType.EAGER)
    private List<Role> role;

    @ManyToMany
    @JoinTable(
            name = "company_by_user",
            joinColumns = @JoinColumn(name = "user_id",referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "company_id",referencedColumnName = "id"))
    private List<Company> companies;

//    @ManyToOne
//    @JoinColumn
//    private Company company;
}
