package com.et.smartrrol.inventory.controller.stock;


import com.et.smartrrol.commondata.models.ApiResponse;
import com.et.smartrrol.inventory.payload.stock.StockAdjusementDTO;
import com.et.smartrrol.inventory.payload.stock.StockSearchDTO;
import com.et.smartrrol.inventory.payload.stock.StockTransferDTO;
import com.et.smartrrol.inventory.services.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/stock")
@CrossOrigin("*")
public class StockController {

    @Autowired
    private StockService stockService;

    @GetMapping
    public String testService(HttpServletRequest request) {
        System.out.println("I am " + request.getRequestURL().toString());
        return request.getRequestURL().toString();
    }
    @PostMapping("/findAllByProduct/{page}/{count}")
    public ApiResponse findAllByProduct(@RequestBody StockSearchDTO stockSearchDTO,
                                        @RequestHeader("userName") String userName,
                                        @RequestHeader("branchId") Integer branchId,
                                        @RequestHeader("compId") Integer compId,
                                        @PathVariable("page") Integer page,
                                        @PathVariable("count") Integer count) throws Exception {
        stockSearchDTO.setUserName(userName);
        stockSearchDTO.setBranchId(branchId);
        stockSearchDTO.setCompId(compId);
        stockSearchDTO.setPage(page);
        stockSearchDTO.setCount(count);
        ApiResponse product = stockService.findAllStock(stockSearchDTO);

        return product;
    }

    @PostMapping("/findAllByProduct")
    public ApiResponse findAllByProductWithoutPagination(@RequestBody StockSearchDTO stockSearchDTO, @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId) throws Exception {
        stockSearchDTO.setUserName(userName);
        stockSearchDTO.setBranchId(branchId);
        stockSearchDTO.setCompId(compId);
        ApiResponse product = stockService.findAllByProductWithoutPagination(stockSearchDTO);

        return product;
    }

    @PostMapping("/availableQty")
    public ApiResponse AvailableQTyByProduct(@RequestBody StockSearchDTO stockSearchDTO, @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId) throws Exception {
        stockSearchDTO.setUserName(userName);
        stockSearchDTO.setBranchId(branchId);
        stockSearchDTO.setCompId(compId);
        ApiResponse product = stockService.AvailableQTyByProduct(stockSearchDTO);

        return product;
    }

    @PostMapping("/stockAdjustMeant")
    public ApiResponse stockAdjustMeant(@RequestBody StockAdjusementDTO stockAdjusementDTO, @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId) throws Exception {
        stockAdjusementDTO.setUserName(userName);
        stockAdjusementDTO.setBranchId(branchId);
        stockAdjusementDTO.setCompId(compId);
        ApiResponse product = stockService.stockAdjustMeant(stockAdjusementDTO);

        return product;
    }

    @PostMapping("/stock-transfer")
    public ApiResponse stockAdjustMeant(@RequestBody StockTransferDTO stockTransferDTO, @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId) throws Exception {
        stockTransferDTO.setUserName(userName);
        stockTransferDTO.setBranchId(branchId);
        stockTransferDTO.setCompId(compId);
        ApiResponse product = stockService.stockTransfer(stockTransferDTO);

        return product;
    }

    @PostMapping("/find-all-stock-transfer/{page}/{count}")
    public ApiResponse findAllStockTransfer(@RequestBody StockSearchDTO stockSearchDTO,
                                            @RequestHeader("userName") String userName,
                                            @RequestHeader("branchId") Integer branchId,
                                            @RequestHeader("compId") Integer compId,
                                            @PathVariable("page") Integer page,
                                            @PathVariable("count") Integer count) throws Exception {
        stockSearchDTO.setUserName(userName);
        stockSearchDTO.setBranchId(branchId);
        stockSearchDTO.setCompId(compId);
        stockSearchDTO.setPage(page);
        stockSearchDTO.setCount(count);
        ApiResponse product = stockService.findAllStockTransfer(stockSearchDTO);

        return product;
    }

    @PostMapping("/find-all-stock-adjusement/{page}/{count}")
    public ApiResponse findAllStokAdjusement(@RequestBody StockSearchDTO stockSearchDTO,
                                             @RequestHeader("userName") String userName,
                                             @RequestHeader("branchId") Integer branchId,
                                             @RequestHeader("compId") Integer compId,
                                             @PathVariable("page") Integer page,
                                             @PathVariable("count") Integer count) throws Exception {
        stockSearchDTO.setUserName(userName);
        stockSearchDTO.setBranchId(branchId);
        stockSearchDTO.setCompId(compId);
        stockSearchDTO.setPage(page);
        stockSearchDTO.setCount(count);
        ApiResponse product = stockService.findAllStokAdjusement(stockSearchDTO);

        return product;
    }

    @PostMapping("/find-all-available-batch-stock-by-product/{page}/{count}")
    public ApiResponse findAllAvailableBatchStockByProduct(@RequestBody StockSearchDTO stockSearchDTO,
                                                           @RequestHeader("userName") String userName,
                                                           @RequestHeader("branchId") Integer branchId,
                                                           @RequestHeader("compId") Integer compId,
                                                           @PathVariable("page") Integer page,
                                                           @PathVariable("count") Integer count) throws Exception {
        stockSearchDTO.setUserName(userName);
        stockSearchDTO.setBranchId(branchId);
        stockSearchDTO.setCompId(compId);
        stockSearchDTO.setPage(page);
        stockSearchDTO.setCount(count);
        ApiResponse product = stockService.findAllAvailableBatchStockByProduct(stockSearchDTO);

        return product;
    }
}
