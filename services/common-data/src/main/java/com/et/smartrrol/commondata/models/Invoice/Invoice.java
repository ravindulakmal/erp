package com.et.smartrrol.commondata.models.Invoice;

import com.et.smartrrol.commondata.models.BaseEntity;

import com.et.smartrrol.commondata.models.order.Orders;
import com.et.smartrrol.commondata.models.payment.Payment;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Getter
@Setter
public class Invoice extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private BigDecimal balanceDue;
    private BigDecimal paidAmount;
    private BigDecimal payableAmount;
    private BigDecimal balanceDueCreditLimit;
    private Integer isSettled;
    //    private BigDecimal paidCreditLimit;
    private BigDecimal payableCreditLimit;
    private String code;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "orders_id", referencedColumnName = "id")
    @JsonIgnore
    private Orders orders;
    private Integer paymentRows;

    @JsonIgnore
    @OneToMany(mappedBy = "invoice", cascade = CascadeType.ALL)
    private List<Payment> payments;
}
