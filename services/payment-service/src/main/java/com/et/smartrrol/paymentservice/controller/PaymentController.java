package com.et.smartrrol.paymentservice.controller;

import com.et.smartrrol.commondata.models.ApiResponse;
import com.et.smartrrol.paymentservice.payload.payment.PaymentDTO;
import com.et.smartrrol.paymentservice.payload.payment.PaymentSearchDTO;
import com.et.smartrrol.paymentservice.payload.payment.PaymentTypeDTO;
import com.et.smartrrol.paymentservice.service.PaymentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/payment")
public class PaymentController {

    @Autowired
    private PaymentService paymentService;


    @PostMapping("/create")
    public ApiResponse create(@RequestBody PaymentDTO paymentDTO, @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId) {

        paymentDTO.setUserName(userName);
        paymentDTO.setBranchId(branchId);
        paymentDTO.setCompId(compId);

        return paymentService.paymentCreate(paymentDTO);

    }

    @DeleteMapping("/delete/{id}")
    public ApiResponse delete(@PathVariable Integer id, @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId) {
        return paymentService.paymentDelete(id);

    }



    @GetMapping("/availableCreditedByCustomerID/{id}")
    public ApiResponse availableCreditedByCustomerID(@PathVariable Integer id, @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId) {
        return paymentService.availableCreditedByCustomerID(id);

    }

    @PostMapping("/findallPayment/{page}/{count}")
    public ApiResponse findallPayment(@RequestBody PaymentSearchDTO paymentSearchDTO, @RequestHeader("userName") String userName,
                                      @RequestHeader("branchId") Integer branchId,
                                      @RequestHeader("compId") Integer compId,
                                      @PathVariable("page") Integer page,
                                      @PathVariable("count") Integer count) {
        paymentSearchDTO.setUserName(userName);
        paymentSearchDTO.setBranchId(branchId);
        paymentSearchDTO.setCompId(compId);
        paymentSearchDTO.setPage(page);
        paymentSearchDTO.setCount(count);
        return paymentService.findAllPayments(paymentSearchDTO);

    }

    @PostMapping("/findallPayment")
    public ApiResponse findAllPaymentsWithoutPagination(@RequestBody PaymentSearchDTO paymentSearchDTO, @RequestHeader("userName") String userName,
                                                        @RequestHeader("branchId") Integer branchId,
                                                        @RequestHeader("compId") Integer compId) {
        paymentSearchDTO.setUserName(userName);
        paymentSearchDTO.setBranchId(branchId);
        paymentSearchDTO.setCompId(compId);
        return paymentService.findAllPaymentsWithoutPagination(paymentSearchDTO);

    }

}
