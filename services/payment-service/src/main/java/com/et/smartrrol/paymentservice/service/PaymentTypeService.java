package com.et.smartrrol.paymentservice.service;


import com.et.smartrrol.commondata.models.ApiResponse;
import com.et.smartrrol.commondata.models.Status;
import com.et.smartrrol.commondata.models.payment.PaymentType;
import com.et.smartrrol.commondata.repo.payment.PaymentTypeRepo;
import com.et.smartrrol.paymentservice.payload.payment.PaymentTypeDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentTypeService {

    @Autowired
    private PaymentTypeRepo paymentTypeRepo;


    public ApiResponse createPayment(PaymentTypeDTO paymentTypeDTO){
        ApiResponse apiResponse = new ApiResponse();

        try{
            PaymentType paymentType = new PaymentType();
            BeanUtils.copyProperties(paymentTypeDTO,paymentType);
            paymentType.setStatus(Status.STATUS_ACTIVE.getVal());
            PaymentType save = paymentTypeRepo.save(paymentType);
            if (save != null) {
                apiResponse.setResponseDesc("PAYMENT TYPE ADDED");
                apiResponse.setResponseCode(200);
                apiResponse.setData(save);
            }
        }catch (Exception e){
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }
        return apiResponse;

    }

    public ApiResponse updatePayment(PaymentTypeDTO paymentTypeDTO){
        ApiResponse apiResponse = new ApiResponse();

        try{
            PaymentType paymentType = paymentTypeRepo.findById(paymentTypeDTO.getId()).get();
            BeanUtils.copyProperties(paymentTypeDTO,paymentType);
            paymentType.setStatus(Status.STATUS_ACTIVE.getVal());
            PaymentType save = paymentTypeRepo.save(paymentType);
            if (save != null) {
                apiResponse.setResponseDesc("PAYMENT TYPE UPDATED");
                apiResponse.setResponseCode(200);
                apiResponse.setData(save);
            }
        }catch (Exception e){
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }
        return apiResponse;

    }
    public ApiResponse findAllByBranchAndCompId(Integer compId, Integer branchId){
        ApiResponse apiResponse = new ApiResponse();

        try{
//            List<PaymentType> paymentType = paymentTypeRepo.findAllByStatusAndCompIdAndBranchId(Status.STATUS_ACTIVE.getVal(),compId,branchId);
            List<PaymentType> paymentType = paymentTypeRepo.findAll();


            apiResponse.setResponseDesc("FIND ALL PAYMENT TYPE BY COMPANY AND BRANCH");
            apiResponse.setResponseCode(200);
            apiResponse.setData(paymentType);

        }catch (Exception e){
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }
        return apiResponse;

    }

}
