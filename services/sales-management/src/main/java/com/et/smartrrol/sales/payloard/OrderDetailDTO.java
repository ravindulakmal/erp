package com.et.smartrrol.sales.payloard;


import com.et.smartrrol.commondata.models.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class OrderDetailDTO extends BaseEntity {
    private Integer id;
    private BigDecimal qty;
    private BigDecimal unitPrice;
    private BigDecimal tot;
    private Integer stockId;
}
