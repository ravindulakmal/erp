package com.et.smartrrol.commondata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication(scanBasePackages = {"com.et.smartrrol.commondata.*"})
@EntityScan(basePackages = {"com.et.smartrrol.commondata.*"})
@EnableFeignClients
@EnableJpaAuditing
public class CommonDataApplication {

    public static void main(String[] args) {
        SpringApplication.run(CommonDataApplication.class, args);
    }


}
