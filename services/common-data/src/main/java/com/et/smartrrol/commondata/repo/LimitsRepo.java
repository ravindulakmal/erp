package com.et.smartrrol.commondata.repo;

import com.et.smartrrol.commondata.models.v0.Limits;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface LimitsRepo extends JpaRepository<Limits, Integer>, JpaSpecificationExecutor<Limits> {
}
