package com.et.smartrrol.auth.controller;


import com.et.smartrrol.auth.dto.company.AddCompanyToUserDTO;
import com.et.smartrrol.auth.dto.company.CompanyDTO;
import com.et.smartrrol.auth.dto.company.ImageDTO;
import com.et.smartrrol.auth.service.CompanyService;
import com.et.smartrrol.commondata.models.ApiResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@RestController
@CrossOrigin
@RequestMapping("/company")

public class CompanyController {
    @Autowired
    private CompanyService companyService;

    //    private final String path = "C:\\upload\\";
    private String path ="/root/images";

    @PostMapping("/create")
    public ApiResponse create(@RequestBody CompanyDTO companyDTO) {

        return companyService.companyCreateWithRequest(companyDTO);
    }

    @PostMapping("/addedMembertoCompany")
    public ApiResponse addedMembertoCompany(@RequestBody AddCompanyToUserDTO companyDTO) {

        return companyService.addUserByCompany(companyDTO);
    }

    @PutMapping("/update")
    public ApiResponse companyUpdateWithRequest(@RequestBody CompanyDTO companyDTO) {

        return companyService.companyUpdateWithRequest(companyDTO);
    }


    @PostMapping("/findAll")
    public ApiResponse findAll( @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId) {

        return companyService.findAll(userName, compId);
    }

    @PostMapping("/findByCompIdandBranchID/{compid}/{brId}")
    public ApiResponse findAll( @RequestHeader("userName") String userName,
                                @RequestHeader("branchId") Integer branchId,
                                @RequestHeader("compId") Integer compId,
                                @PathVariable("compid") Integer compid,
                                @PathVariable("brId") Integer brId) {

        return companyService.filterByCompanyIdAndBranchId(compid,brId);
    }


    @PostMapping("/upload")
    public ApiResponse handleFileUpload(@RequestParam(value = "file", required = false) MultipartFile file,
                                        @RequestHeader("userName") String userName,
                                        @RequestHeader("branchId") Integer branchId,
                                        @RequestHeader("compId") Integer compId) {
        ApiResponse apiResponse = new ApiResponse();
        String fileName = file.getOriginalFilename();
        try {
            file.transferTo(new File(this.path + fileName));
        } catch (Exception e) {
            apiResponse.setData(HttpStatus.INTERNAL_SERVER_ERROR + "");
            apiResponse.setResponseCode(500);
            apiResponse.setResponseDesc(e.getMessage());
        }

        ImageDTO imageDTO = new ImageDTO();
        imageDTO.setImageName(this.path + fileName);
        imageDTO.setImageType(this.path + fileName);
//        imageDTO.setImagePath("/root/imagesAndVedios" + fileName);
        Integer integer = companyService.companyImageUpload(imageDTO, compId);
        if (integer!= 0){
            apiResponse.setData(integer);
            apiResponse.setResponseCode(200);
            apiResponse.setResponseDesc("IMAGE UPLOADED SUCCESS");

        }


        return apiResponse;
    }

}
