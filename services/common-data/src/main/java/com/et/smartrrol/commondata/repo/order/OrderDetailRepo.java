package com.et.smartrrol.commondata.repo.order;


import com.et.smartrrol.commondata.models.order.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderDetailRepo extends JpaRepository<OrderDetail,Integer> , JpaSpecificationExecutor<OrderDetail> {
}
