package com.et.smartrrol.customerservice.controller;



import com.et.smartrrol.commondata.models.ApiResponse;
import com.et.smartrrol.customerservice.dto.CustomerDTO;
import com.et.smartrrol.customerservice.dto.CustomerSerachDTO;
import com.et.smartrrol.customerservice.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/customers")
public class CustomerController {

    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping
    public String testService(HttpServletRequest request) {
        System.out.println("I am " + request.getRequestURL().toString());
        return request.getRequestURL().toString();
    }





    @PostMapping("/create")
    public ApiResponse create(@RequestBody CustomerDTO customerDTO, @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId) {
        customerDTO.setUserName(userName);
        customerDTO.setBranchId(branchId);
        customerDTO.setCompId(compId);
        return customerService.createCustomer(customerDTO);

    }

    @PutMapping("/update")
    public ApiResponse updateCustomer(@RequestBody CustomerDTO customerDTO, @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId) {
        customerDTO.setUserName(userName);
        customerDTO.setBranchId(branchId);
        customerDTO.setCompId(compId);
        return customerService.updateCustomer(customerDTO);

    }

    @PostMapping("/getAll/{page}/{count}")
    public ApiResponse getAll(@RequestBody CustomerSerachDTO customerSerachDTO,
                              @RequestHeader("userName") String userName,
                              @RequestHeader("branchId") Integer branchId,
                              @RequestHeader("compId") Integer compId,
                              @PathVariable("page") Integer page,
                              @PathVariable("count") Integer count) {
        customerSerachDTO.setUserName(userName);
        customerSerachDTO.setBranchId(branchId);
        customerSerachDTO.setCompId(compId);
        customerSerachDTO.setPage(page);
        customerSerachDTO.setCount(count);
        return customerService.findAll(customerSerachDTO);


    }

    @PostMapping("/getAll")
    public ApiResponse searchToAutoCommit(@RequestBody CustomerSerachDTO customerSerachDTO,
                                          @RequestHeader("userName") String userName,
                                          @RequestHeader("branchId") Integer branchId,
                                          @RequestHeader("compId") Integer compId) {
        customerSerachDTO.setUserName(userName);
        customerSerachDTO.setBranchId(branchId);
        customerSerachDTO.setCompId(compId);
        return customerService.searchToAutoCommit(customerSerachDTO);


    }

    @DeleteMapping("/delete/{id}")
    public ApiResponse delete(@PathVariable Integer id) {
        ApiResponse apiResponse = new ApiResponse();
        try {
            customerService.delete(id);
            apiResponse.setResponseCode(HttpStatus.OK.value());

        } catch (Exception e) {
            e.printStackTrace();
            apiResponse.setResponseCode(HttpStatus.BAD_REQUEST.value());
            apiResponse.setData(null);
        }

        return apiResponse;
    }

}
