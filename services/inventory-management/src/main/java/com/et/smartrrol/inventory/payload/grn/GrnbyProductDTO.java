package com.et.smartrrol.inventory.payload.grn;


import com.et.smartrrol.commondata.models.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class GrnbyProductDTO extends BaseEntity {
    private Integer id;
    private Integer productID;
    private BigDecimal amount;
    private BigDecimal qtytolimit;
    private BigDecimal unitPrice;
    private BigDecimal sellingPrice;
    private BigDecimal retailPrice;
    private LocalDate expiryDate;
    private Integer qty;
}
