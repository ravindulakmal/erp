package com.et.smartrrol.inventory.payload.stock;


import lombok.Data;

import java.math.BigDecimal;

@Data
public class StockTransferResponceTODTO {

    private Integer id;
    private BigDecimal transferQty;
    private StockResponceDTO oldStock;
    private StockResponceDTO newStock;
    private BranchDTO newBranch;
    private CompanyResponceDTO newCompany;
    private BranchDTO oldBranch;
    private CompanyResponceDTO oldCompany;
}
