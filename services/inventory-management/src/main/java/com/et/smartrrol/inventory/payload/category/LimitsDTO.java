package com.et.smartrrol.inventory.payload.category;

import lombok.Data;

@Data
public class LimitsDTO {
    private String name;
    private String tag;
    private String description;
    private Integer minimumCount;
    private Integer id;
}
