package com.et.smartrrol.auth.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class SubscriptionDTO {
    private Integer id;
    private String subscription;
    private LocalDate startDate;
    private LocalDate expiredDate;
    private BigDecimal price;
    private String decription;

}
