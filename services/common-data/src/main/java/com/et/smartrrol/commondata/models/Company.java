package com.et.smartrrol.commondata.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
public class Company extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String brNo;
    private String officialMobile,businessName,businessType;
    private String companyCode;
    private String imageType;
    private String imageName;
    @JsonIgnore
    @ManyToMany(mappedBy = "companies",cascade = CascadeType.ALL)
    private List<User> userList;
    @JsonIgnore
    @OneToMany(mappedBy = "company",cascade = CascadeType.ALL)
    private List<Branch> branches;
    @ManyToOne
    @JoinColumn
    private Subscription subscription;

}
