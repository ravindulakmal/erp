package com.et.smartrrol.auth.controller;

import com.et.smartrrol.auth.dto.FunctionDTO;
import com.et.smartrrol.auth.service.FunctionServices;

import com.et.smartrrol.commondata.models.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/function")
@CrossOrigin("*")
public class FunctionController {
    @Autowired
    private FunctionServices functionServices;

    @PostMapping("/create")
    public ApiResponse createUser(@RequestBody FunctionDTO functionDTO) throws Exception {
//        grnVersionTwoDTO.setUserName(userName);
//        grnVersionTwoDTO.setBranchId(branchId);

        ApiResponse apiResponse = functionServices.createFunction(functionDTO);


        return apiResponse;
    }


    @PutMapping("/update")
    public ApiResponse updateFunction(@RequestBody FunctionDTO functionDTO) {
//        grnVersionTwoDTO.setUserName(userName);
//        grnVersionTwoDTO.setBranchId(branchId);

        ApiResponse apiResponse = functionServices.updateFunction(functionDTO);


        return apiResponse;
    }

    @GetMapping("/findAll")
    public ApiResponse findAll() {
//        grnVersionTwoDTO.setUserName(userName);
//        grnVersionTwoDTO.setBranchId(branchId);

        ApiResponse apiResponse = functionServices.findAllFunctions();


        return apiResponse;
    }

    @DeleteMapping("/delete/{id}")
    public ApiResponse deleteGrn(@PathVariable Integer id) {
        return functionServices.deleteFunction(id);
    }
}
