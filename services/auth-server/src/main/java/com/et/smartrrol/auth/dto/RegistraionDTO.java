package com.et.smartrrol.auth.dto;

import lombok.Data;

@Data
public class RegistraionDTO {
    private Integer id;
    private Integer status;
    private Integer subscriptionId;
    private String code;
    private String password;
    private String email;
    private String title, firstname, lastname,
            personalMobile,
            officialMobile,
            businessName,
            sex;
    private String businessType;
    private String brNo;

}
