package com.et.smartrrol.auth.service;


import com.et.smartrrol.auth.dto.FunctionDTO;
import com.et.smartrrol.auth.dto.RoleDTO;
import com.et.smartrrol.commondata.models.ApiResponse;
import com.et.smartrrol.commondata.models.Function;
import com.et.smartrrol.commondata.models.Role;
import com.et.smartrrol.commondata.models.Status;
import com.et.smartrrol.commondata.repo.FunctionRepo;
import com.et.smartrrol.commondata.repo.RoleRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RoleService {
    @Autowired
    private RoleRepo roleRepo;
    @Autowired
    private FunctionRepo functionRepo;

    public Integer creteRoel(RoleDTO roleDTO){
        Role role = new Role();
        role.setRole(roleDTO.getRole());
        role.setStatus(Status.STATUS_ACTIVE.getVal());
        List<Function> functionList = new ArrayList<>();
        for (Integer integer:
                roleDTO.getIntegerList()) {
            Function function = functionRepo.findById(integer).get();

            functionList.add(function);
        }
        role.setFunctionList(functionList);
        return roleRepo.save(role).getId();
    }

    public List<RoleDTO> findALl(){

        List<RoleDTO> roleDTOS =new ArrayList<>();
        roleRepo.findAllByStatus(Status.STATUS_ACTIVE.getVal()).forEach(role -> {
            RoleDTO roleDTO = new RoleDTO();
            roleDTO.setRole(role.getRole());
            roleDTO.setId(role.getId());
            roleDTO.setStatus(role.getStatus());
            List<Function> functionList = role.getFunctionList();
            List<FunctionDTO> functionDTOList = new ArrayList<>();
            for (Function function : functionList){
                FunctionDTO functionDTO = new FunctionDTO();
                BeanUtils.copyProperties(function,functionDTO);
                functionDTOList.add(functionDTO);
            }

            roleDTO.setFunctionDTOS(functionDTOList);
            if  (roleDTO.getStatus()==Status.STATUS_ACTIVE.getVal()) {
                roleDTOS.add(roleDTO);
            }
        });

        return roleDTOS;
    }

    public ApiResponse deleteRole(Integer id) {
        ApiResponse apiResponse = new ApiResponse();
        Role role = roleRepo.findById(id).get();
        role.setStatus(Status.STATUS_DISABLED.getVal());
        Role save = roleRepo.save(role);
        if (save!=null){
            apiResponse.setResponseDesc("ROLE DISABLED");
            apiResponse.setResponseCode(200);
            apiResponse.setData(save);
        }else{
            apiResponse.setResponseDesc("ROLE DISABLED ERROR");
            apiResponse.setResponseCode(500);
            apiResponse.setData(0);
        }
        return apiResponse;


    }


}
