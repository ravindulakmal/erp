package com.et.smartrrol.paymentservice.controller;

import com.et.smartrrol.commondata.models.ApiResponse;
import com.et.smartrrol.paymentservice.payload.payment.PaymentDTO;
import com.et.smartrrol.paymentservice.payload.payment.PaymentTypeDTO;
import com.et.smartrrol.paymentservice.service.PaymentService;
import com.et.smartrrol.paymentservice.service.PaymentTypeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/paymentType")
public class PaymentTypeController {
    @Autowired
    private PaymentTypeService paymentTypeService;


    @PostMapping("/create")
    public ApiResponse createPaymentType(@RequestBody PaymentTypeDTO paymentTypeDTO, @RequestHeader("userName") String userName,
                                         @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId
    ) {

        paymentTypeDTO.setBranchId(branchId);
        paymentTypeDTO.setCompId(compId);
        paymentTypeDTO.setCompId(compId);
        paymentTypeDTO.setUserName(userName);
        return paymentTypeService.createPayment(paymentTypeDTO);

    }

    @PostMapping("/update")
    public ApiResponse updatePaymentType(@RequestBody PaymentTypeDTO paymentTypeDTO, @RequestHeader("userName") String userName,
                                         @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId
    ) {

        paymentTypeDTO.setBranchId(branchId);
        paymentTypeDTO.setCompId(compId);
        paymentTypeDTO.setCompId(compId);
        paymentTypeDTO.setUserName(userName);
        return paymentTypeService.updatePayment(paymentTypeDTO);
    }

    @PostMapping("/findAll")
    public ApiResponse findAllPaymentTypeByCompandBranch(@RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId) {



        return paymentTypeService.findAllByBranchAndCompId(compId,branchId);
    }

}
