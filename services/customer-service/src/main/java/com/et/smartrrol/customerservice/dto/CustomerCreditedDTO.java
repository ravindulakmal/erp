package com.et.smartrrol.customerservice.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CustomerCreditedDTO {
    private Integer id;
    private BigDecimal creditLimit;
    private BigDecimal paymentTerms;
}
