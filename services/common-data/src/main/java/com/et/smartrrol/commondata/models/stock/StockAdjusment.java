package com.et.smartrrol.commondata.models.stock;


import com.et.smartrrol.commondata.models.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Getter
@Setter
public class StockAdjusment extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private LocalDate date;
    private Integer stockAdjusmentTypeIsMinusOrPlus;
    private BigDecimal newQty;
    private BigDecimal qtyBalance;
    @ManyToOne
    @JoinColumn
    private Stock stock;


}
