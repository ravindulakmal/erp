package com.et.smartrrol.inventory.payload.stock;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class StockAdjusementResponceDTO {
    private Integer id;
    private LocalDate date;
    private Integer stockAdjusmentTypeIsMinusOrPlus;
    private BigDecimal newQty;
    private BigDecimal qtyBalance;
    private StockResponceDTO stockResponceDTO;
}
