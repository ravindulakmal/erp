package com.et.smartrrol.auth.config;

import lombok.Data;

@Data
public class LoginRequest {
    private String email;
    private String password;
}
