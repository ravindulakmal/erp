package com.et.smartrrol.inventory.payload.product;

import com.et.smartrrol.commondata.models.BaseEntity;
import com.et.smartrrol.inventory.payload.category.BrandDto;
import com.et.smartrrol.inventory.payload.category.LimitsDTO;
import com.et.smartrrol.inventory.payload.supplier.SupplierDTO;
import lombok.Data;


import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class ProductResponceDTO extends BaseEntity {
    private Integer id;
    private String productCode;
    private String productName;
    private LocalDate expiryDate;
    private Integer reOrderLevel;
    private String quality;
    private Integer supplierID;
    private SupplierDTO supplierDTO;
    private BrandDto brandDto;
    private ProductCategoryDTO productCategoryDTO;
    private Integer limitId;
    private LimitsDTO limitsDTO;
    private Integer productCategoryId;
    private Integer brandId;
    private BigDecimal initialQty;
//    private String userName;
//    private Integer branchId;
//    private Integer status;
//    private Integer compId;
}
