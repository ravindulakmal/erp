package com.et.smartrrol.inventory.services;



import com.et.smartrrol.commondata.models.ApiResponse;
import com.et.smartrrol.commondata.models.Company;
import com.et.smartrrol.commondata.models.Status;
import com.et.smartrrol.commondata.models.stock.Stock;
import com.et.smartrrol.commondata.models.v0.Grn;
import com.et.smartrrol.commondata.models.v0.GrnByProducts;
import com.et.smartrrol.commondata.models.v0.Product;
import com.et.smartrrol.commondata.repo.CompanyRepo;
import com.et.smartrrol.commondata.repo.stock.StockRepo;
import com.et.smartrrol.commondata.repo.v0.GrnByProductRepo;
import com.et.smartrrol.commondata.repo.v0.GrnRepo;
import com.et.smartrrol.commondata.repo.v0.ProductRepo;
import com.et.smartrrol.commondata.repo.v0.SupplierRepo;
import com.et.smartrrol.inventory.payload.grn.*;
import com.et.smartrrol.inventory.payload.product.ProductDTO;
import com.et.smartrrol.inventory.payload.supplier.SupplierDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Slf4j
@Service
public class GrnService {
    private static final Integer ACTIVE = 1;
    private static final Integer DISABLE = 9;

    @Autowired
    private GrnRepo grnRepo;

    @Autowired
    private ProductRepo productRepo;

    @Autowired
    private SupplierRepo supplierRepo;

    @Autowired
    private CompanyRepo companyRepo;

    @Autowired
    private GrnByProductRepo grnByProductRepo;

    @Autowired
    private StockRepo stockRepo;

    @Transactional
    public ApiResponse createGrn(GrnDto grnDto) {
        ApiResponse apiResponse = new ApiResponse();

        try {
            Grn grn = new Grn();
            BeanUtils.copyProperties(grnDto, grn);
            grn.setStatus(Status.STATUS_ACTIVE.getVal());
            grn.setSupplier(supplierRepo.findById(grnDto.getSupplierID()).get());
            grn.setGrnNo(this.generateGrn(companyRepo.findById(grn.getCompId()).get()));
            grn.setTax(grnDto.getTax());
            grn.setTransport(grnDto.getTransport());
            Grn save = grnRepo.save(grn);
            if (save != null) {
                ApiResponse grnByProduct = this.createGrnByProduct(save, grnDto);
                if (grnByProduct.getResponseCode() == 200) {
                    apiResponse.setData(save);
                    apiResponse.setResponseCode(200);
                    apiResponse.setResponseDesc("GRN BY ADDED");
                } else {
                    grnRepo.delete(save);
                }
            }
        } catch (Exception e) {
            apiResponse.setData(null);
            apiResponse.setResponseCode(500);
            apiResponse.setResponseDesc(e.getMessage());
        }
        return apiResponse;
    }


    private ApiResponse createGrnByProduct(Grn grn, GrnDto grnDto) throws Exception {
        ApiResponse apiResponse = new ApiResponse();
        List<GrnByProducts> grnByProductsList = new ArrayList<>();

        try {
            for (GrnbyProductDTO productDTO :
                    grnDto.getGrnbyProductDTOS()) {
                GrnByProducts grnByProducts = new GrnByProducts();
                BeanUtils.copyProperties(productDTO,grnByProducts);
                Product product = productRepo.findById(productDTO.getProductID()).get();
                grnByProducts.setProduct(product);
                grnByProducts.setGrn(grn);
                grnByProducts.setQty(productDTO.getQty());
                grnByProducts.setAmount(productDTO.getAmount());
//                grnByProducts.setQtytolimit(new BigDecimal(product.getLimits().getMinimumCount()*productDTO.getQty()));
                grnByProducts.setStatus(Status.STATUS_ACTIVE.getVal());
                grnByProducts.setCompId(grn.getCompId());

                String s = this.generateBatchNumber(companyRepo.findById(grnDto.getCompId()).get());
                grnByProducts.setBatchNumber(s);
                GrnByProducts save = grnByProductRepo.save(grnByProducts);

                if (save != null) {
                    Stock stock = new Stock();
                    stock.setProduct(product);
                    stock.setIsTransferdEnable(Status.STATUS_ACTIVE.getVal());
                    stock.setLocalDate(LocalDate.now());
                    stock.setMinusQty(BigDecimal.ZERO);
                    stock.setStatus(Status.STATUS_ACTIVE.getVal());
                    stock.setReason("Added new Batch With grn No : " + grn.getGrnNo() + " and Batch Number :" + save.getBatchNumber());
                    stock.setBatchNumber(save.getBatchNumber());
                    stock.setPlusQty(new BigDecimal(save.getQty()));
                    stock.setCompId(grnByProducts.getCompId());
                    stock.setBranchId(grnByProducts.getBranchId());
                    stock.setUserName(grnByProducts.getUserName());
                    stock.setGrnByproductId(save.getId());
                    stock.setRetailPrice(save.getRetailPrice());
                    stock.setUnitPrice(save.getUnitPrice());
                    stock.setSellingPrice(save.getSellingPrice());
                    stock.setTransferdOrNot(Status.NOT_TRANSFERED.getVal());
                    Stock st = stockRepo.save(stock);


                    grnByProductsList.add(grnByProducts);

                }
            }

            apiResponse.setData(grnByProductsList);
            apiResponse.setResponseCode(200);
            apiResponse.setResponseDesc("GRN BY PRODUCT ADDED");
            return apiResponse;
        } catch (Exception e) {
            throw new Exception(e);
        }

    }

    public ApiResponse updateGrn(GrnDto grnDto) {
        ApiResponse apiResponse = new ApiResponse();

        try {
            Grn grn = grnRepo.findById(grnDto.getId()).get();
            BeanUtils.copyProperties(grnDto, grn);
            grn.setStatus(grn.getStatus());
            grn.setSupplier(supplierRepo.findById(grnDto.getSupplierID()).get());
            grn.setGrnNo(grnDto.getGrnNo());
            grn.setTax(grnDto.getTax());
            grn.setTransport(grnDto.getTransport());
            Grn save = grnRepo.save(grn);
            System.out.println(save);
            if (save != null) {
                ApiResponse grnByProduct = this.updatedGrnByProduct(save, grnDto);
                System.out.println(grnByProduct);
                if (grnByProduct.getResponseCode() == 200) {
                    apiResponse.setData(save);
                    apiResponse.setResponseCode(200);
                    apiResponse.setResponseDesc("GRN UPDATED");
                } else {
                    grnRepo.delete(save);
                }
            }
        } catch (Exception e) {
            apiResponse.setData(null);
            apiResponse.setResponseCode(500);
            apiResponse.setResponseDesc(e.getMessage());
        }
        return apiResponse;
    }

    private ApiResponse updatedGrnByProduct(Grn grn, GrnDto grnDto) throws Exception {
        ApiResponse apiResponse = new ApiResponse();
        List<GrnByProducts> grnByProductsList = new ArrayList<>();

        try {
            for (GrnbyProductDTO productDTO :
                    grnDto.getGrnbyProductDTOS()) {
                if (productDTO.getId() == 0 || productDTO.getId() == null) {

                    GrnByProducts grnByProducts = new GrnByProducts();
                    BeanUtils.copyProperties(productDTO,grnByProducts);
                    Product product = productRepo.findById(productDTO.getProductID()).get();
                    grnByProducts.setProduct(product);
                    grnByProducts.setGrn(grn);
                    grnByProducts.setQty(productDTO.getQty());
                    grnByProducts.setAmount(productDTO.getAmount());
                    grnByProducts.setStatus(Status.STATUS_ACTIVE.getVal());
//                    grnByProducts.setQtytolimit(new BigDecimal(product.getLimits().getMinimumCount()*productDTO.getQty()));
                    grnByProducts.setCompId(grn.getCompId());
                    GrnByProducts save = grnByProductRepo.save(grnByProducts);
                    if (save != null) {

                        Stock stock = new Stock();
                        stock.setProduct(product);
                        stock.setIsTransferdEnable(Status.STATUS_ACTIVE.getVal());
                        stock.setLocalDate(LocalDate.now());
                        stock.setMinusQty(BigDecimal.ZERO);
                        stock.setStatus(Status.STATUS_ACTIVE.getVal());
                        stock.setRetailPrice(save.getRetailPrice());
                        stock.setUnitPrice(save.getUnitPrice());
                        stock.setSellingPrice(save.getSellingPrice());
                        stock.setReason("Added new Batch With grn No : " + grn.getGrnNo() + " and Batch Number :" + save.getBatchNumber());
                        stock.setBatchNumber(save.getBatchNumber());
                        stock.setPlusQty(new BigDecimal(save.getQty()));
                        stock.setCompId(grnByProducts.getCompId());
                        stock.setBranchId(grnByProducts.getBranchId());
                        stock.setUserName(grnByProducts.getUserName());
                        stock.setGrnByproductId(save.getId());
                        stock.setTransferdOrNot(Status.NOT_TRANSFERED.getVal());
                        Stock st = stockRepo.save(stock);

                        grnByProductsList.add(grnByProducts);
                    }
                } else {

                    GrnByProducts grnByProducts = grnByProductRepo.findById(productDTO.getId()).get();

                    Product product = productRepo.findById(productDTO.getProductID()).get();
                    grnByProducts.setProduct(product);
                    grnByProducts.setGrn(grn);
                    grnByProducts.setQty(productDTO.getQty());
                    grnByProducts.setAmount(productDTO.getAmount());
                    grnByProducts.setStatus(grnByProducts.getStatus());
                    grnByProducts.setCompId(grn.getCompId());
                    grnByProducts.setRetailPrice(productDTO.getRetailPrice());
                    grnByProducts.setSellingPrice(productDTO.getSellingPrice());
                    grnByProducts.setUnitPrice(productDTO.getUnitPrice());

                    GrnByProducts save = grnByProductRepo.save(grnByProducts);
                    if (save != null) {
                        Stock stock = stockRepo.findByGrnByproductId(grnByProducts.getId());
                        stock.setProduct(product);
                        stock.setIsTransferdEnable(Status.STATUS_ACTIVE.getVal());
                        stock.setLocalDate(LocalDate.now());
                        stock.setMinusQty(BigDecimal.ZERO);
                        stock.setStatus(Status.STATUS_ACTIVE.getVal());
                        stock.setRetailPrice(save.getRetailPrice());
                        stock.setUnitPrice(save.getUnitPrice());
                        stock.setSellingPrice(save.getSellingPrice());
                        stock.setReason("Added new Batch With grn No : " + grn.getGrnNo() + " and Batch Number :" + save.getBatchNumber());
                        stock.setBatchNumber(save.getBatchNumber());
                        stock.setPlusQty(new BigDecimal(save.getQty()));
                        stock.setCompId(grnByProducts.getCompId());
                        stock.setBranchId(grnByProducts.getBranchId());
                        stock.setUserName(grnByProducts.getUserName());
                        stock.setGrnByproductId(save.getId());
                        Stock st = stockRepo.save(stock);
                        grnByProductsList.add(grnByProducts);

                    }
                }
            }

            apiResponse.setData(grnByProductsList);
            apiResponse.setResponseCode(200);
            apiResponse.setResponseDesc("GRN BY PRODUCT UPDATED");
            return apiResponse;
        } catch (Exception e) {
            throw new Exception(e);
        }

    }

//    public ApiResponse findAll(SearchgrnDTO searchgrnDTO) {
//        ApiResponse apiResponse = new ApiResponse();
//        List<GrnResponceDTO> grnDtos = new ArrayList<>();
//
//        try {
//            PageRequest page_req = PageRequest.of(searchgrnDTO.getPage(), searchgrnDTO.getCount());
////            PageRequest page_req = PageRequest.of(0, 10);
//            List<Grn> byname = grnRepo.findByname(Status.STATUS_ACTIVE.getVal(), searchgrnDTO.getCompId(), searchgrnDTO.getName(),page_req);
//            for (Grn grn :
//                    byname) {
//                GrnResponceDTO grnDto = new GrnResponceDTO();
//                BeanUtils.copyProperties(grn, grnDto);
//                List<GrnByProductResponceDTO> grnbyProductDTOS = new ArrayList<>();
//                List<GrnByProducts> allByStatusAndGrn_id = grnByProductRepo.findAllByStatusAndGrn_Id(Status.STATUS_ACTIVE.getVal(), grn.getId());
//                for (GrnByProducts grnByProducts :
//                        allByStatusAndGrn_id) {
//                    GrnByProductResponceDTO productDTO = new GrnByProductResponceDTO();
//                    BeanUtils.copyProperties(grnByProducts, productDTO);
//
//                    Product product = productRepo.findById(grnByProducts.getProduct().getId()).get();
//                    productDTO.setProductId(product.getId());
//                    productDTO.setProductCode(product.getProductCode());
//                    productDTO.setProductCategoryId(product.getProductCategory().getParentCategoryId());
//                    productDTO.setProductName(product.getProductName());
//                    productDTO.setBrandId(product.getBrand().getId());
//                    productDTO.setInitialQty(product.getInitialQty());
////                    productDTO.setProductDto(productDto);
//                    grnbyProductDTOS.add(productDTO);
//
//                }
//                SupplierDTO supplierDTO = new SupplierDTO();
//                BeanUtils.copyProperties(grn.getSupplier(), supplierDTO);
//                grnDto.setSupplierDTO(supplierDTO);
//                grnDto.setGrnByProductResponceDTOS(grnbyProductDTOS);
//                grnDtos.add(grnDto);
//            }
//            apiResponse.setData(grnDtos);
//            apiResponse.setResponseCode(200);
//            apiResponse.setResponseDesc("FIND ALL GRN");
//        } catch (Exception e) {
//            apiResponse.setData(null);
//            apiResponse.setResponseCode(500);
//            apiResponse.setResponseDesc(e.getMessage());
//        }
//        return apiResponse;
//    }

    public ApiResponse findAll(SearchgrnDTO searchgrnDTO) {
        ApiResponse apiResponse = new ApiResponse();
        try{
            PageRequest page_req = PageRequest.of(searchgrnDTO.getPage(), searchgrnDTO.getCount());
            Page<Grn> all = grnRepo.findAll(this.getGrnBySearch(searchgrnDTO),page_req);
        }catch (Exception e){
            apiResponse.setData(null);
            apiResponse.setResponseCode(500);
            apiResponse.setResponseDesc(e.getMessage());
        }
        return apiResponse;
    }



    private Specification<Grn> getGrnBySearch(SearchgrnDTO searchgrnDTO){

        Specification<Grn> ordersSpecification = new Specification<Grn>() {
            @Override
            public Predicate toPredicate(Root<Grn> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                predicates.add(criteriaBuilder.equal(root.get("compId"),searchgrnDTO.getCompId()));
                predicates.add(criteriaBuilder.equal(root.get("status"),Status.STATUS_ACTIVE.getVal()));
                predicates.add(criteriaBuilder.equal(root.get("branchId"),searchgrnDTO.getBranchID()));
                Predicate like = criteriaBuilder.like(root.get("supplier").get("fullName"), "%" + searchgrnDTO.getName() + "%");
                Predicate orderCode = criteriaBuilder.like(root.get("grnNo"), "%" + searchgrnDTO.getName() + "%");
                predicates.add(criteriaBuilder.or(like,orderCode));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };

        return ordersSpecification;

    }

    public ApiResponse findAllBynameWIthoutPagination(SearchgrnDTO searchgrnDTO) {
        ApiResponse apiResponse = new ApiResponse();
        List<GrnResponceDTO> grnDtos = new ArrayList<>();

        try {

            List<Grn> byname = grnRepo.findBynameWIthoutPagination(Status.STATUS_ACTIVE.getVal(), searchgrnDTO.getCompId(), searchgrnDTO.getName());
            for (Grn grn :
                    byname) {
                GrnResponceDTO grnDto = new GrnResponceDTO();
                BeanUtils.copyProperties(grn, grnDto);
                List<GrnByProductResponceDTO> grnbyProductDTOS = new ArrayList<>();
                List<GrnByProducts> allByStatusAndGrn_id = grnByProductRepo.findAllByStatusAndGrn_Id(Status.STATUS_ACTIVE.getVal(), grn.getId());
                for (GrnByProducts grnByProducts :
                        allByStatusAndGrn_id) {
                    GrnByProductResponceDTO productDTO = new GrnByProductResponceDTO();
                    BeanUtils.copyProperties(grnByProducts, productDTO);

                    Product product = productRepo.findById(grnByProducts.getProduct().getId()).get();
                    productDTO.setProductId(product.getId());
                    productDTO.setProductCode(product.getProductCode());
                    productDTO.setProductCategoryId(product.getProductCategory().getParentCategoryId());
                    productDTO.setProductName(product.getProductName());
                    productDTO.setBrandId(product.getBrand().getId());
                    productDTO.setInitialQty(product.getInitialQty());
//                    productDTO.setProductDto(productDto);
                    grnbyProductDTOS.add(productDTO);

                }
                SupplierDTO supplierDTO = new SupplierDTO();
                BeanUtils.copyProperties(grn.getSupplier(), supplierDTO);
                grnDto.setSupplierDTO(supplierDTO);
                grnDto.setGrnByProductResponceDTOS(grnbyProductDTOS);
                grnDtos.add(grnDto);
            }
            apiResponse.setData(grnDtos);
            apiResponse.setResponseCode(200);
            apiResponse.setResponseDesc("FIND ALL GRN");
        } catch (Exception e) {
            apiResponse.setData(null);
            apiResponse.setResponseCode(500);
            apiResponse.setResponseDesc(e.getMessage());
        }
        return apiResponse;
    }



    private String generateGrn(Company company) {
        Integer maxId = grnRepo.getMaxId();
        int i = maxId + 1;
        String code = "GRN" + company.getId() + "00" + i;
        return code;
    }

    private String generateBatchNumber(Company company) {
        Integer maxId = grnByProductRepo.getMaxId();
        int i = maxId + 1;
        String code = "BATCH" + company.getId() + "00" + i;
        return code;
    }

}
