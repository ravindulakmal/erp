package com.et.smartrrol.commondata.repo.v0;

import com.et.smartrrol.commondata.models.v0.SupplierCredited;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface SupplierCreditedRepo extends JpaRepository<SupplierCredited,Integer>, JpaSpecificationExecutor<SupplierCredited> {

    SupplierCredited findBySupplierIdAndStatus(Integer supplierId,Integer status);}
