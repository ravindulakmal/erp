package com.et.smartrrol.paymentservice.payload.payment;


import com.et.smartrrol.commondata.models.BaseEntity;
import lombok.Data;

@Data
public class PaymentSearchDTO extends BaseEntity {
    private String payment;
    private int page;
    private int count;
}
