package com.et.smartrrol.inventory.payload.stock;

import com.et.smartrrol.commondata.models.BaseEntity;
import com.et.smartrrol.inventory.payload.product.ProductResponceDTO;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class AvailableQtyDTO extends BaseEntity {
    private ProductResponceDTO productResponceDTO;
    private BigDecimal balance;
}
