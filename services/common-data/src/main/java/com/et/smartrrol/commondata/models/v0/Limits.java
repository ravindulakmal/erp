package com.et.smartrrol.commondata.models.v0;

import com.et.smartrrol.commondata.models.AuditModel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
public class Limits extends AuditModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String tag;
    private String description;
    private Integer minimumCount;
//    @JsonIgnore
//    @OneToMany(mappedBy = "productCategory", cascade = CascadeType.ALL)
//    private List<Product> products;
}
