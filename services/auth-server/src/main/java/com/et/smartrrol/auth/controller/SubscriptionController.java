package com.et.smartrrol.auth.controller;

import com.et.smartrrol.auth.dto.SubscriptionDTO;
import com.et.smartrrol.auth.service.SubscriptionService;
import com.et.smartrrol.commondata.models.ApiResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/subscription")
@CrossOrigin("*")
public class SubscriptionController {

    @Autowired
    private SubscriptionService subscriptionService;

    @PostMapping("/create")
    public ApiResponse createSubscription(@RequestBody SubscriptionDTO subscriptionDTO) {
        ApiResponse apiResponse = new ApiResponse();

        try {
            Integer grnVersionTwo = subscriptionService.createS(subscriptionDTO);
            apiResponse.setResponseCode(HttpStatus.OK.value());
            apiResponse.setData(grnVersionTwo);
            apiResponse.setResponseDesc("SUBSCRIPTION CREATE");
        } catch (Exception e) {
            e.printStackTrace();
            apiResponse.setResponseCode(HttpStatus.BAD_REQUEST.value());
            apiResponse.setData(null);
        }

        return apiResponse;
    }

    @GetMapping("/findAll")
    public ApiResponse findAll() {
        ApiResponse apiResponse = new ApiResponse();
        try {
            List<SubscriptionDTO> allGrn = subscriptionService.findAll();
            System.out.println(allGrn.size());
            apiResponse.setData(allGrn);
            apiResponse.setResponseDesc("FIND ALL SUBSCRIPTION");
        } catch (Exception e) {
            e.printStackTrace();
        }
        apiResponse.setResponseCode(HttpStatus.OK.value());
        return apiResponse;
    }
}
