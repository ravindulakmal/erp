package com.et.smartrrol.inventory.payload.supplier;

import lombok.Data;

@Data
public class SupplierSearchDTO{
    private String name;
    private Integer compId;
    private Integer branchId;
    private String userName;
    private Integer page;
    private Integer count;
}
