package com.et.smartrrol.commondata.models.order;


import com.et.smartrrol.commondata.models.BaseEntity;
import com.et.smartrrol.commondata.models.stock.Stock;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
public class OrderDetail extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private BigDecimal qty;
    private BigDecimal unitPrice;
    private BigDecimal tot;
    @ManyToOne
    @JoinColumn
    private Stock stock;
    @ManyToOne
    @JoinColumn
    @JsonIgnore
    private Orders orders;
}
