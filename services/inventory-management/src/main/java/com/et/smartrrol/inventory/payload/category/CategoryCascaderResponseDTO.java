package com.et.smartrrol.inventory.payload.category;

import lombok.Data;

import java.util.List;

@Data
public class CategoryCascaderResponseDTO {
    private String value;
    private String label;
    private List<CategoryCascaderResponseDTO> children;
}
