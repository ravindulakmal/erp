package com.et.smartrrol.user.controller;

import com.et.smartrrol.user.model.Kpi;
import com.et.smartrrol.user.service.KpiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/kpi")
@CrossOrigin
public class KpiController {

    @Autowired
    private KpiService kpiService;

    @GetMapping("/create")
    public Kpi createPost() {
        return kpiService.create();
    }

    @GetMapping("/findbyID/{id}")
    public Kpi findById(@PathVariable String id) {
        return kpiService.findById(id);
    }
}
