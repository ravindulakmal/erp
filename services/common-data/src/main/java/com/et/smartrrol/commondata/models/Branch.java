package com.et.smartrrol.commondata.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Branch extends AuditModel{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;
    private String branchCode;
    private String contact;
    private String address;
    private Integer numberOfEmployee;
    private Integer status;
    @ManyToOne
    private Company company;
}
