package com.et.smartrrol.paymentservice.payload.payment;

import com.et.smartrrol.commondata.models.BaseEntity;

import lombok.Data;

@Data
public class PaymentTypeDTO extends BaseEntity {
    private Integer id;
    private String paymentType;
}
