package com.et.smartrrol.customerservice.service;


import com.et.smartrrol.commondata.models.ApiResponse;
import com.et.smartrrol.commondata.models.Company;
import com.et.smartrrol.commondata.models.Status;
import com.et.smartrrol.commondata.models.v0.Customer;
import com.et.smartrrol.commondata.models.v0.CustomerCredited;
import com.et.smartrrol.commondata.repo.CompanyRepo;
import com.et.smartrrol.commondata.repo.v0.CustomerCreditedRepo;
import com.et.smartrrol.commondata.repo.v0.CustomerRepo;
import com.et.smartrrol.customerservice.dto.CustomerCreditedDTO;
import com.et.smartrrol.customerservice.dto.CustomerDTO;
import com.et.smartrrol.customerservice.dto.CustomerSerachDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepo customerRepo;
    @Autowired
    private CompanyRepo companyRepo;
    @Autowired
    private CustomerCreditedRepo customerCreditedRepo;

    public ApiResponse createCustomer(CustomerDTO customerDTO) {
        ApiResponse apiResponse = new ApiResponse();
        if (customerRepo.existsByMobileAndStatusAndCompId(customerDTO.getMobile(), Status.STATUS_ACTIVE.getVal(),customerDTO.getCompId())) {
            apiResponse.setResponseDesc("CUSTOMER ALLREADY DONE WITH SAME MOBILE NUMBER");
            apiResponse.setResponseCode(400);
            apiResponse.setData(0);
            return apiResponse;
        } else {
            try {
                Customer customer = null;
                customer = new Customer();
                BeanUtils.copyProperties(customerDTO, customer);
                customer.setStatus(Status.STATUS_ACTIVE.getVal());
                customer.setCode(this.generateCUSTOMERCode(companyRepo.findById(customerDTO.getCompId()).get()));
                Customer save = customerRepo.save(customer);
                if (save != null) {
                    if (customerDTO.getCustomerCreditedDTO() != null) {
                        CustomerCredited customerCredited = new CustomerCredited();
                        customerCredited.setCustomer(save);
                        customerCredited.setCreditLimit(customerDTO.getCustomerCreditedDTO().getCreditLimit());
                        customerCredited.setPaymentTerms(customerDTO.getCustomerCreditedDTO().getPaymentTerms());
                        CustomerCredited save1 = customerCreditedRepo.save(customerCredited);
                        if (save1 != null) {
                            apiResponse.setResponseDesc("CUSTOMER CREATED WITH CREDIT");
                            apiResponse.setResponseCode(200);
                            apiResponse.setData(save);
                        }
                    } else {
                        apiResponse.setResponseDesc("CUSTOMER CREATED WITHOUT CREDITED");
                        apiResponse.setResponseCode(200);
                        apiResponse.setData(save);
                    }
                }
                return apiResponse;
            } catch (Exception e) {
                apiResponse.setResponseDesc("e.getMessage()");
                apiResponse.setResponseCode(500);
                apiResponse.setData(e);
            }
        }
//        customer.set
        return apiResponse;
    }

    public ApiResponse updateCustomer(CustomerDTO customerDTO) {
        ApiResponse apiResponse = new ApiResponse();
//        if (!customerRepo.existsByMobileAndStatus(customerDTO.getMobile(),Status.STATUS_ACTIVE.getVal())){
//            apiResponse.setResponseDesc("CUSTOMER ALLREADY DONE");
//            apiResponse.setResponseCode(400);
//            apiResponse.setData(0);
//            return apiResponse;
//        }else {
        try {
            Customer customer = null;
            customer = customerRepo.findById(customerDTO.getId()).get();
            BeanUtils.copyProperties(customerDTO, customer);
            customer.setStatus(customerDTO.getStatus());
            customer.setCode(customerDTO.getCode());


            Customer save = customerRepo.save(customer);
            if (save != null) {
                if (customerDTO.getCustomerCreditedDTO() != null) {
                    CustomerCredited customerCredited = null;
                    if (customerDTO.getCustomerCreditedDTO().getId() == null || customerDTO.getCustomerCreditedDTO().getId() == 0) {
                        customerCredited = new CustomerCredited();
                    } else {
                        customerCredited = customerCreditedRepo.findById(customerDTO.getCustomerCreditedDTO().getId()).get();
                    }
                    customerCredited.setCustomer(save);
                    customerCredited.setCreditLimit(customerDTO.getCustomerCreditedDTO().getCreditLimit());
                    customerCredited.setPaymentTerms(customerDTO.getCustomerCreditedDTO().getPaymentTerms());
                    CustomerCredited save1 = customerCreditedRepo.save(customerCredited);
                    if (save1 != null) {
                        apiResponse.setResponseDesc("CUSTOMER UPDATED WITH CREDIT");
                        apiResponse.setResponseCode(200);
                        apiResponse.setData(save);
                    }
                } else {
                    apiResponse.setResponseDesc("CUSTOMER UPDATED WITHOUT CREDITED");
                    apiResponse.setResponseCode(200);
                    apiResponse.setData(save);
                }
            }
        } catch (Exception e) {
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(200);
            apiResponse.setData(e);
        }
//        }
////        customer.set
        return apiResponse;
    }

    public ApiResponse findAll(CustomerSerachDTO customerSerachDTO) {
        ApiResponse apiResponse = new ApiResponse();
        PageRequest page_req = PageRequest.of(customerSerachDTO.getPage(), customerSerachDTO.getCount());
        try{


            Page<Customer> all = customerRepo.findAll(this.getCustomerBySearch(customerSerachDTO), page_req);
            apiResponse.setResponseDesc("FIND ALL CUSTOMERS");
            apiResponse.setResponseCode(200);
            apiResponse.setData(all);
        } catch (Exception e){
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(null);
        }
        return apiResponse;
    }

    private Specification<Customer> getCustomerBySearch(CustomerSerachDTO customerSerachDTO){

        Specification<Customer> ordersSpecification = new Specification<Customer>() {
            @Override
            public Predicate toPredicate(Root<Customer> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                predicates.add(criteriaBuilder.equal(root.get("compId"),customerSerachDTO.getCompId()));
                predicates.add(criteriaBuilder.equal(root.get("status"),Status.STATUS_ACTIVE.getVal()));
                Predicate like = criteriaBuilder.like(root.get("firstName"), "%" + customerSerachDTO.getName() + "%");
                Predicate orderCode = criteriaBuilder.like(root.get("email"), "%" + customerSerachDTO.getName() + "%");
                Predicate lastname = criteriaBuilder.like(root.get("lastName"), "%" + customerSerachDTO.getName() + "%");
                Predicate mobile = criteriaBuilder.like(root.get("mobile"), "%" + customerSerachDTO.getName() + "%");
                Predicate city = criteriaBuilder.like(root.get("city"), "%" + customerSerachDTO.getName() + "%");
                predicates.add(criteriaBuilder.or(like,orderCode,lastname,mobile,city));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };

        return ordersSpecification;

    }

    public ApiResponse searchToAutoCommit(CustomerSerachDTO customerSerachDTO) {
        ApiResponse apiResponse = new ApiResponse();
        List<Customer> all = customerRepo.findBynameWithoutPageble(Status.STATUS_ACTIVE.getVal(), customerSerachDTO.getCompId(), customerSerachDTO.getName());
        List<CustomerDTO> customerDTOS = new ArrayList<>();
        all.forEach(customer -> {
            CustomerDTO customerDTO = new CustomerDTO();
            BeanUtils.copyProperties(customer, customerDTO);
            if(customer.getCustomerCredited()!=null){
                CustomerCreditedDTO supplierCredited= new CustomerCreditedDTO();

                BeanUtils.copyProperties(customerCreditedRepo.findByCustomerId(customer.getId()),supplierCredited);
                customerDTO.setCustomerCreditedDTO(supplierCredited);
            }
            customerDTOS.add(customerDTO);
        });
        if (!customerDTOS.isEmpty()) {
            apiResponse.setResponseDesc("FIND ALL CUSTOMERS");
            apiResponse.setResponseCode(200);
            apiResponse.setData(customerDTOS);
        } else {
            apiResponse.setResponseDesc("FIND ALL CUSTOMERS EMPTY");
            apiResponse.setResponseCode(200);
            apiResponse.setData(customerDTOS);
        }
        return apiResponse;
    }

    public void delete(Integer id) {
        Customer all = customerRepo.findById(id).get();
        all.setStatus(Status.STATUS_DISABLED.getVal());
        customerRepo.save(all);

    }

    private String generateCUSTOMERCode(Company company) {
        Integer maxId = customerRepo.getMaxId();
        int i = maxId + 1;
        String code = "CUST" + company.getId() + "00" + i;
        return code;
    }

}
