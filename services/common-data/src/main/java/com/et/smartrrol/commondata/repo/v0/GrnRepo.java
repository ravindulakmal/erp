package com.et.smartrrol.commondata.repo.v0;



import com.et.smartrrol.commondata.models.v0.Grn;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface GrnRepo extends JpaRepository<Grn,Integer>, JpaSpecificationExecutor<Grn> {

    @Query("Select u from Grn u where  (:status is null or u.status = :status) and (:companyID is null or u.compId = :companyID) and " +
            " (:email is null or  u.supplier.fullName like %:email%  or u.grnNo like %:email%)")
    List<Grn> findByname(Integer status, Integer companyID, String email, Pageable pageable);

    @Query("Select u from Grn u where  (:status is null or u.status = :status) and (:companyID is null or u.compId = :companyID) and " +
            " (:email is null or  u.supplier.fullName like %:email%  or u.grnNo like %:email%)")
    List<Grn> findBynameWIthoutPagination(Integer status, Integer companyID, String email);

    @Query("SELECT coalesce(max(inv.id), 0) FROM Grn inv")
    Integer getMaxId();

}
