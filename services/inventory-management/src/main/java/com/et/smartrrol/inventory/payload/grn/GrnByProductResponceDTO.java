package com.et.smartrrol.inventory.payload.grn;


import com.et.smartrrol.commondata.models.BaseEntity;
import com.et.smartrrol.inventory.payload.product.ProductDTO;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class GrnByProductResponceDTO extends BaseEntity {
    private Integer id;
    //    private Integer productID;
    private BigDecimal amount;
    private BigDecimal unitPrice;
    private BigDecimal sellingPrice;
    private BigDecimal retailPrice;
    private String batchNumber;
    private Integer qty;
    //    private ProductDTO productDto;
    private Integer status;
    private LocalDate expiryDate;


    // ProductDetails
    private Integer productId;
    private String productCode;
    private String productName;
    private Integer productCategoryId;
    private Integer brandId;
    private BigDecimal initialQty;
}
