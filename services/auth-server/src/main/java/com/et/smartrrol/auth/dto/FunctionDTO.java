package com.et.smartrrol.auth.dto;

import lombok.Data;

@Data
public class FunctionDTO {
    private Integer id;
    private String function;
    private Integer status;
}
