package com.et.smartrrol.auth.dto.company;

import lombok.Data;

@Data
public class ImageDTO {
        private String imageName;
        private String imageType;
        private String imagePath;
    private Integer branchId;

}


// retail customers only loyalty
// business customers
