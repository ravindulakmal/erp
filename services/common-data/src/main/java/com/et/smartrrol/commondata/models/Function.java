package com.et.smartrrol.commondata.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
public class Function extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String function;

    @ManyToMany(mappedBy = "functionList", fetch = FetchType.EAGER)
    private List<Role> role;

}
