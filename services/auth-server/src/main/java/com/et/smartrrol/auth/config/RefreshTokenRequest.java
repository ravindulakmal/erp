package com.et.smartrrol.auth.config;



import javax.validation.constraints.NotNull;

public class RefreshTokenRequest {
    @NotNull
    private String refreshToken;

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
