package com.et.smartrrol.commondata.repo.stock;

import com.et.smartrrol.commondata.models.stock.StockTransfer;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StockTransferRepo extends JpaRepository<StockTransfer,Integer>, JpaSpecificationExecutor<StockTransfer> {
    List<StockTransfer> findAllByStatusAndOldCompanyAndOldBranch(Integer status, Integer compId, Integer branchId, Pageable pageable);
}
