package com.et.smartrrol.auth.dto;

import lombok.Data;

import java.util.List;

@Data
public class RoleDTO {
    private Integer id;
    private  Integer status;
    private String role;
    private List<Integer> integerList;
    private List<FunctionDTO> functionDTOS;
}
