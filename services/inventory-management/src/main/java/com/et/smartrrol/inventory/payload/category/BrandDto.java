package com.et.smartrrol.inventory.payload.category;


import com.et.smartrrol.commondata.models.BaseEntity;
import lombok.Data;

@Data
public class BrandDto extends BaseEntity {
    private Integer id;
    private String name;
    private String userName;
    private String code,description;
}
