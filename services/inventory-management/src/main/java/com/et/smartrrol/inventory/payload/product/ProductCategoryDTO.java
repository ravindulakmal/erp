package com.et.smartrrol.inventory.payload.product;

import com.et.smartrrol.commondata.models.BaseEntity;

import lombok.Data;

@Data
public class ProductCategoryDTO extends BaseEntity {
    private Integer id;
    private String name;
    //    private String userName;
    private String description;
    private String code;
    //    private Integer branchId;
//    private Integer status;
//    private Integer compId;
    private Integer parentCategoryId;
}
