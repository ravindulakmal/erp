package com.et.smartrrol.inventory.controller.product;

import com.et.smartrrol.commondata.models.ApiResponse;
import com.et.smartrrol.inventory.payload.category.LimitsDTO;
import com.et.smartrrol.inventory.services.LimitsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/limits")
@CrossOrigin("*")
public class LimitsController {
    @Autowired
    private LimitsService limitsService;

    @GetMapping
    public String testService(HttpServletRequest request) {
        System.out.println("I am " + request.getRequestURL().toString());
        return request.getRequestURL().toString();
    }

    @PostMapping("/create")
    public ApiResponse createLimits(@RequestBody LimitsDTO limitsDTO) {

        return limitsService.createLimits(limitsDTO);

    }

    @PostMapping("/findAll")
    public ApiResponse findAll( ) {

        return limitsService.findAll();

    }

    @PutMapping("/update")
    public ApiResponse update(@RequestBody LimitsDTO limitsDTO ) {

        return limitsService.updateLimits(limitsDTO);

    }


}
