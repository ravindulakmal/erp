package com.et.smartrrol.commondata.models.order;

import com.et.smartrrol.commondata.models.BaseEntity;

import com.et.smartrrol.commondata.models.Invoice.Invoice;
import com.et.smartrrol.commondata.models.v0.Customer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Getter
@Setter
public class Orders extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private BigDecimal grossAmount;
    private BigDecimal netAmount;
    private BigDecimal tax;
    private BigDecimal transport;
    private BigDecimal discount;
    private Integer isCredited;
    private Integer isSettled;
    private String orderCode;


    @OneToMany(mappedBy = "orders", cascade = CascadeType.ALL)
    private List<OrderDetail> orderDetails;

    @ManyToOne
    @JoinColumn
    private Customer customer;

    @OneToOne(mappedBy = "orders")
    private Invoice invoice;
}
