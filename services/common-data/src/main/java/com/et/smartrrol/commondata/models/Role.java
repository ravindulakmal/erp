package com.et.smartrrol.commondata.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
public class Role extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String  role;
    @JsonIgnore
    @ManyToMany
    @JoinTable(
            name = "role_by_function",
            joinColumns = @JoinColumn(name = "function_id",referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id",referencedColumnName = "id"))
    private List<Function> functionList;

    @JsonIgnore
    @ManyToMany
    @JoinTable(
            name = "role_by_user",
            joinColumns = @JoinColumn(name = "user_id",referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id",referencedColumnName = "id"))
    private List<User> userList;
}
