package com.et.smartrrol.commondata.repo.payment;


import com.et.smartrrol.commondata.models.payment.Payment;
import com.et.smartrrol.commondata.models.payment.PaymentType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface PaymentTypeRepo extends JpaRepository<PaymentType, Integer>, JpaSpecificationExecutor<PaymentType> {


    List<PaymentType> findAllByStatusAndCompIdAndBranchId(Integer status, Integer compId, Integer branch);
}
