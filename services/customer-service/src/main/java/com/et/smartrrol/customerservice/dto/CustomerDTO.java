package com.et.smartrrol.customerservice.dto;


import com.et.smartrrol.commondata.models.BaseEntity;
import lombok.Data;

@Data
public class CustomerDTO extends BaseEntity {
    private Integer id;
    private String userName,code;
    //    private Integer branchId;
//    private Integer status;
//    private Integer compId;
    private CustomerCreditedDTO customerCreditedDTO;


    private String firstName,
            lastName,
            title,
            mobile,
            addLine1,
            addLine2,
            city,
            email;
}
