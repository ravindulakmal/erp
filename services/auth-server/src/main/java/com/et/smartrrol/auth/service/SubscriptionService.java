package com.et.smartrrol.auth.service;


import com.et.smartrrol.auth.dto.SubscriptionDTO;
import com.et.smartrrol.commondata.models.Status;
import com.et.smartrrol.commondata.models.Subscription;
import com.et.smartrrol.commondata.repo.SubscriptionRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SubscriptionService {
    @Autowired
    private SubscriptionRepo subscriptionRep;

    public Integer createS(SubscriptionDTO subscriptionDTO) {
        Subscription subscription = new Subscription();
        BeanUtils.copyProperties(subscriptionDTO, subscription);
        subscription.setStatus(Status.STATUS_ACTIVE.getVal());
        return subscriptionRep.save(subscription).getId();
    }


    public List<SubscriptionDTO> findAll() {
        List<SubscriptionDTO> subscriptionDTOS = new ArrayList<>();
        subscriptionRep.findAllByStatus(Status.STATUS_ACTIVE.getVal()).forEach(subscription1 -> {
            SubscriptionDTO subscriptionDTO = new SubscriptionDTO();
            BeanUtils.copyProperties(subscription1, subscriptionDTO);
            subscriptionDTOS.add(subscriptionDTO);
        });

        return subscriptionDTOS;
    }
}
