package com.et.smartrrol.auth.dto.company;



import com.et.smartrrol.commondata.models.BaseEntity;
import lombok.Data;

@Data
public class CompanyDTO extends BaseEntity {
    private Integer id;
    private Integer subscriptionId;
    private Integer userId;

    private String name;
    private String companyCode;
    private String subsName;
    private String brNo;
    private String officialMobile,businessName,businessType;
}
