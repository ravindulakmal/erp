package com.et.smartrrol.inventory.payload.stock;


import com.et.smartrrol.commondata.models.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class StockTransferDTO extends BaseEntity {
    private Integer id;
    private BigDecimal transferQty;
    private Integer transferOldStockId;
    private Integer newBatchStock;
    private Integer newBranch;
    private Integer newCompany;
    private Integer oldBranch;
    private Integer oldCompany;
}
