package com.et.smartrrol.inventory.payload.supplier;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class SupplierCreditedDTO {
    private Integer id;
    private BigDecimal creditLimit;
    private BigDecimal paymentTerms;
}
