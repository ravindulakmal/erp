package com.et.smartrrol.commondata.repo.order;

import com.et.smartrrol.commondata.models.order.Orders;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepo extends JpaRepository<Orders,Integer>, JpaSpecificationExecutor<Orders> {



    @Query("SELECT coalesce(max(inv.id), 0) FROM Orders inv")
    Integer getMaxId();

    @Query(value = "Select u from Orders u where  (:status is null or u.status = :status) and (:branchId is null or u.branchId = :branchId)  and (:companyID is null or u.compId = :companyID) and  (:email is null or  u.orderCode like %:email% or" +
            "  u.customer.firstName like %:email% or u.customer.lastName like %:email% )")
    List<Orders> findByname(Integer status, Integer branchId, Integer companyID, String email, Pageable pageable);

    @Query(value = "Select u from Orders u where  (:status is null or u.status = :status) and (:branchId is null or u.branchId = :branchId)  and (:companyID is null or u.compId = :companyID) and  (:email is null or  u.orderCode like %:email% or" +
            "  u.customer.firstName like %:email% or u.customer.lastName like %:email% )")
    List<Orders> findBynameWithoutPagination(Integer status, Integer branchId, Integer companyID, String email);
}
