package com.et.smartrrol.inventory.payload.grn;

import com.et.smartrrol.commondata.models.BaseEntity;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
public class GrnDto extends BaseEntity {
    private Integer Id;
    private Integer supplierID;
    private String grnNo;
    private LocalDate grnDate;
    private String remark;
    private BigDecimal grossAmount;
    private BigDecimal discount;
    private BigDecimal tax;
    private BigDecimal netAmount;
    private BigDecimal transport;
    private List<GrnbyProductDTO> grnbyProductDTOS;
}
