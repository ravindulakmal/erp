package com.et.smartrrol.commondata.repo.invoice;


import com.et.smartrrol.commondata.models.Invoice.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InvoiceRepo extends JpaRepository<Invoice,Integer>, JpaSpecificationExecutor<Invoice> {
    @Query("SELECT coalesce(max(inv.id), 0) FROM Invoice inv")
    Integer getMaxId();


    List<Invoice> findAllByOrders_Customer_IdAndStatusAndIsSettledAndOrders_IsCredited(Integer custId, Integer status, Integer isSettled, Integer isCredited);




}
