package com.et.smartrrol.commondata.models.payment;

import com.et.smartrrol.commondata.models.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
public class PaymentType extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String paymentType;

    @JsonIgnore
    @OneToMany(mappedBy = "paymentType", cascade = CascadeType.ALL)
    List<Payment> paymentList;

}
