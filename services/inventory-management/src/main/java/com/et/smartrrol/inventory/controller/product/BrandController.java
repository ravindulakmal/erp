package com.et.smartrrol.inventory.controller.product;


import com.et.smartrrol.commondata.models.ApiResponse;
import com.et.smartrrol.inventory.payload.category.BrandDto;
import com.et.smartrrol.inventory.payload.category.SearchBrandDTO;
import com.et.smartrrol.inventory.services.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/brand")
public class BrandController {


    @Autowired
    private BrandService brandService;

    @GetMapping
    public String testService(HttpServletRequest request) {
        System.out.println("I am " + request.getRequestURL().toString());
        return request.getRequestURL().toString();
    }


    @PostMapping("/create")
    public ApiResponse createBrand(@RequestBody BrandDto brandDto, @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId) {
        brandDto.setUserName(userName);
        brandDto.setBranchId(branchId);
        brandDto.setCompId(compId);
        return brandService.saveBrand(brandDto);

    }

    @PostMapping("/findAll/{page}/{count}")
    public ApiResponse findAll(@RequestBody SearchBrandDTO searchBrandDTO,
                               @RequestHeader("userName") String userName,
                               @RequestHeader("branchId") Integer branchId,
                               @RequestHeader("compId") Integer compId,
                               @PathVariable("page") Integer page,
                               @PathVariable("count") Integer count) {
        searchBrandDTO.setUserName(userName);
        searchBrandDTO.setBranchId(branchId);
        searchBrandDTO.setCompId(compId);
        searchBrandDTO.setPage(page);
        searchBrandDTO.setCount(count);
        return brandService.findAllBrand(searchBrandDTO);

    }

    @PostMapping("/findAll")
    public ApiResponse findAll(@RequestBody SearchBrandDTO searchBrandDTO,
                               @RequestHeader("userName") String userName,
                               @RequestHeader("branchId") Integer branchId,
                               @RequestHeader("compId") Integer compId) {
        searchBrandDTO.setUserName(userName);
        searchBrandDTO.setBranchId(branchId);
        searchBrandDTO.setCompId(compId);
        return brandService.findAllBrand(searchBrandDTO);

    }

    @PutMapping("/update")
    public ApiResponse update(@RequestBody BrandDto brandDto, @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId) {
        brandDto.setUserName(userName);
        brandDto.setBranchId(branchId);
        brandDto.setCompId(compId);
        return brandService.update(brandDto);

    }

    @DeleteMapping("/delete/{id}")
    public ApiResponse update(@PathVariable Integer id) {

        return brandService.deleteBran(id);

    }
}
