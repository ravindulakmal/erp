package com.et.smartrrol.auth.controller;

//import com.eyerax.inventory.config.JwtTokenUtil;
//import com.eyerax.inventory.config.JwtUserDetailsService;

import com.et.smartrrol.auth.config.*;
import com.et.smartrrol.auth.dto.*;

import com.et.smartrrol.auth.service.UserService;

import com.et.smartrrol.commondata.models.ApiResponse;
import com.et.smartrrol.commondata.models.RefreshToken;
import com.et.smartrrol.commondata.models.Status;
import com.et.smartrrol.commondata.models.User;
import com.et.smartrrol.commondata.repo.RoleRepo;
import com.et.smartrrol.commondata.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
//@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private RoleRepo roleRepo;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private RefreshTokenService refreshTokenService;
    @Autowired
    private AuthenticationManager authenticationManager;


    @PostMapping("/login")
    public ResponseEntity authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        String jwt = jwtUtils.generateJwtToken(userDetails);

        User byUserNameAndStatus = userRepo.findByUserNameAndStatus(loginRequest.getEmail(), Status.STATUS_ACTIVE.getVal());

        RefreshToken refreshToken = refreshTokenService.createRefreshToken(userDetails.getId());


        UserResponceDTO byId = userService.findById(userDetails.getId());

        return ResponseEntity.ok(new SignupResponse(jwt, refreshToken.getToken(), userDetails.getId(),
                userDetails.getUsername(), userDetails.getEmail(), byUserNameAndStatus.getRole()
        ,"BEARER TOKEN",200,byId));
    }

//    @PostMapping("/register")
//    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
//        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
//            return ResponseEntity.badRequest().body(new MessageResponse("Error: Username is already taken!"));
//        }
//
//        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
//            return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is already in use!"));
//        }
//
//        User user = new User(signUpRequest.getUsername(), signUpRequest.getEmail(),
//                encoder.encode(signUpRequest.getPassword()));
//
//        Set<String> strRoles = signUpRequest.getRole();
//        Set<Role> roles = new HashSet<>();
//
//        if (strRoles == null) {
//            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
//                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
//            roles.add(userRole);
//        } else {
//            strRoles.forEach(role -> {
//                switch (role) {
//                    case "admin":
//                        Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
//                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
//                        roles.add(adminRole);
//
//                        break;
//
//                    default:
//                        Role userRole = roleRepository.findByName(ERole.ROLE_USER)
//                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
//                        roles.add(userRole);
//                }
//            });
//        }
//
//        user.setRoles(roles);
//        userRepository.save(user);
//
//        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
//    }

    @PostMapping("/refreshtoken")
    public ResponseEntity<?> refreshtoken(@Valid @RequestBody RefreshTokenRequest request) {
        String requestRefreshToken = request.getRefreshToken();

        return refreshTokenService.findByToken(requestRefreshToken)
                .map(refreshTokenService::verifyExpiration)
                .map(RefreshToken::getUser)
                .map(user -> {
                    String token = jwtUtils.generateTokenFromUsername(((User) user).getUserName());
                    return ResponseEntity.ok(new TokenRefreshResponse(token, requestRefreshToken));
                })
                .orElseThrow(() -> new TokenRefreshException(requestRefreshToken,
                        "Refresh token is not in database!"));
    }



//    @PostMapping("/login")
//    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {
//        ResponseEntity<JwtResponse> jwtResponseResponseEntity = null;
//        try {
//
//            if (userRepo.existsByUserNameAndStatus(authenticationRequest.getEmail(), Status.STATUS_ACTIVE.getVal())) {
//                authenticate(authenticationRequest.getEmail(), authenticationRequest.getPassword());
//
//                final UserDetails userDetails = userDetailsService
//                        .loadUserByUsername(authenticationRequest.getEmail());
//
//                final String token = jwtTokenUtil.generateToken(userDeta ils);
//                final User byUserNameAndStatus = userRepo.findByUserNameAndStatus(authenticationRequest.getEmail(), Status.STATUS_ACTIVE.getVal());
//
//                jwtResponseResponseEntity = ResponseEntity.ok(new JwtResponse(token, byUserNameAndStatus,"TOKEN VALID", 200));
//            } else {
//                jwtResponseResponseEntity = ResponseEntity.notFound().build();
//            }
//        } catch (Exception e) {
//            jwtResponseResponseEntity = ResponseEntity.ok(new JwtResponse("","",e.getMessage(),500));
//        }
//
//        return jwtResponseResponseEntity;
//    }
//
//    private void authenticate(String username, String password) throws Exception {
//        try {
//            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
//        } catch (DisabledException e) {
//            throw new Exception("USER_DISABLED", e);
//        } catch (BadCredentialsException e) {
//            throw new Exception("INVALID_CREDENTIALS", e);
//        }
//    }

    @PostMapping("/create")
    public ApiResponse createUser(@RequestBody UserDTO userDTO) throws Exception {
//        grnVersionTwoDTO.setUserName(userName);
//        grnVersionTwoDTO.setBranchId(branchId);

        ApiResponse user = userService.createUser(userDTO);


        return user;
    }

    @PostMapping("/Registration")
    public ApiResponse Registration(@RequestBody RegistraionDTO registraionDTO) throws Exception {
//        grnVersionTwoDTO.setUserName(userName);
//        grnVersionTwoDTO.setBranchId(branchId);

        ApiResponse registration = userService.Registration(registraionDTO);


        return registration;
    }


    @PostMapping("/findAll")
    public ApiResponse findAllUsers(@RequestBody SearchUserDTO userDTO) {
//        grnVersionTwoDTO.setUserName(userName);
//        grnVersionTwoDTO.setBranchId(branchId);

        ApiResponse allUsers = userService.findAllUsers(userDTO);


        return allUsers;
    }

    @DeleteMapping("/delete/{id}")
    public ApiResponse delete(@PathVariable Integer id) {
        ApiResponse apiResponse = new ApiResponse();
//        grnVersionTwoDTO.setUserName(userName);
//        grnVersionTwoDTO.setBranchId(branchId);

        try {
            userService.deleteUser(id);
            apiResponse.setResponseCode(HttpStatus.OK.value());

        } catch (Exception e) {
            e.printStackTrace();
            apiResponse.setResponseCode(HttpStatus.BAD_REQUEST.value());
            apiResponse.setData(null);
        }

        return apiResponse;
    }

//    @PostMapping("/login")
//    public ApiResponse login(@RequestBody VerifyUser user) throws Exception {
//
////        grnVersionTwoDTO.setUserName(userName);
////        grnVersionTwoDTO.setBranchId(branchId);
//
//        ApiResponse apiResponse = userService.userLogin(user.getEmail(), user.getPassword());
//        return apiResponse;
//    }

    @PostMapping("/verifyUserOtp")
    public ApiResponse verifyUserOtp(@RequestBody VerifyUser user) {
        ApiResponse apiResponse = new ApiResponse();
//        grnVersionTwoDTO.setUserName(userName);
//        grnVersionTwoDTO.setBranchId(branchId);

        try {
            Integer grnVersionTwo = userService.verifyOtp(user.getEmail(), user.getOtp(), user.getId());
            apiResponse.setResponseCode(HttpStatus.OK.value());
            apiResponse.setData(grnVersionTwo);
        } catch (Exception e) {
            e.printStackTrace();
            apiResponse.setResponseCode(HttpStatus.BAD_REQUEST.value());
            apiResponse.setData(null);
        }

        return apiResponse;
    }

    @GetMapping("/resetPassword/{id}")
    public ApiResponse resetPassword(@PathVariable Integer id) {
        ApiResponse apiResponse = new ApiResponse();
//        grnVersionTwoDTO.setUserName(userName);
//        grnVersionTwoDTO.setBranchId(branchId);

        try {
            Integer grnVersionTwo = userService.resetPassword(id);
            apiResponse.setResponseCode(HttpStatus.OK.value());
            apiResponse.setData(grnVersionTwo);
        } catch (Exception e) {
            e.printStackTrace();
            apiResponse.setResponseCode(HttpStatus.BAD_REQUEST.value());
            apiResponse.setData(null);
        }

        return apiResponse;
    }

    @PostMapping("/changePasswordRequest")
    public ApiResponse changePasswordRequest(@RequestBody UserDTO userDTO) throws Exception {
//        grnVersionTwoDTO.setUserName(userName);
//        grnVersionTwoDTO.setBranchId(branchId);
        ApiResponse changePasswordRequest = userService.changePasswordRequest(userDTO);

        return changePasswordRequest;
    }

    @PostMapping("/verifyChangePassword")
    public ApiResponse verifyChangePassword(@RequestBody VerifyUser user) throws Exception {
//        grnVersionTwoDTO.setUserName(userName);
//        grnVersionTwoDTO.setBranchId(branchId);
        ApiResponse verifyChangePassword = userService.verifyChangePassword(user.getOtp(), user.getId());

        return verifyChangePassword;
    }

    @PostMapping("/changePassword")
    public ApiResponse changePassword(@RequestBody UserDTO userDTO) throws Exception {
//        grnVersionTwoDTO.setUserName(userName);
//        grnVersionTwoDTO.setBranchId(branchId);
        ApiResponse passwordChange = userService.passwordChange(userDTO);

        return passwordChange;
    }

    @PostMapping("/updateUser")
    public ApiResponse updateUser(@RequestBody UserDTO userDTO) {

//        grnVersionTwoDTO.setUserName(userName);
//        grnVersionTwoDTO.setBranchId(branchId);

        ApiResponse grnVersionTwo = userService.updateUser(userDTO);


        return grnVersionTwo;
    }
}
