package com.et.smartrrol.commondata.models.stock;

import com.et.smartrrol.commondata.models.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@Setter

public class StockTransfer extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private BigDecimal transferQty;
    @ManyToOne
    @JoinColumn
    private Stock stock;
    private Integer newBatchStock;
    private Integer newBranch;
    private Integer newCompany;
    private Integer oldBranch;
    private Integer oldCompany;

}
