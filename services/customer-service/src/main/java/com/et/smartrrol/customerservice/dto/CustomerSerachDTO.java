package com.et.smartrrol.customerservice.dto;

import lombok.Data;

@Data
public class CustomerSerachDTO {
    private String name;
    private String userName;
    private Integer compId;
    private Integer branchId;
    private Integer page;
    private Integer count;
}
