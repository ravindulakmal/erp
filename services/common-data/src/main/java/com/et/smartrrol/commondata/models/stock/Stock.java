package com.et.smartrrol.commondata.models.stock;


import com.et.smartrrol.commondata.models.BaseEntity;
import com.et.smartrrol.commondata.models.order.OrderDetail;
import com.et.smartrrol.commondata.models.v0.Product;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Entity
@Getter
@Setter

public class Stock extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private BigDecimal plusQty;
    private BigDecimal minusQty;
    private BigDecimal retailPrice;
    private BigDecimal unitPrice;
    private BigDecimal sellingPrice;
    private String reason;
    private String batchNumber;
    private LocalDate localDate;
    private Integer isTransferdEnable;
    private Integer transferdOrNot;
    private Integer grnByproductId;
    @ManyToOne
    @JoinColumn
    private Product product;
    @JsonIgnore
    @OneToMany(mappedBy = "stock", cascade = CascadeType.ALL)
    private List<StockAdjusment> stockAdjusments;

    @JsonIgnore
    @OneToMany(mappedBy = "stock", cascade = CascadeType.ALL)
    private List<StockTransfer> stockTransfers;

    @JsonIgnore
    @OneToMany(mappedBy = "stock", cascade = CascadeType.ALL)
    private List<OrderDetail> orderDetails;

}
