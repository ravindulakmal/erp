package com.et.smartrrol.sales.payloard;

import lombok.Data;

@Data

public class OrderItemOBJ {
    private String itemName;
    private String qty;
    private String amount;

    public OrderItemOBJ(String itemName, String qty, String amount) {
        this.itemName = itemName;
        this.qty = qty;
        this.amount = amount;
    }
}
