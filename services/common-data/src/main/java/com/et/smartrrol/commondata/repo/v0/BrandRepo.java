package com.et.smartrrol.commondata.repo.v0;


import com.et.smartrrol.commondata.models.v0.Brand;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface BrandRepo extends JpaRepository<Brand,Integer>, JpaSpecificationExecutor<Brand> {

    @Query("Select u from Brand u where  (:status is null or u.status = :status) and (:companyID is null or u.compId = :companyID) and  (:email is null or  u.name like %:email% or" +
            "  u.name like %:email% or u.code like %:email% )")
    List<Brand> findByname(Integer status, Integer companyID, String email, Pageable pageable);

    @Query("Select u from Brand u where  (:status is null or u.status = :status) and (:companyID is null or u.compId = :companyID) and  (:email is null or  u.name like %:email% or" +
            "  u.name like %:email% or u.code like %:email% )")
    List<Brand> findBynameWithoutPagination(Integer status, Integer companyID, String email);

    @Query("SELECT coalesce(max(inv.id), 0) FROM Brand inv")
    Integer getMaxId();
}
