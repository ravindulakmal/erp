package com.et.smartrrol.auth.service;

import com.et.smartrrol.auth.dto.branch.BranchDTO;
import com.et.smartrrol.auth.dto.branch.SearchBranchDTO;


import com.et.smartrrol.commondata.models.ApiResponse;
import com.et.smartrrol.commondata.models.Branch;
import com.et.smartrrol.commondata.models.Company;
import com.et.smartrrol.commondata.models.Status;
import com.et.smartrrol.commondata.repo.BranchRepo;
import com.et.smartrrol.commondata.repo.CompanyRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BranchService {

    @Autowired
    private BranchRepo branchRepo;

    @Autowired
    private CompanyRepo companyRepo;

    public ApiResponse createBranch(BranchDTO branchDTO){

        ApiResponse apiResponse = new ApiResponse();
        try {
            Branch branch = new Branch();
            BeanUtils.copyProperties(branchDTO, branch);
            Company company = companyRepo.findById(branchDTO.getCompanyId()).get();
            branch.setCompany(company);
            branch.setBranchCode(this.generateBranchCode(company));
            branch.setStatus(Status.STATUS_ACTIVE.getVal());
            Branch save = branchRepo.save(branch);
            if (save!=null){
                apiResponse.setResponseDesc("BRANCH CREATED");
                apiResponse.setResponseCode(200);
                apiResponse.setData(save);
            }
        }catch (Exception e){
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }return apiResponse;
    }

    public ApiResponse updateBranch(BranchDTO branchDTO){

        ApiResponse apiResponse = new ApiResponse();
        try {
            Branch branch = branchRepo.findById(branchDTO.getId()).get();
            BeanUtils.copyProperties(branchDTO, branch);
            branch.setCompany(companyRepo.findById(branchDTO.getCompanyId()).get());
            branch.setStatus(Status.STATUS_ACTIVE.getVal());
            branch.setBranchCode(branchDTO.getBranchCode());
            Branch save = branchRepo.save(branch);
            if (save!=null){
                apiResponse.setResponseDesc("BRANCH UPDATED");
                apiResponse.setResponseCode(200);
                apiResponse.setData(save);
            }
        }catch (Exception e){
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }return apiResponse;
    }

    public ApiResponse findAllBranch(SearchBranchDTO branchDTO){

        ApiResponse apiResponse = new ApiResponse();
        try {
            PageRequest page_req = PageRequest.of(branchDTO.getPage(), branchDTO.getCount());
            List<BranchDTO> branchDTOList = new ArrayList<>();
            branchRepo.findByname(Status.STATUS_ACTIVE.getVal(), branchDTO.getCompanyId(), branchDTO.getName(), page_req).forEach(branch -> {
                BranchDTO branchDTO1 = new BranchDTO();

                BeanUtils.copyProperties(branch,branchDTO1);
                branchDTO1.setCompanyId(branch.getCompany().getId());
                branchDTOList.add(branchDTO1);
            });


            apiResponse.setResponseDesc("FIND ALL BRANCHES");
            apiResponse.setResponseCode(200);
            apiResponse.setData(branchDTOList);

        }catch (Exception e){
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }return apiResponse;
    }


    private String generateBranchCode(Company company){
        Integer maxId = branchRepo.getMaxId();
        int i = maxId + 1;
        String code = "BR"+company.getId()+"00"+i;
        return code;
    }

}
