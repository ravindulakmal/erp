package com.et.smartrrol.commondata.repo;

import com.et.smartrrol.commondata.models.Branch;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface BranchRepo extends JpaRepository<Branch,Integer>, JpaSpecificationExecutor<Branch> {
    @Query("Select u from Branch u where  (:status is null or u.status = :status) and (:companyID is null or u.company.id = :companyID) and" +
            " (:email is null or u.name like %:email% or u.contact like %:email% or u.branchCode like %:email%)")
    List<Branch> findByname(Integer status, Integer companyID, String email, Pageable pageable);

    @Query("SELECT coalesce(max(inv.id), 0) FROM Branch inv")
    Integer getMaxId();
}
