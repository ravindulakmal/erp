package com.et.smartrrol.commondata.models;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN
}
