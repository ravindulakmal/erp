package com.et.smartrrol.commondata.models.v0;



import com.et.smartrrol.commondata.models.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "core_grn")
public class Grn extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String grnNo;
    private LocalDate grnDate;
    private BigDecimal transport;
    @ManyToOne
    @JoinColumn
    private Supplier supplier;
    private String remark;
    private BigDecimal grossAmount;
    private BigDecimal discount;
    private BigDecimal tax;
    private BigDecimal netAmount;
    @JsonIgnore
    @OneToMany(mappedBy = "grn", cascade = CascadeType.ALL)
    private List<GrnByProducts> grnByProducts;
//    @JsonIgnore
//    @ManyToMany
//    @JoinTable(
//            name = "grn_by_products",
//            joinColumns = @JoinColumn(name = "products_id",referencedColumnName = "id"),
//            inverseJoinColumns = @JoinColumn(name = "grn_id",referencedColumnName = "id"))
//    private List<Product> productList;
}
