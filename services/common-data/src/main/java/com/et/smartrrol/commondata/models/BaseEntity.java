package com.et.smartrrol.commondata.models;

import lombok.Data;

import javax.persistence.MappedSuperclass;

@Data
@MappedSuperclass
public class    BaseEntity extends AuditModel {

    private String userName;
    private Integer branchId;
    private Integer status;
    private Integer compId;


    public void fillEntity(String username,Integer branchId,Integer status) {
        this.userName=username;
        this.branchId= branchId;
        this.status = status;
    }
}
