package com.et.smartrrol.commondata.models;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;


@Getter
@Setter
@ToString
@Entity
public class ContactDetails extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String addLine1;
    private String addLine2;
    private String city;
    private String mobileNo;
    private String emergencyMobileNo;

//    @JsonIgnore
//    @OneToOne(mappedBy = "contactDetails")
//    private Employee employee;

//    @JsonIgnore
//    @OneToOne(mappedBy = "contactDetails")
//    private Supplier supplier;

//    @JsonIgnore
//    @OneToOne(mappedBy = "contactDetails")
//    private Customer customer;
}
