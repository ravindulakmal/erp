package com.et.smartrrol.inventory.payload.category;

import lombok.Data;

@Data
public class SearchBrandDTO {
    private Integer compId;
    private Integer branchId;
    private String name;
    private String userName;
    private Integer page;
    private Integer count;
}
