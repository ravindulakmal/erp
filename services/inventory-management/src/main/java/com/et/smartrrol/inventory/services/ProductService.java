package com.et.smartrrol.inventory.services;



import com.et.smartrrol.commondata.models.ApiResponse;
import com.et.smartrrol.commondata.models.Company;
import com.et.smartrrol.commondata.models.Status;
import com.et.smartrrol.commondata.models.v0.Brand;
import com.et.smartrrol.commondata.models.v0.Product;
import com.et.smartrrol.commondata.models.v0.ProductCategory;
import com.et.smartrrol.commondata.models.v0.Supplier;
import com.et.smartrrol.commondata.repo.CompanyRepo;
import com.et.smartrrol.commondata.repo.LimitsRepo;
import com.et.smartrrol.commondata.repo.v0.BrandRepo;
import com.et.smartrrol.commondata.repo.v0.ProductCategoryRepo;
import com.et.smartrrol.commondata.repo.v0.ProductRepo;
import com.et.smartrrol.inventory.payload.category.BrandDto;
import com.et.smartrrol.inventory.payload.category.LimitsDTO;
import com.et.smartrrol.inventory.payload.product.ProductCategoryDTO;
import com.et.smartrrol.inventory.payload.product.ProductDTO;
import com.et.smartrrol.inventory.payload.product.ProductResponceDTO;
import com.et.smartrrol.inventory.payload.product.SearchProductDTO;
import com.et.smartrrol.inventory.payload.supplier.SupplierDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    @Autowired
    private ProductRepo productRepo;
    @Autowired
    private ProductCategoryRepo productCategoryRepo;
    @Autowired
    private BrandRepo brandRepo;
    @Autowired
    private SupplierService supplierService;
    @Autowired
    private CompanyRepo companyRepo;
    @Autowired
    private LimitsRepo limitsRepo;


    public ApiResponse createProduct(ProductDTO productDTO) {
        ApiResponse apiResponse = new ApiResponse();
        try {
//            if (productRepo.existsByBatchNumber(productDTO.getBatchNumber())){
//                apiResponse.setResponseDesc("BATCH NUMBER ALLREADY EXISTS");
//                apiResponse.setResponseCode(500);
//                apiResponse.setData(null);
//            }else {
            Product product = new Product();
            BeanUtils.copyProperties(productDTO, product);
            product.setStatus(Status.STATUS_ACTIVE.getVal());
            Supplier supplier = supplierService.findByid(productDTO.getSupplierID());
            product.setSupplier(supplier);
//                product.setLimits(limitsRepo.findById(productDTO.getLimitId()).get());

            ProductCategory prodCategory = this.findProdCategory(productDTO.getProductCategoryId());
            product.setProductCategory(prodCategory);
            product.setProductCode(this.generateProductCode(companyRepo.findById(productDTO.getCompId()).get()));
            Brand brand = this.findBrandById(productDTO.getBrandId());
            product.setBrand(brand);

            Product save = productRepo.save(product);
            if (save != null) {
                apiResponse.setResponseDesc("PRODUCT CREATED");
                apiResponse.setResponseCode(200);
                apiResponse.setData(save);
            }
//            }
        } catch (Exception e) {
            e.printStackTrace();
//            apiResponse.setResponseDesc(e.getMessage());
//            apiResponse.setResponseCode(500);
//            apiResponse.setData(e);
        }
        return apiResponse;
    }

    public ApiResponse updateProduct(ProductDTO productDTO) throws Exception {
        ApiResponse apiResponse = new ApiResponse();
        try {
            Optional<Product> byId = productRepo.findById(productDTO.getId());
            if (byId.isPresent()) {
                Product product = byId.get();
                BeanUtils.copyProperties(productDTO, product);
                product.setStatus(productDTO.getStatus());
                Supplier supplier = supplierService.findByid(productDTO.getSupplierID());
                product.setSupplier(supplier);
//                product.setLimits(limitsRepo.findById(productDTO.getLimitId()).get());
                ProductCategory prodCategory = this.findProdCategory(productDTO.getProductCategoryId());
                product.setProductCategory(prodCategory);
                product.setProductCode(this.generateProductCode(companyRepo.findById(productDTO.getCompId()).get()));
                Brand brand = this.findBrandById(productDTO.getBrandId());
                product.setBrand(brand);

                Product save = productRepo.save(product);
                if (save != null) {
                    apiResponse.setResponseDesc("PRODUCT UPDATED");
                    apiResponse.setResponseCode(200);
                    apiResponse.setData(save);
                }
            }

        } catch (Exception e) {
//             throw new Exception(e);
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }
        return apiResponse;
    }

//    public ApiResponse saveProduct(Product product){
//
//        Supplier supplier = supplierService.findByid(product.getSupplier().getId());
//        product.setSupplier(supplier);
//
//        ProductCategoryController prodCategory = this.findProdCategory(product.getProductCategory().getId());
//        product.setProductCategory(prodCategory);
//
//        Brand brand = this.findBrandById(product.getBrand().getId());
//        product.setBrand(brand);
//
//        productRepo.save(product);
//    }

    public ApiResponse findAllProducts(SearchProductDTO searchProductDTO) {
        ApiResponse apiResponse = new ApiResponse();

        try {
            PageRequest page_req = PageRequest.of(searchProductDTO.getPage(), searchProductDTO.getCount());
//            PageRequest page_req = PageRequest.of(0, 10);

            Page<Product> all = productRepo.findAll(this.getProductBySearch(searchProductDTO), page_req);

            apiResponse.setResponseDesc("PRODUCT FIND AND SEARCH");
            apiResponse.setResponseCode(200);
            apiResponse.setData(all);
        } catch (Exception e) {
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }
        return apiResponse;
    }

    private Specification<Product> getProductBySearch(SearchProductDTO searchProductDTO) {

        Specification<Product> ordersSpecification = new Specification<Product>() {
            @Override
            public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                predicates.add(criteriaBuilder.equal(root.get("compId"), searchProductDTO.getCompId()));
                predicates.add(criteriaBuilder.equal(root.get("status"), Status.STATUS_ACTIVE.getVal()));
                predicates.add(criteriaBuilder.equal(root.get("branchId"), searchProductDTO.getBranchID()));
                Predicate like = criteriaBuilder.like(root.get("productName"), "%" + searchProductDTO.getName() + "%");
                Predicate orderCode = criteriaBuilder.like(root.get("productCode"), "%" + searchProductDTO.getName() + "%");
                Predicate lastname = criteriaBuilder.like(root.get("quality"), "%" + searchProductDTO.getName() + "%");
                predicates.add(criteriaBuilder.or(like, orderCode, lastname));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };

        return ordersSpecification;

    }

    public ApiResponse findBynameWithoutPagination(SearchProductDTO searchProductDTO) {
        ApiResponse apiResponse = new ApiResponse();

        List<ProductResponceDTO> productDTOS = new ArrayList<>();
        try {

            productRepo.findBynameWithoutPagination(Status.STATUS_ACTIVE.getVal(), searchProductDTO.getCompId(), searchProductDTO.getName()).forEach(product -> {
                ProductResponceDTO productDTO = new ProductResponceDTO();
                BeanUtils.copyProperties(product, productDTO);
//                 BRAND TO DTO
                BrandDto brandDto = new BrandDto();
                BeanUtils.copyProperties(product.getBrand(), brandDto);
                productDTO.setBrandDto(brandDto);
//                 CATEGORY TO DTO
                ProductCategoryDTO categoryResponseDTO = new ProductCategoryDTO();

                BeanUtils.copyProperties(product.getProductCategory(), categoryResponseDTO);
                productDTO.setProductCategoryDTO(categoryResponseDTO);
//                 SUPPLIER TO DTO
                SupplierDTO supplierDTO = new SupplierDTO();
                BeanUtils.copyProperties(product.getSupplier(), supplierDTO);
                productDTO.setSupplierDTO(supplierDTO);
//                 Limits to DTO

//                 LimitsDTO limitsDTO =new LimitsDTO();
//                 BeanUtils.copyProperties(product.getLimits(),limitsDTO);
//                 productDTO.setLimitsDTO(limitsDTO);

                productDTOS.add(productDTO);
            });
            apiResponse.setResponseDesc("PRODUCT FIND AND SEARCH");
            apiResponse.setResponseCode(200);
            apiResponse.setData(productDTOS);
        } catch (Exception e) {
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(500);
            apiResponse.setData(e);
        }
        return apiResponse;
    }


    public Product findProdByCode(String prodCode) {
        return productRepo.findByProductCode(prodCode);
    }

    public ApiResponse deleteProduct(Integer id) {
        ApiResponse apiResponse = new ApiResponse();
        Product prodByCode = productRepo.findById(id).get();
        prodByCode.setStatus(Status.STATUS_DISABLED.getVal());
        productRepo.save(prodByCode);
        apiResponse.setResponseDesc("PRODUCT DELETE");
        apiResponse.setResponseCode(200);
        apiResponse.setData(id);
        return apiResponse;
    }

    public void saveProdCategory(ProductCategory productCategory) {
        productCategoryRepo.save(productCategory);
    }

    public ProductCategory findProdCategory(Integer id) {
        return productCategoryRepo.findById(id).get();
    }

    public void saveBrand(Brand brand) {
        brandRepo.save(brand);
    }

    public List<Brand> findAllBrand() {
        return brandRepo.findAll();
    }

    public Brand findBrandById(Integer id) {
        return brandRepo.findById(id).get();
    }

    private String generateProductCode(Company company) {
        Integer maxId = productRepo.getMaxId();
        int i = maxId + 1;
        String code = "PR" + company.getId() + "00" + i;
        return code;
    }
}