package com.et.smartrrol.inventory.controller.product;


import com.et.smartrrol.commondata.models.ApiResponse;
import com.et.smartrrol.inventory.payload.product.ProductDTO;
import com.et.smartrrol.inventory.payload.product.SearchProductDTO;
import com.et.smartrrol.inventory.services.GrnService;
import com.et.smartrrol.inventory.services.ProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/product")
@CrossOrigin("*")

public class ProductController {


    @Autowired
    private  ProductService productService;
    @Autowired
    private  GrnService grnService;

    public ProductController(ProductService productService, GrnService grnService) {
        this.productService = productService;
        this.grnService = grnService;
    }


    @GetMapping
    public String testService(HttpServletRequest request) {
        System.out.println("I am " + request.getRequestURL().toString());
        return request.getRequestURL().toString();
    }
//    @PostMapping("/issue")
//    public ApiResponse issue(@RequestBody IssueDto issueDto){
//        issuanceService.issue(issueDto);
//        ApiResponse apiResponse = new ApiResponse();
//         apiResponse.setData(issueDto);
//        apiResponse.setResponseCode(HttpStatus.OK.value());
//        apiResponse.setResponseDesc("Issued Successfully");
//        return apiResponse;
//    }

//    @GetMapping("/find/by-emp/{serviceNo}")
//    public ApiResponse findAllIssuancesByEmp(@PathVariable String serviceNo){
//        ApiResponse apiResponse = new ApiResponse();
//        List<Issuance> instancesByEmployee = issuanceService.findInstancesByEmployee(serviceNo);
//        apiResponse.setData(instancesByEmployee);
//        apiResponse.setResponseCode(HttpStatus.OK.value());
//        return apiResponse;
//    }

    @PostMapping("/create")
    public ApiResponse createProduct(@RequestBody ProductDTO productDTO, @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId) throws Exception {
        productDTO.setUserName(userName);
        productDTO.setBranchId(branchId);
        productDTO.setCompId(compId);
        ApiResponse product = productService.createProduct(productDTO);

        return product;
    }

    @PostMapping("/update")
    public ApiResponse update(@RequestBody ProductDTO productDTO, @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId) throws Exception {
        productDTO.setUserName(userName);
        productDTO.setBranchId(branchId);
        productDTO.setCompId(compId);
        ApiResponse product = productService.updateProduct(productDTO);

        return product;
    }

    @DeleteMapping("/delete/{id}")
    public ApiResponse update(@PathVariable Integer id){

        ApiResponse product = productService.deleteProduct(id);

        return product;
    }

//    @PostMapping("/grn")
//    public ApiResponse grn(@RequestBody GrnDto jobOrderDto){
//
//        ApiResponse apiResponse = new ApiResponse();
//        apiResponse.setData(jobOrderDto);
//        grnService.createGrn(jobOrderDto);
//        apiResponse.setResponseCode(HttpStatus.OK.value());
//        apiResponse.setResponseDesc("Grn Successfully");
//
//        return apiResponse;
//
//    }

//    @PostMapping("/delete/{prodCode}")
//    public ApiResponse deleteProduct(@PathVariable String prodCode){
//        ApiResponse apiResponse = new ApiResponse();
//        apiResponse.setData(prodCode);
//        productService.deleteProduct(prodCode);
//        apiResponse.setResponseCode(HttpStatus.OK.value());
//        apiResponse.setResponseDesc("delete Successfully");
//
//        return apiResponse;
//    }

    @PostMapping("/findALl/{page}/{count}")
    public ApiResponse getAllProducts(@RequestBody SearchProductDTO searchProductDTO ,
                                      @RequestHeader("userName") String userName,
                                      @RequestHeader("branchId") Integer branchId,
                                      @RequestHeader("compId") Integer compId,
                                      @PathVariable("page") Integer page,
                                      @PathVariable("count") Integer count){
        searchProductDTO.setUserName(userName);
        searchProductDTO.setBranchID(branchId);
        searchProductDTO.setCompId(compId);
        searchProductDTO.setPage(page);
        searchProductDTO.setCount(count);
        return productService.findAllProducts(searchProductDTO);

    }


    @PostMapping("/findALl")
    public ApiResponse findBynameWithoutPagination(@RequestBody SearchProductDTO searchProductDTO ,
                                                   @RequestHeader("userName") String userName,
                                                   @RequestHeader("branchId") Integer branchId,
                                                   @RequestHeader("compId") Integer compId){
        searchProductDTO.setUserName(userName);
        searchProductDTO.setBranchID(branchId);
        searchProductDTO.setCompId(compId);
        return productService.findBynameWithoutPagination(searchProductDTO);

    }


}
