package com.et.smartrrol.sales.payloard;


import com.et.smartrrol.commondata.models.BaseEntity;
import lombok.Data;

@Data
public class OrderSearchDTO extends BaseEntity {
    private String customerName;
    private int page;
    private int count;
}
