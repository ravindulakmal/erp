package com.et.smartrrol.commondata.models.payment;


import com.et.smartrrol.commondata.models.BaseEntity;
import com.et.smartrrol.commondata.models.Invoice.Invoice;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
public class Payment extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String paymentCode;
    @ManyToOne
    @JoinColumn
    private Invoice invoice;
    private BigDecimal paidAmount;
    @ManyToOne
    @JoinColumn
    private PaymentType paymentType;

}
