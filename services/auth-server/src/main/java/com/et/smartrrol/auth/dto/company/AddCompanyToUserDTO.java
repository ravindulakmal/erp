package com.et.smartrrol.auth.dto.company;

import lombok.Data;

import java.util.List;

@Data
public class AddCompanyToUserDTO {
    private Integer companyId;
    private List<Integer> userIds;
}
