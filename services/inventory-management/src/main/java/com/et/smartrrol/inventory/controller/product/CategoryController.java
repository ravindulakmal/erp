package com.et.smartrrol.inventory.controller.product;



import com.et.smartrrol.commondata.models.ApiResponse;
import com.et.smartrrol.inventory.payload.category.CategoryRequestDTO;
import com.et.smartrrol.inventory.payload.category.CategorySearchDTO;
import com.et.smartrrol.inventory.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/category")
@CrossOrigin("*")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    @GetMapping
    public String testService(HttpServletRequest request) {
        System.out.println("I am " + request.getRequestURL().toString());
        return request.getRequestURL().toString();
    }

    @PostMapping("/create")
//    @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId
    public ApiResponse createCategory(@RequestBody CategoryRequestDTO categoryRequestDTO, @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId) {
        categoryRequestDTO.setUserName(userName);
        categoryRequestDTO.setBranchId(branchId);
        categoryRequestDTO.setCompId(compId);
        return categoryService.saveCategory(categoryRequestDTO);
    }

    @PutMapping("/update")
    public ApiResponse updateCategory(@RequestBody CategoryRequestDTO categoryRequestDTO, @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId) {
        categoryRequestDTO.setUserName(userName);
        categoryRequestDTO.setBranchId(branchId);
        categoryRequestDTO.setCompId(compId);
        return categoryService.updateCategory(categoryRequestDTO);
    }

//    @PutMapping("/status/{id}/{status}")
//    public ApiResponse changeCategoryStatus(@PathVariable Integer id, @PathVariable String status) {
//        ApiResponse apiResponse = new ApiResponse();
//        apiResponse.setOpperation("CATEGORY STATUS UPDATE");
//        apiResponse.setResponseCode(HttpStatus.OK.value());
//
//        Integer categoryId = categoryService.changeCategoryStatus(id, status);
//        apiResponse.setData(categoryId);
//
//        return apiResponse;
//    }

    @PostMapping("/getAll/{page}/{count}")
    public ApiResponse getAllCategories(@RequestBody CategorySearchDTO categorySearchDTO,
                                        @RequestHeader("userName") String userName,
                                        @RequestHeader("branchId") Integer branchId,
                                        @RequestHeader("compId") Integer compId,
                                        @PathVariable("page") Integer page,
                                        @PathVariable("count") Integer count) {
        categorySearchDTO.setUserName(userName);
        categorySearchDTO.setBranchId(branchId);
        categorySearchDTO.setCompId(compId);
        categorySearchDTO.setPage(page);
        categorySearchDTO.setCount(count);
        return categoryService.getAllCategories(categorySearchDTO);
    }


    @PostMapping("/getAll")
    public ApiResponse findBynameWithoutPagination(@RequestBody CategorySearchDTO categorySearchDTO,
                                                   @RequestHeader("userName") String userName,
                                                   @RequestHeader("branchId") Integer branchId,
                                                   @RequestHeader("compId") Integer compId) {
        categorySearchDTO.setUserName(userName);
        categorySearchDTO.setBranchId(branchId);
        categorySearchDTO.setCompId(compId);
        return categoryService.findBynameWithoutPagination(categorySearchDTO);
    }

//    @GetMapping("/getAll/cascader")
//    public ApiResponse getAllCategoriesForCascader() {
//        ApiResponse apiResponse = new ApiResponse();
//        List<CategoryCascaderResponseDTO> allCategories = categoryService.getAllCategoriesForCascader();
//        apiResponse.setResponseCode(HttpStatus.OK.value());
//        apiResponse.setData(allCategories);
//        return apiResponse;
//    }
}
