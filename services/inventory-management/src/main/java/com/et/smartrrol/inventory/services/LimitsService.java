package com.et.smartrrol.inventory.services;


import com.et.smartrrol.commondata.models.ApiResponse;
import com.et.smartrrol.commondata.models.v0.Limits;
import com.et.smartrrol.commondata.repo.LimitsRepo;
import com.et.smartrrol.inventory.payload.category.LimitsDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LimitsService {

    @Autowired
    private LimitsRepo limitsRepo;

    public ApiResponse createLimits(LimitsDTO limitsDTO){
        ApiResponse apiResponse = new ApiResponse();

        try{
            Limits limits = new Limits();
            BeanUtils.copyProperties(limitsDTO,limits);
            Limits save = limitsRepo.save(limits);
            if (save!=null){
                apiResponse.setResponseDesc("CREATE LIMITS");
                apiResponse.setResponseCode(200);
                apiResponse.setData(save);
            }
        }catch (Exception e){
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(200);
            apiResponse.setData(null);
        }

        return apiResponse;
    }


    public ApiResponse updateLimits(LimitsDTO limitsDTO){
        ApiResponse apiResponse = new ApiResponse();

        try{
            Limits limits = limitsRepo.findById(limitsDTO.getId()).get();
            BeanUtils.copyProperties(limits,limitsDTO);
            Limits save = limitsRepo.save(limits);
            if (save!=null){
                apiResponse.setResponseDesc("UPDATE LIMITS");
                apiResponse.setResponseCode(200);
                apiResponse.setData(save);
            }
        }catch (Exception e){
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(200);
            apiResponse.setData(null);
        }

        return apiResponse;
    }

    public ApiResponse findAll(){
        ApiResponse apiResponse = new ApiResponse();
        List<LimitsDTO> limitsDTOS = new ArrayList<>();
        try {
            List<Limits> all = limitsRepo.findAll();

            for (Limits limits :
                    all) {
                LimitsDTO limitsDTO = new LimitsDTO();
                BeanUtils.copyProperties(limits, limitsDTO);
                limitsDTOS.add(limitsDTO);
            }
            apiResponse.setResponseDesc("FIND ALL LIMITS");
            apiResponse.setResponseCode(200);
            apiResponse.setData(limitsDTOS);
        }catch (Exception e){
            apiResponse.setResponseDesc(e.getMessage());
            apiResponse.setResponseCode(200);
            apiResponse.setData(null);
        }
        return apiResponse;
    }

}
