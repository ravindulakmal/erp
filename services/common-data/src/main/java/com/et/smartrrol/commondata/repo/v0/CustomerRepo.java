package com.et.smartrrol.commondata.repo.v0;


import com.et.smartrrol.commondata.models.v0.Customer;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepo extends JpaRepository<Customer,Integer>, JpaSpecificationExecutor<Customer> {

    boolean existsByMobileAndStatusAndCompId(String mobile, Integer status,Integer compId);


    @Query("Select u from Customer u where  (:status is null or u.status = :status) and (:companyID is null or u.compId = :companyID) and  (:email is null or  u.email like %:email% or" +
            "  u.firstName like %:email% or " +
            " u.lastName like %:email% or" +
            "  u.mobile like %:email% or" +
            " u.city like %:email% )")
    List<Customer> findByname(Integer status, Integer companyID, String email, Pageable pageable);

    @Query("Select u from Customer u where  (:status is null or u.status = :status) and (:companyID is null or u.compId = :companyID) and  (:email is null or  u.email like %:email% or" +
            "  u.firstName like %:email% or " +
            " u.lastName like %:email% or" +
            "  u.mobile like %:email% or" +
            " u.city like %:email% )")
    List<Customer> findBynameWithoutPageble(Integer status, Integer companyID, String email);

    @Query("SELECT coalesce(max(inv.id), 0) FROM Customer inv")
    Integer getMaxId();
}
