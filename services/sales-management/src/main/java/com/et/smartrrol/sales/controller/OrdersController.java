package com.et.smartrrol.sales.controller;


import com.et.smartrrol.commondata.models.ApiResponse;
import com.et.smartrrol.sales.payloard.OrderDTO;
import com.et.smartrrol.sales.payloard.OrderSearchDTO;
import com.et.smartrrol.sales.service.OrderService;
import com.et.smartrrol.sales.service.report.ReportService;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;

@RestController
@CrossOrigin("*")
@RequestMapping("/orders")
public class OrdersController {


    @Autowired
    private OrderService orderService;

    @Autowired
    private ReportService reportService;

    @PostMapping("/create")
    public ApiResponse orderCreate(@RequestBody OrderDTO orderDTO, @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId) {
//        , @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId

        orderDTO.setUserName(userName);
        orderDTO.setBranchId(branchId);
        orderDTO.setCompId(compId);

        return orderService.orderCreate(orderDTO);

    }

    @PutMapping("/update")
    public ApiResponse orderUpdate(@RequestBody OrderDTO orderDTO, @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId) {
//        , @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId

        orderDTO.setUserName(userName);
        orderDTO.setBranchId(branchId);
        orderDTO.setCompId(compId);

        return orderService.orderUpdate(orderDTO);

    }

    @PutMapping("/order-findBy-CustNameOrOrderCOde/{page}/{count}")
    public ApiResponse orderFindByCustNameOrOrderCOde(@RequestBody OrderSearchDTO orderSearchDTO,
                                                      @RequestHeader("userName") String userName,
                                                      @RequestHeader("branchId") Integer branchId,
                                                      @RequestHeader("compId") Integer compId,
                                                      @PathVariable("page") Integer page,
                                                      @PathVariable("count") Integer count) {
//        , @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId

        orderSearchDTO.setUserName(userName);
        orderSearchDTO.setBranchId(branchId);
        orderSearchDTO.setCompId(compId);
        orderSearchDTO.setPage(page);
        orderSearchDTO.setCount(count);

        return orderService.findAllOrder(orderSearchDTO);

    }

    @PutMapping("/order-findBy-CustNameOrOrderCOde")
    public ApiResponse findBynameWithoutPagination(@RequestBody OrderSearchDTO orderSearchDTO,
                                                   @RequestHeader("userName") String userName,
                                                   @RequestHeader("branchId") Integer branchId,
                                                   @RequestHeader("compId") Integer compId) {
//        , @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId

        orderSearchDTO.setUserName(userName);
        orderSearchDTO.setBranchId(branchId);
        orderSearchDTO.setCompId(compId);

        return orderService.findBynameWithoutPagination(orderSearchDTO);

    }

    @DeleteMapping("/order-cancel/{id}")
    public ApiResponse findBynameWithoutPagination(@PathVariable Integer id) {
//        , @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId



        return orderService.orderCancel(id);

    }

    @GetMapping("/print/invoice/{id}")
    public void printReportByBookingID(@PathVariable Integer id, HttpServletResponse httpServletResponse) throws IOException, JRException, SQLException, FontFormatException {


        JasperPrint jasperPrint = reportService.orderReport(id);
        httpServletResponse.setContentType("application/x-pdf");
        httpServletResponse.setHeader("Content-Disposition", "inline; filename=test.pdf");

        final OutputStream outputStream = httpServletResponse.getOutputStream();
        JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
    }

//    @PostMapping("/create")
//    public ApiResponse orderCreate(@RequestBody OrderDTO orderDTO, @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId) {
////        , @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId
//
//        orderDTO.setUserName(userName);
//        orderDTO.setBranchId(branchId);
//        orderDTO.setCompId(compId);
//
//        return orderService.orderCreate(orderDTO);
//
//    }

}
