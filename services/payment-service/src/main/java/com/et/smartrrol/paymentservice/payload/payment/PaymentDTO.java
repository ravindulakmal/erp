package com.et.smartrrol.paymentservice.payload.payment;

import com.et.smartrrol.commondata.models.BaseEntity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class PaymentDTO extends BaseEntity {
    private Integer id;
    private Integer invoiceId;
    private Integer paymentTypeID;
    private Integer isCredited;

    private BigDecimal paidAmount;

}
