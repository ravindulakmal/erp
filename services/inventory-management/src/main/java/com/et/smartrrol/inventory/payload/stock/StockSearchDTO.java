package com.et.smartrrol.inventory.payload.stock;


import com.et.smartrrol.commondata.models.BaseEntity;
import lombok.Data;

@Data
public class StockSearchDTO extends BaseEntity {
    private String name;
    private Integer page;
    private Integer count;
}
