package com.et.smartrrol.inventory.controller.product;



import com.et.smartrrol.commondata.models.ApiResponse;
import com.et.smartrrol.inventory.payload.category.CategorySearchDTO;
import com.et.smartrrol.inventory.payload.product.ProductCategoryDTO;
import com.et.smartrrol.inventory.services.ProductCategoryServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/product/category")
@CrossOrigin("*")
public class ProductCategoryController {

    @Autowired
    private ProductCategoryServices productCategoryServices;

    @GetMapping
    public String testService(HttpServletRequest request) {
        System.out.println("I am " + request.getRequestURL().toString());
        return request.getRequestURL().toString();
    }


    @PostMapping("/create")
    public ApiResponse createProduct(@RequestBody ProductCategoryDTO productCategoryDTO, @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId){
        productCategoryDTO.setUserName(userName);
        productCategoryDTO.setBranchId(branchId);
        productCategoryDTO.setCompId(compId);

        ApiResponse product = productCategoryServices.createProductCategory(productCategoryDTO);

        return product;
    }

    @PostMapping("/update")
    public ApiResponse update(@RequestBody ProductCategoryDTO productCategoryDTO, @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId){
        productCategoryDTO.setUserName(userName);
        productCategoryDTO.setBranchId(branchId);
        productCategoryDTO.setCompId(compId);

        ApiResponse product = productCategoryServices.updateProductCategory(productCategoryDTO);

        return product;
    }

    @PostMapping("/findAll/{page}/{count}")
    public ApiResponse findAll(@RequestBody CategorySearchDTO CategorySearchDTO,
                               @RequestHeader("userName") String userName,
                               @RequestHeader("branchId") Integer branchId,
                               @RequestHeader("compId") Integer compId,
                               @PathVariable("page") Integer page,
                               @PathVariable("count") Integer count){
        CategorySearchDTO.setUserName(userName);
        CategorySearchDTO.setBranchId(branchId);
        CategorySearchDTO.setCompId(compId);
        CategorySearchDTO.setPage(page);
        CategorySearchDTO.setCount(count);

        ApiResponse product = productCategoryServices.findAllProductCategory(CategorySearchDTO);

        return product;
    }

    @PostMapping("/findAll")
    public ApiResponse findBynameWithoutPagination(@RequestBody CategorySearchDTO CategorySearchDTO, @RequestHeader("userName") String userName, @RequestHeader("branchId") Integer branchId, @RequestHeader("compId") Integer compId){
        CategorySearchDTO.setUserName(userName);
        CategorySearchDTO.setBranchId(branchId);
        CategorySearchDTO.setCompId(compId);

        ApiResponse product = productCategoryServices.findBynameWithoutPagination(CategorySearchDTO);

        return product;
    }



    @DeleteMapping("/delete/{id}")
    public ApiResponse delete(@PathVariable Integer id){

        ApiResponse product = productCategoryServices.deleteroductCategory(id);

        return product;
    }
}
